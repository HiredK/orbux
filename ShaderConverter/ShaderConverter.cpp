#include "ShaderConverter.h"  

#include <shaderc/shaderc.hpp>
#include <Xsc/Xsc.h>
#include <sstream>

namespace ShaderConverter
{
	std::string HLSLToGLSL(const std::string& input, orb::Shader::Stage stage)
	{
		auto StageToTarget = std::map<orb::Shader::Stage, Xsc::ShaderTarget>() =
		{
			{ orb::Shader::eUndefined      , Xsc::ShaderTarget::Undefined                    },
			{ orb::Shader::eVertex         , Xsc::ShaderTarget::VertexShader                 },
			{ orb::Shader::eTessControl    , Xsc::ShaderTarget::TessellationControlShader    },
			{ orb::Shader::eTessEvaluation , Xsc::ShaderTarget::TessellationEvaluationShader },
			{ orb::Shader::eGeometry       , Xsc::ShaderTarget::GeometryShader               },
			{ orb::Shader::eFragment       , Xsc::ShaderTarget::FragmentShader               },
			{ orb::Shader::eCompute        , Xsc::ShaderTarget::ComputeShader                }
		};

		auto inputStream = std::make_shared<std::istringstream>(input);
		std::ostringstream outputStream;

		Xsc::ShaderInput inputDesc;
		inputDesc.sourceCode = inputStream;
		inputDesc.shaderVersion = Xsc::InputShaderVersion::HLSL5;
		inputDesc.shaderTarget = StageToTarget[stage];
		inputDesc.entryPoint = "main";

		Xsc::ShaderOutput outputDesc;
		outputDesc.sourceCode = &outputStream;
		outputDesc.shaderVersion = Xsc::OutputShaderVersion::GLSL450;
		outputDesc.options.autoBinding = true;
		outputDesc.options.optimize = true;
		std::string output = "";

		try
		{
			if (Xsc::CompileShader(inputDesc, outputDesc)) {
				output = outputStream.str();
			}
		}
		catch (const std::exception& e)
		{
			throw Exception(e.what());
		}

		return output;
	}

	std::string GLSLToSPIRV(const std::string& input, orb::Shader::Stage stage)
	{
		auto StageToKind = std::map<orb::Shader::Stage, shaderc_shader_kind>() =
		{
			{ orb::Shader::eUndefined      , shaderc_glsl_infer_from_source      },
			{ orb::Shader::eVertex         , shaderc_glsl_vertex_shader          },
			{ orb::Shader::eTessControl    , shaderc_glsl_tess_control_shader    },
			{ orb::Shader::eTessEvaluation , shaderc_glsl_tess_evaluation_shader },
			{ orb::Shader::eGeometry       , shaderc_glsl_geometry_shader        },
			{ orb::Shader::eFragment       , shaderc_glsl_fragment_shader        },
			{ orb::Shader::eCompute        , shaderc_glsl_compute_shader         }
		};

		shaderc::CompileOptions options;
		options.SetOptimizationLevel(shaderc_optimization_level_size);
		options.SetSourceLanguage(shaderc_source_language_glsl);
		shaderc::Compiler compiler;

		auto res = compiler.CompileGlslToSpv(input, StageToKind[stage], "test", options);
		if (res.GetCompilationStatus() != shaderc_compilation_status_success)
		{
			throw Exception(res.GetErrorMessage());
		}

		return std::string(
			reinterpret_cast<const char*>(res.cbegin()),
			reinterpret_cast<const char*>(res.cend())
		);
	}
}