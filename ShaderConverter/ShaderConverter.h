/**
* @file ShaderConverter.h
* @brief
*/

#pragma once

#include "Exports.h"
#include "Shader.h"

namespace ShaderConverter
{
	////////////////////////////////////////////////////////////////////////////////
	// HLSLToGLSL:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	RENDERER_API std::string HLSLToGLSL(
		const std::string& input,
		orb::Shader::Stage stage = orb::Shader::eUndefined
	);

	////////////////////////////////////////////////////////////////////////////////
	// GLSLToSPIRV:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	RENDERER_API std::string GLSLToSPIRV(
		const std::string& input,
		orb::Shader::Stage stage = orb::Shader::eUndefined
	);

	////////////////////////////////////////////////////////////////////////////////
	// Exception:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct Exception : public std::exception
	{
		Exception(std::string str) : str(str) {}
		virtual ~Exception() throw() {}
		const char* what() const throw() { return str.c_str(); }
		std::string str;
	};
}