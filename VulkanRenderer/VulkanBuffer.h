/**
* @file VulkanBuffer.h
* @brief
*/

#pragma once

#include "Buffer.h"
#include <memory>

// Forward declarations:
namespace vk
{
	class Buffer;
	class DeviceMemory;
}

namespace orb
{
	class RENDERER_API VulkanBuffer : public Buffer
	{
		public:
			friend class VulkanRenderPass;
			friend class VulkanTexture;

			//! CTOR/DTOR:
			VulkanBuffer(Renderer* renderer);
			virtual ~VulkanBuffer();

			//! VIRTUALS:
			void Build(uint32_t size, const void* data, const CreateInfo& ci) override;
			void* Map(uint32_t size, uint32_t offset) override;
			void Unmap() override;
			void Flush() override;
			void Bind(Command* cmd) override;
			void Cleanup() override;

		protected:
			//! MEMBERS:
			std::shared_ptr<vk::Buffer> m_buffer;
			std::shared_ptr<vk::DeviceMemory> m_memory;
	};

	////////////////////////////////////////////////////////////////////////////////
	// VulkanBuffer inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline VulkanBuffer:: VulkanBuffer(Renderer* renderer)
		: Buffer(renderer) {
	}
	/*----------------------------------------------------------------------------*/
	inline VulkanBuffer::~VulkanBuffer() {
		VulkanBuffer::Cleanup(); // temp
	}
}