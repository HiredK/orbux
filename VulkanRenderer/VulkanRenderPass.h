/**
* @file VulkanRenderPass.h
* @brief
*/

#pragma once

#include "RenderPass.h"

namespace vk
{
	class CommandPool;
	class RenderPass;
	class DescriptorPool;
	class DescriptorSet;
	class PipelineLayout;
	class Pipeline;
	class Semaphore;
	class Framebuffer;
	struct Rect2D;
}

namespace orb
{
	class RENDERER_API VulkanRenderPass : public RenderPass
	{
		public:
			friend class VulkanRenderer;
			friend class VulkanCommand;

			//! CTOR/DTOR:
			VulkanRenderPass(Renderer* renderer, RenderPass* parent);
			virtual ~VulkanRenderPass();

			//! VIRTUALS:
			void Build(const CreateInfo& ci) override;
			void Resize(int w, int h) override;
			void Record() override;
			void Cleanup() override;

		protected:
			struct ThreadData
			{
				std::shared_ptr<vk::CommandPool> commandPool;
				std::vector<std::shared_ptr<Command>> commands;
			};

			//! SERVICES:
			void CreateCommandPool();
			void CreateRenderPass();
			void CreateDescriptorPool();
			void CreateDescriptorSets();
			void CreatePipeline();
			void UpdateDescriptorSets();

			//! MEMBERS:
			std::vector<ThreadData> m_threadData;
			std::shared_ptr<vk::RenderPass> m_renderPass;
			std::shared_ptr<vk::DescriptorPool> m_descriptorPool;
			std::vector<std::shared_ptr<vk::DescriptorSet>> m_descriptorSets;
			std::shared_ptr<vk::PipelineLayout> m_pipelineLayout;
			std::shared_ptr<vk::Pipeline> m_pipeline;
			std::shared_ptr<vk::Semaphore> m_semaphore;
			std::vector<std::shared_ptr<vk::Framebuffer>> m_fbos;
			std::shared_ptr<vk::Rect2D> m_renderArea;
	};

	////////////////////////////////////////////////////////////////////////////////
	// VulkanRenderPass inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline VulkanRenderPass:: VulkanRenderPass(Renderer* renderer, RenderPass* parent)
		: RenderPass(renderer, parent) {
	}
	/*----------------------------------------------------------------------------*/
	inline VulkanRenderPass::~VulkanRenderPass() {
	}

} // orb namespace