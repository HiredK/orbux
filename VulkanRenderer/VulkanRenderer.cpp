#include "VulkanRenderer.h"
#include "VulkanBuffer.h"
#include "VulkanCommand.h"
#include "VulkanRenderPass.h"
#include "VulkanTexture.h"
#include "Engine.h"
#include "Window.h"

#include "SDL/SDL.h"
#include "SDL/SDL_syswm.h"

#define VK_USE_PLATFORM_WIN32_KHR
#include <Vulkan/vulkan.hpp>

VkResult VKAPI_CALL vkCreateDebugReportCallbackEXT(
	VkInstance instance,
	const VkDebugReportCallbackCreateInfoEXT* createInfo,
	const VkAllocationCallbacks* allocator,
	VkDebugReportCallbackEXT* callback) {
	auto func = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
	return (func != nullptr) ? func(instance, createInfo, allocator, callback) : VK_ERROR_EXTENSION_NOT_PRESENT;
}

void VKAPI_CALL vkDestroyDebugReportCallbackEXT(
	VkInstance instance,
	VkDebugReportCallbackEXT callback,
	const VkAllocationCallbacks* allocator) {
	auto func = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");
	if (func != nullptr) {
		func(instance, callback, allocator);
	}
}

uint32_t orb::VulkanRenderer::GetMemoryTypeIndex(uint32_t typeBits, uint32_t memProps) const
{
	auto gpuMemoryProps = m_gpu->getMemoryProperties();
	for (uint32_t i = 0; i < gpuMemoryProps.memoryTypeCount; i++)
	{
		if ((typeBits & 1) == 1) {
			if ((gpuMemoryProps.memoryTypes[i].propertyFlags &
				vk::MemoryPropertyFlagBits(memProps)) ==
				vk::MemoryPropertyFlagBits(memProps)) {
				return i;
			}
		}

		typeBits >>= 1;
	}

	return 0;
}

vk::Format orb::VulkanRenderer::GetOptimalDepthFormat() const
{
	std::vector<vk::Format> depthFormats = {
		vk::Format::eD32SfloatS8Uint,
		vk::Format::eD32Sfloat,
		vk::Format::eD24UnormS8Uint,
		vk::Format::eD16UnormS8Uint,
		vk::Format::eD16Unorm
	};

	vk::Format optimalDepthFormat;
	for (auto& format : depthFormats)
	{
		auto depthFormatProperties = m_gpu->getFormatProperties(format);
		if (depthFormatProperties.optimalTilingFeatures & 
			vk::FormatFeatureFlagBits::eDepthStencilAttachment) {
			optimalDepthFormat = format;
			break;
		}
	}

	return optimalDepthFormat;
}

vk::Format orb::VulkanRenderer::ConvertToVkFormat(Format format)
{
	auto FormatToVkFormat = std::map<Format, vk::Format>() =
	{
		{ Format::eUndefined , vk::Format::eUndefined          },
		{ Format::eR32F      , vk::Format::eR32Sfloat          },
		{ Format::eRG32F     , vk::Format::eR32G32Sfloat       },
		{ Format::eRGB32F    , vk::Format::eR32G32B32Sfloat    },
		{ Format::eRGBA32F   , vk::Format::eR32G32B32A32Sfloat },
		{ Format::eR8        , vk::Format::eR8Unorm            },
		{ Format::eRG8       , vk::Format::eR8G8Unorm          },
		{ Format::eRGBA8     , vk::Format::eR8G8B8A8Unorm      }
	};

	return FormatToVkFormat[format];
}

std::shared_ptr<orb::Buffer> orb::VulkanRenderer::CreateBuffer(
	uint32_t size,
	const void* data,
	const Buffer::CreateInfo& ci)
{
	auto buffer = std::shared_ptr<Buffer>(new VulkanBuffer(this));
	buffer->Build(size, data, ci);
	return buffer;
}

std::shared_ptr<orb::Texture> orb::VulkanRenderer::CreateTexture(
	uint32_t w,
	uint32_t h,
	uint32_t depth,
	const void* data,
	const Texture::CreateInfo& ci)
{
	auto texture = std::shared_ptr<Texture>(new VulkanTexture(this, w, h, depth));
	texture->Build(data, ci);
	return texture;
}

orb::RenderPass* orb::VulkanRenderer::CreateRenderPass(
	RenderPass* parent,
	const RenderPass::CreateInfo& ci)
{
	RenderPass* actualParent = nullptr;
	if (!m_renderPasses.empty()) {
		actualParent = m_renderPasses.back().get();
	}

	auto renderPass = std::shared_ptr<RenderPass>(new VulkanRenderPass(this, actualParent));
	renderPass->Build(ci);

	if (ci.type == RenderPass::eDrawOnce ||
		ci.type == RenderPass::eDrawOnceBackground)
	{
		auto it = m_renderPasses.begin();
		while (it != m_renderPasses.end()) {
			if ((*it)->GetType() == RenderPass::eDrawOnce ||
				(*it)->GetType() == RenderPass::eDrawOnceBackground) {
				it++;
			}
			else {
				break;
			}
		}

		m_renderPasses.insert(it, renderPass);
	}
	else {
		m_renderPasses.push_back(renderPass);
	}

	for (unsigned int i = 0; i < m_renderPasses.size(); ++i) {
		auto renderPass = static_cast<VulkanRenderPass*>(m_renderPasses[i].get());
		if (i > 0) {
			renderPass->m_parent = m_renderPasses[i - 1].get();
		}
		else {
			renderPass->m_parent = nullptr;
		}
	}

	return renderPass.get();
}

void orb::VulkanRenderer::Initialize()
{
	VulkanRenderer::CreateInstance();
	VulkanRenderer::CreateDebugReportCallback();
	VulkanRenderer::CreateSurface();
	VulkanRenderer::CreateDevice();

	m_imageAvailableSemaphore = std::make_shared<vk::Semaphore>(
		m_device->createSemaphore(vk::SemaphoreCreateInfo())
	);

	m_renderFinishedSemaphore = std::make_shared<vk::Semaphore>(
		m_device->createSemaphore(vk::SemaphoreCreateInfo())
	);

	VulkanRenderer::CreateSwapchainCI();
}

void orb::VulkanRenderer::CreateInstance()
{
	std::vector<const char*> wantedExtensions = 
	{
		VK_KHR_SURFACE_EXTENSION_NAME,
		VK_KHR_WIN32_SURFACE_EXTENSION_NAME
	};

	auto installedExtensions = vk::enumerateInstanceExtensionProperties();
	auto extensions = std::vector<const char*>();
	auto layers = std::vector<const char*>();

	if (VulkanRenderer::m_enableDebugLayer) {
		wantedExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
		layers.push_back("VK_LAYER_LUNARG_standard_validation");
	}

	for (auto &w : wantedExtensions) {
		for (auto &i : installedExtensions) {
			if (std::string(i.extensionName).compare(w) == 0) {
				extensions.emplace_back(w);
				break;
			}
		}
	}

	m_instance = std::make_shared<vk::Instance>(
		vk::createInstance(
			vk::InstanceCreateInfo(
				vk::InstanceCreateFlags(),
				&vk::ApplicationInfo(
					"AppName",
					VK_MAKE_VERSION(1, 0, 0),
					"Orbux",
					VK_MAKE_VERSION(ENGINE_VER_MAJOR, ENGINE_VER_MINOR, ENGINE_VER_PATCH),
					VK_API_VERSION_1_0
				),
				static_cast<uint32_t>(layers.size()),
				layers.data(),
				static_cast<uint32_t>(extensions.size()),
				extensions.data()
			)
		)
	);
}

void orb::VulkanRenderer::CreateDebugReportCallback()
{
	m_debugReportCallback = std::make_shared<vk::DebugReportCallbackEXT>(
		m_instance->createDebugReportCallbackEXT(
			vk::DebugReportCallbackCreateInfoEXT(
				vk::DebugReportFlagsEXT(
					/*vk::DebugReportFlagBitsEXT::eInformation |
					vk::DebugReportFlagBitsEXT::eDebug |*/
					vk::DebugReportFlagBitsEXT::eWarning |
					vk::DebugReportFlagBitsEXT::ePerformanceWarning |
					vk::DebugReportFlagBitsEXT::eError
				),
				[](
					VkDebugReportFlagsEXT flags,
					VkDebugReportObjectTypeEXT objType,
					uint64_t obj,
					size_t location, 
					int32_t code,
					const char* layerPrefix, 
					const char* msg, 
					void* userData) -> VkBool32
				{
					printf("%s\n", msg);
					return VK_FALSE;
				},
				nullptr
			)
		)
	);
}

void orb::VulkanRenderer::CreateSurface() 
{
	m_surface = std::make_shared<vk::SurfaceKHR>(
		m_instance->createWin32SurfaceKHR(
			vk::Win32SurfaceCreateInfoKHR(
				vk::Win32SurfaceCreateFlagsKHR(),
				m_window.GetSysWMinfo()->info.win.hinstance, // HINSTANCE
				m_window.GetSysWMinfo()->info.win.window     // HWND
			)
		)
	);
}

void orb::VulkanRenderer::CreateDevice()
{
	std::vector<const char*> wantedGpuExtensions =
	{
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};

	for (auto& gpu : m_instance->enumeratePhysicalDevices())
	{
		auto props = gpu.getProperties();
		if (props.deviceType == vk::PhysicalDeviceType::eVirtualGpu) {
			continue;
		}

		m_gpu = std::make_shared<vk::PhysicalDevice>(gpu);
		m_gpuInfo.vendorID = props.vendorID;
		m_gpuInfo.deviceID = props.deviceID;
		m_gpuInfo.info = std::string(props.deviceName);
		m_graphicsFamilyIndex = 0;
		break;
	}

	auto installedGpuExtensions = m_gpu->enumerateDeviceExtensionProperties();
	auto queueCreateInfos = std::vector<vk::DeviceQueueCreateInfo>();
	auto gpuExtensions = std::vector<const char*>();
	auto gpuLayers = std::vector<const char*>();
	float priority = 0.0f;

	if (VulkanRenderer::m_enableDebugLayer) {
		wantedGpuExtensions.push_back(VK_EXT_DEBUG_MARKER_EXTENSION_NAME);
		gpuLayers.push_back("VK_LAYER_LUNARG_standard_validation");
	}

	for (auto &w : wantedGpuExtensions) {
		for (auto &i : installedGpuExtensions) {
			if (std::string(i.extensionName).compare(w) == 0) {
				gpuExtensions.emplace_back(w);
				break;
			}
		}
	}

	for (auto& queuefamily : m_gpu->getQueueFamilyProperties()) {
		if (m_gpu->getSurfaceSupportKHR(m_graphicsFamilyIndex, *m_surface) && 
			queuefamily.queueFlags & vk::QueueFlagBits::eGraphics)
		{
			queueCreateInfos.push_back(
				vk::DeviceQueueCreateInfo(
					vk::DeviceQueueCreateFlags(),
					m_graphicsFamilyIndex,
					1,
					&priority
				)
			);

			break;
		}

		m_graphicsFamilyIndex++;
	}

	m_device = std::make_shared<vk::Device>(
		m_gpu->createDevice(
			vk::DeviceCreateInfo(
				vk::DeviceCreateFlags(),
				static_cast<uint32_t>(queueCreateInfos.size()),
				queueCreateInfos.data(),
				static_cast<uint32_t>(gpuLayers.size()),
				gpuLayers.data(),
				static_cast<uint32_t>(gpuExtensions.size()),
				gpuExtensions.data(),
				&m_gpu->getFeatures()
			)
		)
	);

	m_graphicsQueue = std::make_shared<vk::Queue>(
		m_device->getQueue(m_graphicsFamilyIndex, 0)
	);
}

void orb::VulkanRenderer::CreateSwapchainCI()
{
	std::vector<std::pair<vk::PresentModeKHR, uint32_t>> PMRating
	{
		{ vk::PresentModeKHR::eImmediate  , 1 },
		{ vk::PresentModeKHR::eFifoRelaxed, 2 },
		{ vk::PresentModeKHR::eFifo       , 3 },
		{ vk::PresentModeKHR::eMailbox    , 4 }
	};

	auto surfaceExtent = m_gpu->getSurfaceCapabilitiesKHR(*m_surface).currentExtent;
	auto surfaceFormats = m_gpu->getSurfaceFormatsKHR(*m_surface);
	auto surfaceFormat = surfaceFormats[0];
	auto surfacePresentModes = m_gpu->getSurfacePresentModesKHR(*m_surface);
	auto surfacePresentMode = surfacePresentModes[0];
	uint32_t bestRating = 0u;

	if (surfaceFormats.size() == 1 && 
		surfaceFormats[0].format == vk::Format::eUndefined) {
		surfaceFormat = { 
			vk::Format::eB8G8R8A8Unorm, 
			vk::ColorSpaceKHR::eSrgbNonlinear 
		};
	}

	for (auto& mode : surfacePresentModes) {
		for (auto rating : PMRating) {
			if (mode == rating.first && rating.second > bestRating) {
				surfacePresentMode = mode;
				bestRating = rating.second;
			}
		}
	}

	m_swapchainCI = std::make_shared<vk::SwapchainCreateInfoKHR>(
		vk::SwapchainCreateInfoKHR(
			vk::SwapchainCreateFlagsKHR(),
			*m_surface,
			static_cast<uint32_t>(GetBufferCount()),
			surfaceFormat.format,
			surfaceFormat.colorSpace,
			surfaceExtent,
			1,
			vk::ImageUsageFlagBits::eColorAttachment,
			vk::SharingMode::eExclusive,
			0,
			nullptr,
			vk::SurfaceTransformFlagBitsKHR::eIdentity,
			vk::CompositeAlphaFlagBitsKHR::eOpaque,
			surfacePresentMode,
			true,
			vk::SwapchainKHR()
		)
	);

	VulkanRenderer::Resize(
		static_cast<int>(surfaceExtent.width),
		static_cast<int>(surfaceExtent.height)
	);
}

void orb::VulkanRenderer::Resize(int w, int h)
{
	m_device->waitIdle();

	if (!m_swapchainImages.empty()) {
		m_device->destroyImageView(*m_swapchainImages.front().views[SwapchainImage::eDepth]);
		for (auto& img : m_swapchainImages) {
			m_device->destroyImageView(*img.views[SwapchainImage::eColor]);
			m_device->destroyFence(*img.fence);
		}
	}

	auto surfaceCapabilities = m_gpu->getSurfaceCapabilitiesKHR(*m_surface);
	const auto oldSwapchain = m_swapchain;
	m_swapchainCI->setImageExtent(surfaceCapabilities.currentExtent);
	if (oldSwapchain) {
		m_swapchainCI->setOldSwapchain(*oldSwapchain);
	}

	m_swapchain = std::make_shared<vk::SwapchainKHR>(
		m_device->createSwapchainKHR(*m_swapchainCI)
	);

	if (oldSwapchain) {
		m_device->destroySwapchainKHR(*oldSwapchain);
	}

	auto swapchainImages = m_device->getSwapchainImagesKHR(*m_swapchain);
	m_swapchainImages.clear();

	auto depthOptimalFormat = VulkanRenderer::GetOptimalDepthFormat();
	auto depthImage = m_device->createImage(
		vk::ImageCreateInfo(
			vk::ImageCreateFlags(),
			vk::ImageType::e2D,
			depthOptimalFormat,
			vk::Extent3D(w, h, 1),
			1,
			1,
			vk::SampleCountFlagBits::e1,
			vk::ImageTiling::eOptimal,
			vk::ImageUsageFlagBits::eDepthStencilAttachment | vk::ImageUsageFlagBits::eTransferSrc,
			vk::SharingMode::eExclusive,
			1,
			&m_graphicsFamilyIndex,
			vk::ImageLayout::eUndefined
		)
	);

	auto depthMemoryReq = m_device->getImageMemoryRequirements(depthImage);
	auto depthMemory = m_device->allocateMemory(
		vk::MemoryAllocateInfo(
			depthMemoryReq.size,
			VulkanRenderer::GetMemoryTypeIndex(
				depthMemoryReq.memoryTypeBits,
				static_cast<uint32_t>(vk::MemoryPropertyFlagBits::eDeviceLocal)
			)
		)
	);

	m_device->bindImageMemory(depthImage, depthMemory, 0);
	auto depthImageView = std::make_shared<vk::ImageView>(
		m_device->createImageView(
			vk::ImageViewCreateInfo(
				vk::ImageViewCreateFlags(),
				depthImage,
				vk::ImageViewType::e2D,
				depthOptimalFormat,
				vk::ComponentMapping(),
				vk::ImageSubresourceRange(
					vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil,
					0, 1, 0, 1
				)
			)
		)
	);

	for (auto& img : swapchainImages)
	{
		SwapchainImage newImg;

		newImg.views[SwapchainImage::eColor] = std::make_shared<vk::ImageView>(
			m_device->createImageView(
				vk::ImageViewCreateInfo(
					vk::ImageViewCreateFlags(),
					img,
					vk::ImageViewType::e2D,
					m_swapchainCI->imageFormat,
					vk::ComponentMapping() =
					{
						vk::ComponentSwizzle::eR,
						vk::ComponentSwizzle::eG,
						vk::ComponentSwizzle::eB,
						vk::ComponentSwizzle::eA
					},
					vk::ImageSubresourceRange(
						vk::ImageAspectFlagBits::eColor, 
						0, 1, 0, 1
					)
				)
			)
		);

		newImg.views[SwapchainImage::eDepth] = depthImageView;

		newImg.fence = std::make_shared<vk::Fence>(
			m_device->createFence(
				vk::FenceCreateInfo(vk::FenceCreateFlagBits::eSignaled)
			)
		);

		m_swapchainImages.push_back(newImg);
	}

	for (auto& renderPass : m_renderPasses) {
		renderPass->Resize(w, h);
	}
}

void orb::VulkanRenderer::Display()
{
	m_device->acquireNextImageKHR(
		*m_swapchain,
		(std::numeric_limits<uint64_t>::max)(),
		*m_imageAvailableSemaphore,
		nullptr,
		&m_bufferIndex
	);

	auto swapchainFence = m_swapchainImages[m_bufferIndex].fence.get();
	m_device->waitForFences(1, swapchainFence, VK_TRUE, UINT64_MAX);
	m_device->resetFences(1, swapchainFence);

	for (auto& renderPass : m_renderPasses) {
		renderPass->Record();
	}

	m_device->waitIdle();

	m_graphicsQueue->presentKHR(
		vk::PresentInfoKHR(
			1,
			m_renderFinishedSemaphore.get(),
			1,
			m_swapchain.get(),
			&m_bufferIndex,
			nullptr
		)
	);

	std::vector<std::shared_ptr<RenderPass>>::const_iterator it = m_renderPasses.begin();
	while (it != m_renderPasses.end()) {
		auto renderPass = static_cast<VulkanRenderPass*>((*it).get());
		if (renderPass->m_createInfo.type == RenderPass::eDrawOnce ||
			renderPass->m_createInfo.type == RenderPass::eDrawOnceBackground) {
			(*it)->Cleanup();
			m_renderPasses.erase(it);
		}

		++it;
	}

	for (unsigned int i = 0; i < m_renderPasses.size(); ++i) {
		auto renderPass = static_cast<VulkanRenderPass*>(m_renderPasses[i].get());
		if (i > 0) {
			renderPass->m_parent = m_renderPasses[i - 1].get();
		}
		else {
			renderPass->m_parent = nullptr;
		}
	}
}

void orb::VulkanRenderer::Cleanup()
{
	for (auto& renderPass : m_renderPasses) {
		renderPass->Cleanup();
	}

	m_renderPasses.clear();
}