/**
* @file VulkanRenderer.h
* @brief
*/

#pragma once

#include "Renderer.h"
#include <array>

// Forward declarations:
namespace vk
{
	class Instance;
	class DebugReportCallbackEXT;
	class SurfaceKHR;
	class PhysicalDevice;
	class Device;
	class Queue;
	struct SwapchainCreateInfoKHR;
	class SwapchainKHR;
	class ImageView;
	class Fence;
	class Semaphore;
	enum class Format;
}

namespace orb
{
	class RENDERER_API VulkanRenderer : public Renderer
	{
		public:
			friend class VulkanBuffer;
			friend class VulkanCommand;
			friend class VulkanRenderPass;
			friend class VulkanTexture;

			//! CTOR/DTOR:
			VulkanRenderer(const Window& window, BufferingType buffering = eTriple);
			virtual ~VulkanRenderer();

			////////////////////////////////////////////////////////////////////////
			// VulkanRenderer::CreateBuffer:
			////////////////////////////////////////////////////////////////////////
			/*--------------------------------------------------------------------*/
			std::shared_ptr<Buffer> CreateBuffer(
				uint32_t size,
				const void* data,
				const Buffer::CreateInfo& ci
			) override;

			////////////////////////////////////////////////////////////////////////
			// VulkanRenderer::CreateTexture:
			////////////////////////////////////////////////////////////////////////
			/*--------------------------------------------------------------------*/
			std::shared_ptr<Texture> CreateTexture(
				uint32_t w,
				uint32_t h,
				uint32_t depth,
				const void* data,
				const Texture::CreateInfo& ci
			) override;

			////////////////////////////////////////////////////////////////////////
			// VulkanRenderer::CreateRenderPass:
			////////////////////////////////////////////////////////////////////////
			/*--------------------------------------------------------------------*/
			RenderPass* CreateRenderPass(
				RenderPass* parent,
				const RenderPass::CreateInfo& ci
			) override;

			void Initialize() override;
			void Resize(int w, int h) override;
			void Display() override;
			void Cleanup() override;
			Type GetType() const override;

			//! SERVICES:
			uint32_t GetMemoryTypeIndex(uint32_t typeBits, uint32_t memProps) const;
			vk::Format GetOptimalDepthFormat() const;
			static vk::Format ConvertToVkFormat(Format format);

		protected:
			struct SwapchainImage
			{
				enum { eColor, eDepth };
				std::array<std::shared_ptr<vk::ImageView>, 2> views;
				std::shared_ptr<vk::Fence> fence;
			};

			//! SERVICES:
			void CreateInstance();
			void CreateDebugReportCallback();
			void CreateSurface();
			void CreateDevice();
			void CreateSwapchainCI();

			//! MEMBERS:
			std::shared_ptr<vk::Instance> m_instance;
			std::shared_ptr<vk::DebugReportCallbackEXT> m_debugReportCallback;
			std::shared_ptr<vk::SurfaceKHR> m_surface;
			std::shared_ptr<vk::PhysicalDevice> m_gpu;
			std::shared_ptr<vk::Device> m_device;
			std::shared_ptr<vk::Queue> m_graphicsQueue;
			std::shared_ptr<vk::SwapchainCreateInfoKHR> m_swapchainCI;
			std::shared_ptr<vk::SwapchainKHR> m_swapchain;
			std::vector<SwapchainImage> m_swapchainImages;
			std::shared_ptr<vk::Semaphore> m_imageAvailableSemaphore;
			std::shared_ptr<vk::Semaphore> m_renderFinishedSemaphore;
			uint32_t m_graphicsFamilyIndex;
			bool m_enableDebugLayer;
	};

	////////////////////////////////////////////////////////////////////////////////
	// VulkanRenderer inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline VulkanRenderer:: VulkanRenderer(const Window& window, BufferingType buffering)
		: Renderer(window, buffering)
		, m_graphicsFamilyIndex(0)
		, m_enableDebugLayer(true) {
	}
	/*----------------------------------------------------------------------------*/
	inline VulkanRenderer::~VulkanRenderer() {
	}

	/*----------------------------------------------------------------------------*/
	inline Renderer::Type VulkanRenderer::GetType() const {
		return eVulkan;
	}
}