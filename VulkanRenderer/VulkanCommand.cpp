#include "VulkanCommand.h"
#include "VulkanRenderer.h"
#include "VulkanRenderPass.h"
#include <Vulkan/vulkan.hpp>

void orb::VulkanCommand::Build()
{
	auto device = static_cast<VulkanRenderer*>(m_parent->GetRenderer())->m_device.get();
	m_cmdBuffer = std::make_shared<vk::CommandBuffer>(
		device->allocateCommandBuffers(
			vk::CommandBufferAllocateInfo(
				*m_commandPool,
				vk::CommandBufferLevel::ePrimary,
				1
			)
		)[0]
	);
}

void orb::VulkanCommand::Viewport(float x, float y, float w, float h, float minDepth, float maxDepth)
{
	auto viewport = vk::Viewport(x, y, w, h, minDepth, maxDepth);
	m_cmdBuffer->setViewport(0, viewport);
}

void orb::VulkanCommand::Scissor(int x, int y, int w, int h)
{
	auto x2 = (int32_t)(x) > 0 ? (int32_t)(x) : 0;
	auto y2 = (int32_t)(y) > 0 ? (int32_t)(y) : 0;
	auto w2 = (uint32_t)(w - x);
	auto h2 = (uint32_t)(h - y + 1); // FIXME: Why +1 here?

	auto scissor = vk::Rect2D(vk::Offset2D(x2, y2), vk::Extent2D(w2, h2));
	m_cmdBuffer->setScissor(0, scissor);
}

void orb::VulkanCommand::Test(unsigned int offset)
{
	auto pipelineLayout = static_cast<VulkanRenderPass*>(m_parent)->m_pipelineLayout.get();
	auto descriptorSet = static_cast<VulkanRenderPass*>(m_parent)->m_descriptorSets[0].get();

	std::vector<uint32_t> offsets;
	for (auto& ubo : static_cast<VulkanRenderPass*>(m_parent)->m_createInfo.ubos) {
		if (ubo->IsDynamic()) {
			offsets.push_back(offset);
		}
	}

	m_cmdBuffer->bindDescriptorSets(
		vk::PipelineBindPoint::eGraphics,      // pipelineBindPoint
		*pipelineLayout,                       // layout
		0,                                     // firstSet
		1,                                     // descriptorSetCount
		descriptorSet,                         // descriptorSets
		static_cast<uint32_t>(offsets.size()), // dynamicOffsetCount
		offsets.data()                         // dynamicOffsets
	);
}

void orb::VulkanCommand::Draw(
	uint32_t vertexCount,
	uint32_t instanceCount,
	uint32_t firstVertex,
	uint32_t firstInstance)
{
	m_cmdBuffer->draw(vertexCount, instanceCount, firstVertex, firstInstance);
}

void orb::VulkanCommand::DrawIndexed(
	uint32_t indexCount, 
	uint32_t instanceCount, 
	uint32_t firstIndex, 
	int32_t vertexOffset, 
	uint32_t firstInstance)
{
	m_cmdBuffer->drawIndexed(indexCount, instanceCount, firstIndex, vertexOffset, firstInstance);
}

void orb::VulkanCommand::Cleanup()
{
	auto device = static_cast<VulkanRenderer*>(m_parent->GetRenderer())->m_device.get();
	device->freeCommandBuffers(
		*m_commandPool,
		1,
		m_cmdBuffer.get()
	);
}