/**
* @file VulkanCommand.h
* @brief
*/

#pragma once

#include "Command.h"
#include <memory>

// Forward declarations:
namespace vk
{
	class CommandBuffer;
	class CommandPool;
}

namespace orb
{
	class RENDERER_API VulkanCommand : public Command
	{
		public:
			friend class VulkanBuffer;
			friend class VulkanRenderPass;

			//! CTOR/DTOR:
			VulkanCommand(RenderPass* parent, vk::CommandPool* commandPool);
			virtual ~VulkanCommand();

			//! VIRTUALS:
			void Build() override;
			void Viewport(float x, float y, float w, float h, float minDepth = 0.0f, float maxDepth = 1.0f) override;
			void Scissor(int x, int y, int w, int h) override;
			void Test(unsigned int offset) override;

			void Draw(
				uint32_t vertexCount,
				uint32_t instanceCount,
				uint32_t firstVertex,
				uint32_t firstInstance
			) override;

			void DrawIndexed(
				uint32_t indexCount,
				uint32_t instanceCount, 
				uint32_t firstIndex, 
				int32_t vertexOffset, 
				uint32_t firstInstance
			) override;

			void Cleanup() override;

		protected:
			//! MEMBERS:
			std::shared_ptr<vk::CommandBuffer> m_cmdBuffer;
			vk::CommandPool* m_commandPool;
	};

	////////////////////////////////////////////////////////////////////////////////
	// VulkanCommand inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline VulkanCommand:: VulkanCommand(RenderPass* parent, vk::CommandPool* commandPool)
		: Command(parent), m_commandPool(commandPool) {
	}
	/*----------------------------------------------------------------------------*/
	inline VulkanCommand::~VulkanCommand() {
	}
}