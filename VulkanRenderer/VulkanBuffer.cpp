#include "VulkanBuffer.h"
#include "VulkanCommand.h"
#include "VulkanRenderer.h"
#include <Vulkan/vulkan.hpp>

void orb::VulkanBuffer::Build(uint32_t size, const void* data, const CreateInfo& ci)
{
	auto ConvertUsageFlags = [](uint32_t flags) -> vk::BufferUsageFlags
	{
		vk::BufferUsageFlags result;
		if (flags & eTransferSrc)  result |= vk::BufferUsageFlagBits::eTransferSrc;
		if (flags & eTransferDst)  result |= vk::BufferUsageFlagBits::eTransferDst;
		if (flags & eUniformTexel) result |= vk::BufferUsageFlagBits::eUniformTexelBuffer;
		if (flags & eStorageTexel) result |= vk::BufferUsageFlagBits::eStorageTexelBuffer;
		if (flags & eUniform)      result |= vk::BufferUsageFlagBits::eUniformBuffer;
		if (flags & eStorage)      result |= vk::BufferUsageFlagBits::eStorageBuffer;
		if (flags & eIndex)        result |= vk::BufferUsageFlagBits::eIndexBuffer;
		if (flags & eVertex)       result |= vk::BufferUsageFlagBits::eVertexBuffer;
		if (flags & eIndirect)     result |= vk::BufferUsageFlagBits::eIndirectBuffer;
		return result;
	};

	auto ConvertMemoryProperty = [](uint32_t flags) -> uint32_t
	{
		vk::MemoryPropertyFlags result;
		if (flags & eDeviceLocal)  result |= vk::MemoryPropertyFlagBits::eDeviceLocal;
		if (flags & eHostVisible)  result |= vk::MemoryPropertyFlagBits::eHostVisible;
		if (flags & eHostCoherent) result |= vk::MemoryPropertyFlagBits::eHostCoherent;
		if (flags & eHostCached)   result |= vk::MemoryPropertyFlagBits::eHostCached;
		return static_cast<uint32_t>(result);
	};

	auto device = static_cast<VulkanRenderer*>(m_renderer)->m_device.get();
	auto graphicsFamilyIndex = static_cast<VulkanRenderer*>(m_renderer)->m_graphicsFamilyIndex;
	m_usage = ci.usage;
	m_size = size;
	m_isDynamic = ci.dynamic;

	m_buffer = std::make_shared<vk::Buffer>(
		device->createBuffer(
			vk::BufferCreateInfo(
				vk::BufferCreateFlags(),
				size,
				ConvertUsageFlags(ci.usage),
				vk::SharingMode::eExclusive,
				1,
				&graphicsFamilyIndex
			)
		)
	);

	auto memReqs = device->getBufferMemoryRequirements(*m_buffer);
	auto memTypeIndex = static_cast<VulkanRenderer*>(m_renderer)->GetMemoryTypeIndex(
		memReqs.memoryTypeBits,
		ConvertMemoryProperty(ci.memProps)
	);

	m_memory = std::make_shared<vk::DeviceMemory>(
		device->allocateMemory(vk::MemoryAllocateInfo(
			memReqs.size,
			memTypeIndex
		))
	);

	device->bindBufferMemory(*m_buffer, *m_memory, 0);

	if (ci.usage & eTransferDst)
	{
		auto stagingBuffer = m_renderer->CreateBuffer(size, data, Buffer::CreateInfo::GetStaging());
		auto graphicsQueue = static_cast<VulkanRenderer*>(m_renderer)->m_graphicsQueue.get();
		auto srcBuffer = static_cast<VulkanBuffer*>(stagingBuffer.get())->m_buffer.get();

		auto stagingCmdPool = std::make_shared<vk::CommandPool>(
			device->createCommandPool(vk::CommandPoolCreateInfo(
				vk::CommandPoolCreateFlags(vk::CommandPoolCreateFlagBits::eResetCommandBuffer),
				static_cast<VulkanRenderer*>(m_renderer)->m_graphicsFamilyIndex
			))
		);

		auto stagingCmd = device->allocateCommandBuffers(
			vk::CommandBufferAllocateInfo(
				*stagingCmdPool,
				vk::CommandBufferLevel::ePrimary,
				1
			)
		)[0];

		stagingCmd.begin(vk::CommandBufferBeginInfo());
		stagingCmd.copyBuffer(*srcBuffer, *m_buffer, { vk::BufferCopy(0, 0, size) });
		stagingCmd.end();

		std::vector<vk::SubmitInfo> submitInfos = {
			vk::SubmitInfo(0, nullptr, nullptr, 1, &stagingCmd, 0, nullptr)
		};

		auto stagingFence = device->createFence(vk::FenceCreateInfo());
		graphicsQueue->submit(submitInfos, stagingFence);
		device->waitForFences(1, &stagingFence, VK_TRUE, UINT64_MAX);
		device->destroyFence(stagingFence);
		device->freeCommandBuffers(*stagingCmdPool, 1, &stagingCmd);
	}
	else if (data)
	{
		void* mapped = VulkanBuffer::Map(size, 0);
		memcpy(mapped, data, size);
		VulkanBuffer::Unmap();
	}
}

void* orb::VulkanBuffer::Map(uint32_t size, uint32_t offset)
{
	auto device = static_cast<VulkanRenderer*>(m_renderer)->m_device.get();
	m_mapped = device->mapMemory(*m_memory, offset, size);

	m_isMapped = true;
	return m_mapped;
}

void orb::VulkanBuffer::Unmap()
{
	auto device = static_cast<VulkanRenderer*>(m_renderer)->m_device.get();
	device->unmapMemory(*m_memory);
	m_isMapped = false;
}

void orb::VulkanBuffer::Flush()
{
	auto device = static_cast<VulkanRenderer*>(m_renderer)->m_device.get();

	vk::MappedMemoryRange range;
	range.memory = *m_memory;
	range.offset = 0;
	range.size = VK_WHOLE_SIZE;
	device->flushMappedMemoryRanges(1, &range);
}

void orb::VulkanBuffer::Bind(Command* cmd)
{
	auto cmdBuffer = static_cast<VulkanCommand*>(cmd)->m_cmdBuffer.get();

	if (m_usage & Buffer::eVertex) {
		std::array<vk::DeviceSize, 1> offsets = { 0 };
		cmdBuffer->bindVertexBuffers(0, 1, m_buffer.get(), offsets.data());
	}

	if (m_usage & Buffer::eIndex) {
		cmdBuffer->bindIndexBuffer(*m_buffer, 0, vk::IndexType::eUint16);
	}
}

void orb::VulkanBuffer::Cleanup()
{
	auto device = static_cast<VulkanRenderer*>(m_renderer)->m_device.get();
	device->freeMemory(*m_memory);
	device->destroyBuffer(*m_buffer);
}