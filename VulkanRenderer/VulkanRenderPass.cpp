#include "VulkanRenderPass.h"
#include "VulkanBuffer.h"
#include "VulkanCommand.h"
#include "VulkanRenderer.h"
#include "VulkanTexture.h"
#include "Shader.h"
#include "Window.h"

#include <Vulkan/vulkan.hpp>

void orb::VulkanRenderPass::Build(const CreateInfo& ci)
{
	m_createInfo = ci;

	if (ci.type == eDrawOnceBackground ||
		ci.type == eDrawSyncBackground) {
		m_threadPool.SetThreadCount(ci.numThreads);
	}

	VulkanRenderPass::CreateCommandPool();
	VulkanRenderPass::CreateRenderPass();
	VulkanRenderPass::CreateDescriptorPool();
	VulkanRenderPass::CreateDescriptorSets();
	VulkanRenderPass::CreatePipeline();

	m_cameraUBO = m_renderer->CreateBuffer(
		sizeof(CameraData),
		&m_cameraData,
		Buffer::CreateInfo() = {
			Buffer::eUniform, 
			Buffer::eHostVisible,
			false
		}
	);

	ci.camera->onRebuild.Connect([&](const Camera::Matrices* data)
	{
		const glm::mat4 clipMatrix
		(
			1.0f, 0.0f, 0.0f, 0.0f,
			0.0f,-1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 1.0f
		);

		m_cameraData.proj = glm::rowMajor4(data->proj);
		m_cameraData.view = glm::rowMajor4(clipMatrix * (mat4)data->view);
		m_cameraData.viewProj = glm::rowMajor4((mat4)data->viewProj);
		m_cameraData.invProj = glm::rowMajor4(glm::inverse((mat4)data->proj));
		m_cameraData.invView = glm::rowMajor4(glm::inverse(clipMatrix * (mat4)data->view));
		m_cameraData.viewport = vec2(m_renderArea->extent.width, m_renderArea->extent.height);

		memcpy(m_cameraUBO->GetMappedMemory(),
			&m_cameraData, sizeof(CameraData)
		);
	});

	VulkanRenderPass::UpdateDescriptorSets();
	VulkanRenderPass::Resize(
		static_cast<int>(ci.resolution.x), 
		static_cast<int>(ci.resolution.y)
	);
}

void orb::VulkanRenderPass::CreateCommandPool()
{
	auto device = static_cast<VulkanRenderer*>(m_renderer)->m_device.get();
	if (m_createInfo.type == eDrawOnceBackground ||
		m_createInfo.type == eDrawSyncBackground) {
		for (uint32_t t = 0; t < m_threadPool.GetThreadCount(); ++t)
		{
			ThreadData data;
			data.commandPool = std::make_shared<vk::CommandPool>(
				device->createCommandPool(
					vk::CommandPoolCreateInfo(
						vk::CommandPoolCreateFlags(vk::CommandPoolCreateFlagBits::eResetCommandBuffer),
						static_cast<VulkanRenderer*>(m_renderer)->m_graphicsFamilyIndex
					)
				)
			);

			auto newCmd = std::shared_ptr<Command>(new VulkanCommand(this, data.commandPool.get()));
			newCmd->Build();

			data.commands.push_back(newCmd);
			m_threadData.push_back(data);
		}
	}
	else {
		ThreadData data;
		data.commandPool = std::make_shared<vk::CommandPool>(
			device->createCommandPool(
				vk::CommandPoolCreateInfo(
					vk::CommandPoolCreateFlags(vk::CommandPoolCreateFlagBits::eResetCommandBuffer),
					static_cast<VulkanRenderer*>(m_renderer)->m_graphicsFamilyIndex
				)
			)
		);

		for (uint32_t i = 0; i < m_renderer->GetBufferCount(); ++i) {
			auto newCmd = std::shared_ptr<Command>(new VulkanCommand(this, data.commandPool.get()));
			newCmd->Build();

			data.commands.push_back(newCmd);
		}

		m_threadData.push_back(data);
	}
}

void orb::VulkanRenderPass::CreateRenderPass()
{
	auto device = static_cast<VulkanRenderer*>(m_renderer)->m_device.get();
	std::vector<vk::AttachmentDescription> attachmentDescs;
	std::vector<vk::AttachmentReference> colorReferences;
	std::vector<vk::AttachmentReference> depthReferences;
	std::vector<vk::SubpassDescription> subpassDescs;
	std::vector<vk::SubpassDependency> subpassDependencies;

	auto AddColorAttachment = [&](uint32_t index, vk::Format format, vk::ImageLayout finalLayout)
	{
		bool temp = m_createInfo.type == RenderPass::eDrawOnceBackground; //TEMP!

		attachmentDescs.push_back(
			vk::AttachmentDescription(
				vk::AttachmentDescriptionFlags(),
				format,
				vk::SampleCountFlagBits::e1,
				(temp) ? vk::AttachmentLoadOp::eDontCare : vk::AttachmentLoadOp::eClear,
				vk::AttachmentStoreOp::eStore,
				vk::AttachmentLoadOp::eDontCare,
				vk::AttachmentStoreOp::eDontCare,
				vk::ImageLayout::eUndefined,
				finalLayout
			)
		);

		colorReferences.push_back(
			vk::AttachmentReference(
				index,
				vk::ImageLayout::eColorAttachmentOptimal
			)
		);
	};

	for (uint32_t i = 0; i < m_createInfo.attachments.size(); ++i) {
		auto format = VulkanRenderer::ConvertToVkFormat(m_createInfo.attachments[i].input->GetFormat());
		AddColorAttachment(i, format, vk::ImageLayout::eShaderReadOnlyOptimal);
	}

	if (m_createInfo.attachments.empty()) {
		auto format = static_cast<VulkanRenderer*>(m_renderer)->m_swapchainCI->imageFormat;
		AddColorAttachment(0, format, vk::ImageLayout::ePresentSrcKHR);

		if (m_createInfo.enableDepth)
		{
			attachmentDescs.push_back(
				vk::AttachmentDescription(
					vk::AttachmentDescriptionFlags(),
					static_cast<VulkanRenderer*>(m_renderer)->GetOptimalDepthFormat(),
					vk::SampleCountFlagBits::e1,
					vk::AttachmentLoadOp::eClear,
					vk::AttachmentStoreOp::eDontCare,
					vk::AttachmentLoadOp::eDontCare,
					vk::AttachmentStoreOp::eDontCare,
					vk::ImageLayout::eUndefined,
					vk::ImageLayout::eDepthStencilAttachmentOptimal
				)
			);

			depthReferences.push_back(
				vk::AttachmentReference(1, vk::ImageLayout::eDepthStencilAttachmentOptimal)
			);
		}
	}

	subpassDescs.push_back(
		vk::SubpassDescription(
			vk::SubpassDescriptionFlags(),
			vk::PipelineBindPoint::eGraphics,
			0,
			nullptr,
			static_cast<uint32_t>(colorReferences.size()),
			colorReferences.data(),
			nullptr,
			depthReferences.data(),
			0,
			nullptr
		)
	);

	subpassDependencies.push_back(
		vk::SubpassDependency(
			VK_SUBPASS_EXTERNAL,
			0,
			vk::PipelineStageFlagBits::eBottomOfPipe,
			vk::PipelineStageFlagBits::eColorAttachmentOutput,
			vk::AccessFlagBits::eMemoryRead,
			vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite,
			vk::DependencyFlagBits::eByRegion
		)
	);

	subpassDependencies.push_back(
		vk::SubpassDependency(
			0,
			VK_SUBPASS_EXTERNAL,
			vk::PipelineStageFlagBits::eColorAttachmentOutput,
			vk::PipelineStageFlagBits::eBottomOfPipe,
			vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite,
			vk::AccessFlagBits::eMemoryRead,
			vk::DependencyFlagBits::eByRegion
		)
	);

	m_renderPass = std::make_shared<vk::RenderPass>(
		device->createRenderPass(
			vk::RenderPassCreateInfo(
				vk::RenderPassCreateFlags(),
				static_cast<uint32_t>(attachmentDescs.size()),
				attachmentDescs.data(),
				static_cast<uint32_t>(subpassDescs.size()),
				subpassDescs.data(),
				static_cast<uint32_t>(subpassDependencies.size()),
				subpassDependencies.data()
			)
		)
	);
}

void orb::VulkanRenderPass::CreateDescriptorPool()
{
	std::vector<vk::DescriptorPoolSize> descriptorPoolSizes = 
	{
		vk::DescriptorPoolSize(vk::DescriptorType::eUniformBuffer, 4),
		vk::DescriptorPoolSize(vk::DescriptorType::eUniformBufferDynamic, 4),
		vk::DescriptorPoolSize(vk::DescriptorType::eCombinedImageSampler, 8)
	};

	auto device = static_cast<VulkanRenderer*>(m_renderer)->m_device.get();
	m_descriptorPool = std::make_shared<vk::DescriptorPool>(
		device->createDescriptorPool(
			vk::DescriptorPoolCreateInfo(
				vk::DescriptorPoolCreateFlags(),
				1,
				static_cast<uint32_t>(descriptorPoolSizes.size()),
				descriptorPoolSizes.data()
			)
		)
	);
}

void orb::VulkanRenderPass::CreateDescriptorSets()
{
	auto device = static_cast<VulkanRenderer*>(m_renderer)->m_device.get();
	uint32_t currentBindingIndex = 0;

	// Camera Buffer (0):
	std::vector<vk::DescriptorSetLayoutBinding> layoutBindings = {
		vk::DescriptorSetLayoutBinding(
			currentBindingIndex++,
			vk::DescriptorType::eUniformBuffer,
			1,
			vk::ShaderStageFlagBits::eAll
		)
	};

	for (uint32_t i = 0; i < m_createInfo.ubos.size(); ++i) {

		// Uniform Buffer (Camera Buffer + i):
		layoutBindings.push_back(
			vk::DescriptorSetLayoutBinding(
				currentBindingIndex++,
				(m_createInfo.ubos[i]->IsDynamic()) ? vk::DescriptorType::eUniformBufferDynamic : vk::DescriptorType::eUniformBuffer,
				1,
				vk::ShaderStageFlagBits::eAll
			)
		);
	}

	for (uint32_t i = 0; i < m_createInfo.samplers.size(); ++i) {

		// Image Sampler (Camera Buffer + UBOs + i):
		layoutBindings.push_back(
			vk::DescriptorSetLayoutBinding(
				currentBindingIndex++,
				vk::DescriptorType::eCombinedImageSampler,
				1,
				vk::ShaderStageFlagBits::eAll
			)
		);
	}

	std::vector<vk::DescriptorSetLayout> layouts = {
		device->createDescriptorSetLayout(
			vk::DescriptorSetLayoutCreateInfo(
				vk::DescriptorSetLayoutCreateFlags(),
				static_cast<uint32_t>(layoutBindings.size()),
				layoutBindings.data()
			)
		)
	};

	for (auto& layout : layouts) {
		m_descriptorSets.push_back(
			std::make_shared<vk::DescriptorSet>(
				device->allocateDescriptorSets(
					vk::DescriptorSetAllocateInfo(
						*m_descriptorPool,
						1,
						&layout
					)
				)[0]
			)
		);
	}

	m_pipelineLayout = std::make_shared<vk::PipelineLayout>(
		device->createPipelineLayout(
			vk::PipelineLayoutCreateInfo(
				vk::PipelineLayoutCreateFlags(),
				static_cast<uint32_t>(layouts.size()),
				layouts.data(),
				0,
				nullptr
			)
		)
	);
}

void orb::VulkanRenderPass::CreatePipeline()
{
	auto ConvertToVkBlending = [](const Blending& input) -> vk::PipelineColorBlendAttachmentState
	{
		auto BlendFactorToVulkan = std::map<BlendFactor, vk::BlendFactor>() =
		{
			{ eZero                  , vk::BlendFactor::eZero                  },
			{ eOne                   , vk::BlendFactor::eOne                   },
			{ eSrcColor              , vk::BlendFactor::eSrcColor              },
			{ eOneMinusSrcColor      , vk::BlendFactor::eOneMinusSrcColor      },
			{ eDstColor              , vk::BlendFactor::eDstColor              },
			{ eOneMinusDstColor      , vk::BlendFactor::eOneMinusDstColor      },
			{ eSrcAlpha              , vk::BlendFactor::eSrcAlpha              },
			{ eOneMinusSrcAlpha      , vk::BlendFactor::eOneMinusSrcAlpha      },
			{ eDstAlpha              , vk::BlendFactor::eDstAlpha              },
			{ eOneMinusDstAlpha      , vk::BlendFactor::eOneMinusDstAlpha      },
			{ eConstantColor         , vk::BlendFactor::eConstantColor         },
			{ eOneMinusConstantColor , vk::BlendFactor::eOneMinusConstantColor },
			{ eConstantAlpha         , vk::BlendFactor::eConstantAlpha         },
			{ eOneMinusConstantAlpha , vk::BlendFactor::eOneMinusConstantAlpha },
			{ eSrcAlphaSaturate      , vk::BlendFactor::eSrcAlphaSaturate      },
			{ eSrc1Color             , vk::BlendFactor::eSrc1Color             },
			{ eOneMinusSrc1Color     , vk::BlendFactor::eOneMinusSrc1Color     },
			{ eSrc1Alpha             , vk::BlendFactor::eSrc1Alpha             },
			{ eOneMinusSrc1Alpha     , vk::BlendFactor::eOneMinusSrc1Alpha     }
		};

		auto BlendOpToVulkan = std::map<BlendOp, vk::BlendOp>() =
		{
			{ eAdd                   , vk::BlendOp::eAdd                       },
			{ eSubtract              , vk::BlendOp::eSubtract                  },
			{ eReverseSubtract       , vk::BlendOp::eReverseSubtract           },
			{ eMin                   , vk::BlendOp::eMin                       },
			{ eMax                   , vk::BlendOp::eMax                       }
		};

		vk::PipelineColorBlendAttachmentState vkBlending;
		vkBlending.blendEnable = input.blendEnable;
		vkBlending.srcColorBlendFactor = BlendFactorToVulkan[input.srcColorBlendFactor];
		vkBlending.dstColorBlendFactor = BlendFactorToVulkan[input.dstColorBlendFactor];
		vkBlending.colorBlendOp = BlendOpToVulkan[input.colorBlendOp];
		vkBlending.srcAlphaBlendFactor = BlendFactorToVulkan[input.srcAlphaBlendFactor];
		vkBlending.dstAlphaBlendFactor = BlendFactorToVulkan[input.dstAlphaBlendFactor];
		vkBlending.alphaBlendOp = BlendOpToVulkan[input.alphaBlendOp];
		vkBlending.colorWriteMask = vk::ColorComponentFlags(
			vk::ColorComponentFlagBits::eR |
			vk::ColorComponentFlagBits::eG |
			vk::ColorComponentFlagBits::eB |
			vk::ColorComponentFlagBits::eA
		);

		return vkBlending;
	};

	auto TopologyToVulkan = std::map<Mesh::Topology, vk::PrimitiveTopology>() =
	{
		{ Mesh::ePointList                  , vk::PrimitiveTopology::ePointList                  },
		{ Mesh::eLineList                   , vk::PrimitiveTopology::eLineList                   },
		{ Mesh::eLineStrip                  , vk::PrimitiveTopology::eLineStrip                  },
		{ Mesh::eTriangleList               , vk::PrimitiveTopology::eTriangleList               },
		{ Mesh::eTriangleStrip              , vk::PrimitiveTopology::eTriangleStrip              },
		{ Mesh::eTriangleFan                , vk::PrimitiveTopology::eTriangleFan                },
		{ Mesh::eLineListWithAdjacency      , vk::PrimitiveTopology::eLineListWithAdjacency      },
		{ Mesh::eLineStripWithAdjacency     , vk::PrimitiveTopology::eLineStripWithAdjacency     },
		{ Mesh::eTriangleListWithAdjacency  , vk::PrimitiveTopology::eTriangleListWithAdjacency  },
		{ Mesh::eTriangleStripWithAdjacency , vk::PrimitiveTopology::eTriangleStripWithAdjacency },
		{ Mesh::eTriangleStripWithAdjacency , vk::PrimitiveTopology::eTriangleStripWithAdjacency },
		{ Mesh::ePatchList                  , vk::PrimitiveTopology::ePatchList                  }
	};

	auto StageToVulkan = std::map<Shader::Stage, vk::ShaderStageFlagBits>() =
	{
		{ Shader::eVertex                   , vk::ShaderStageFlagBits::eVertex                   },
		{ Shader::eTessControl              , vk::ShaderStageFlagBits::eTessellationControl      },
		{ Shader::eTessEvaluation           , vk::ShaderStageFlagBits::eTessellationEvaluation   },
		{ Shader::eGeometry                 , vk::ShaderStageFlagBits::eGeometry                 },
		{ Shader::eFragment                 , vk::ShaderStageFlagBits::eFragment                 },
		{ Shader::eCompute                  , vk::ShaderStageFlagBits::eCompute                  }
	};

	auto device = static_cast<VulkanRenderer*>(m_renderer)->m_device.get();
	std::vector<vk::VertexInputAttributeDescription> vertexInputAttributeDescs;
	std::vector<vk::PipelineColorBlendAttachmentState> colorBlendAttachments;
	std::vector<vk::PipelineShaderStageCreateInfo> pipelineStages;
	auto pipelineCache = device->createPipelineCache(vk::PipelineCacheCreateInfo());
	std::vector<vk::DynamicState> dynamicStates = {
		vk::DynamicState::eViewport,
		vk::DynamicState::eScissor
	};

	for (auto& input : m_createInfo.vertexInput) {
		vertexInputAttributeDescs.push_back(
			vk::VertexInputAttributeDescription(
				input.first,
				0,
				VulkanRenderer::ConvertToVkFormat(input.second.format),
				input.second.offset
			)
		);
	}

	for (auto& attachment : m_createInfo.attachments) {
		colorBlendAttachments.push_back(
			ConvertToVkBlending(attachment.blending)
		);
	}

	if (m_createInfo.attachments.empty()) {
		colorBlendAttachments.push_back(
			ConvertToVkBlending(Blending::GetDefault())
		);
	}

	std::map<Shader::Stage, vk::ShaderModule> shaderModules;
	for (unsigned int i = 0; i < Shader::eStageNum; ++i) {
		auto stage = static_cast<Shader::Stage>(i);
		if (m_createInfo.shader->Exists(stage, Shader::eSPIRV)) {
			auto stageData = m_createInfo.shader->GetStageData(stage, Shader::eSPIRV);
			shaderModules[stage] = device->createShaderModule(
				vk::ShaderModuleCreateInfo(
					vk::ShaderModuleCreateFlags(),
					stageData.size(),
					(uint32_t*)stageData.data()
				)
			);
		}
	}

	for (auto& shaderModule : shaderModules) {
		pipelineStages.push_back(
			vk::PipelineShaderStageCreateInfo(
				vk::PipelineShaderStageCreateFlags(),
				StageToVulkan[shaderModule.first],
				shaderModule.second,
				"main",
				nullptr
			)
		);
	}

	m_pipeline = std::make_shared<vk::Pipeline>(
		device->createGraphicsPipeline(pipelineCache,
			vk::GraphicsPipelineCreateInfo(
				vk::PipelineCreateFlags(),
				static_cast<uint32_t>(pipelineStages.size()),
				pipelineStages.data(),
				&vk::PipelineVertexInputStateCreateInfo(
					vk::PipelineVertexInputStateCreateFlags(),
					1,
					&vk::VertexInputBindingDescription(
						0,
						m_createInfo.vertexStride,
						vk::VertexInputRate::eVertex
					),
					static_cast<uint32_t>(vertexInputAttributeDescs.size()),
					vertexInputAttributeDescs.data()
				),
				&vk::PipelineInputAssemblyStateCreateInfo(
					vk::PipelineInputAssemblyStateCreateFlags(),
					TopologyToVulkan[m_createInfo.topology]
				),
				&vk::PipelineTessellationStateCreateInfo(
					vk::PipelineTessellationStateCreateFlags(),
					(m_createInfo.topology == Mesh::ePatchList) ? 4 : 0
				),
				&vk::PipelineViewportStateCreateInfo(
					vk::PipelineViewportStateCreateFlagBits(),
					1, 
					&vk::Viewport(
						0.0f, 
						0.0f, 
						(float)m_createInfo.resolution.x,
						(float)m_createInfo.resolution.y,
						0.0f,
						1.0f
					),
					1, 
					&vk::Rect2D(
						vk::Offset2D(),
						vk::Extent2D(
							m_createInfo.resolution.x,
							m_createInfo.resolution.x
						)
					)
				),
				&vk::PipelineRasterizationStateCreateInfo(
					vk::PipelineRasterizationStateCreateFlags(),
					VK_FALSE,
					VK_FALSE,
					(m_createInfo.enableDepth) ? vk::PolygonMode::eLine : vk::PolygonMode::eFill, // eFill / eLine
					(m_createInfo.enableDepth) ? vk::CullModeFlagBits::eBack : vk::CullModeFlagBits::eNone,
					vk::FrontFace::eCounterClockwise,
					VK_FALSE,
					0,
					0,
					0,
					1.0f
				),
				&vk::PipelineMultisampleStateCreateInfo(
					vk::PipelineMultisampleStateCreateFlags(),
					vk::SampleCountFlagBits::e1
				),
				&vk::PipelineDepthStencilStateCreateInfo(
					vk::PipelineDepthStencilStateCreateFlags(),
					VK_TRUE,
					VK_TRUE,
					vk::CompareOp::eLessOrEqual,
					VK_FALSE,
					VK_FALSE,
					vk::StencilOpState(),
					vk::StencilOpState(),
					0,
					0
				),
				&vk::PipelineColorBlendStateCreateInfo(
					vk::PipelineColorBlendStateCreateFlags(),
					0,
					vk::LogicOp::eClear,
					static_cast<uint32_t>(colorBlendAttachments.size()),
					colorBlendAttachments.data()
				),
				&vk::PipelineDynamicStateCreateInfo(
					vk::PipelineDynamicStateCreateFlags(),
					static_cast<uint32_t>(dynamicStates.size()),
					dynamicStates.data()
				),
				*m_pipelineLayout,
				*m_renderPass
			)
		)
	);

	m_semaphore = std::make_shared<vk::Semaphore>(
		device->createSemaphore(vk::SemaphoreCreateInfo())
	);
}

void orb::VulkanRenderPass::UpdateDescriptorSets()
{
	auto device = static_cast<VulkanRenderer*>(m_renderer)->m_device.get();

	// Camera Buffer (0):
	auto cameraBuffer = static_cast<VulkanBuffer*>(m_cameraUBO.get())->m_buffer.get();
	std::vector<vk::DescriptorBufferInfo> staticUBOs, dynamicUBOs;
	staticUBOs.push_back(vk::DescriptorBufferInfo(
		*cameraBuffer,
		0,
		sizeof(CameraData)
	));

	// Attached Buffers (1 + i):
	for (auto& ubo : m_createInfo.ubos) {
		auto vkBuffer = static_cast<VulkanBuffer*>(ubo)->m_buffer.get();
		if (ubo->IsDynamic()) {
			dynamicUBOs.push_back(
				vk::DescriptorBufferInfo(
					*vkBuffer,
					0,
					VK_WHOLE_SIZE
				)
			);
		}
		else {
			staticUBOs.push_back(
				vk::DescriptorBufferInfo(
					*vkBuffer,
					0,
					VK_WHOLE_SIZE
				)
			);
		}
	}

	std::vector<vk::WriteDescriptorSet> descriptorWrites = {
		vk::WriteDescriptorSet(
			*m_descriptorSets[0].get(),
			0,
			0,
			static_cast<uint32_t>(staticUBOs.size()),
			vk::DescriptorType::eUniformBuffer,
			nullptr,
			staticUBOs.data()
		)
	};

	if (!dynamicUBOs.empty()) {
		descriptorWrites.push_back(
			vk::WriteDescriptorSet(
				*m_descriptorSets[0].get(),
				static_cast<uint32_t>(staticUBOs.size()),
				0,
				static_cast<uint32_t>(dynamicUBOs.size()),
				vk::DescriptorType::eUniformBufferDynamic,
				nullptr,
				dynamicUBOs.data()
			)
		);
	}

	std::vector<vk::DescriptorImageInfo*> descriptorImageInfo;
	if (!m_createInfo.samplers.empty())
	{
		auto defaultSampler = device->createSampler(
			vk::SamplerCreateInfo(
				vk::SamplerCreateFlags(),
				vk::Filter::eLinear,
				vk::Filter::eLinear,
				vk::SamplerMipmapMode::eNearest,
				vk::SamplerAddressMode::eRepeat,
				vk::SamplerAddressMode::eRepeat,
				vk::SamplerAddressMode::eRepeat,
				0.0f,
				VK_FALSE,
				1.0f,
				VK_FALSE,
				vk::CompareOp::eNever,
				-1000.0f,
				1000.0f,
				vk::BorderColor::eFloatTransparentBlack
			)
		);

		for (uint32_t i = 0; i < m_createInfo.samplers.size(); ++i) {
			descriptorImageInfo.push_back(
				new vk::DescriptorImageInfo(
					defaultSampler,
					*static_cast<VulkanTexture*>(m_createInfo.samplers[i])->m_imageView.get(),
					vk::ImageLayout::eColorAttachmentOptimal // eColorAttachmentOptimal
				)
			);

			descriptorWrites.push_back(
				vk::WriteDescriptorSet(
					*m_descriptorSets[0].get(),
					static_cast<uint32_t>(staticUBOs.size() + dynamicUBOs.size() + i),
					0,
					1,
					vk::DescriptorType::eCombinedImageSampler,
					descriptorImageInfo.back()
				)
			);
		}
	}

	device->updateDescriptorSets(descriptorWrites, nullptr);
}

void orb::VulkanRenderPass::Resize(int w, int h)
{
	auto device = static_cast<VulkanRenderer*>(m_renderer)->m_device.get();
	auto bufferCount = m_renderer->GetBufferCount();

	for (auto& fb : m_fbos) {
		device->destroyFramebuffer(*fb);
	}

	m_fbos.clear();

	for (uint32_t i = 0; i < bufferCount; ++i)
	{
		std::vector<vk::ImageView> vkImageViews;
		uint32_t numLayers = 1;

		if (!m_createInfo.attachments.empty()) {
			for (auto attachment : m_createInfo.attachments) {
				vkImageViews.push_back(
					*static_cast<VulkanTexture*>(attachment.input)->m_imageView.get()
				);

				numLayers = attachment.input->GetLayerCount();
			}
		}
		else {
			auto colorImgView = *static_cast<VulkanRenderer*>(m_renderer)->m_swapchainImages[i].views[VulkanRenderer::SwapchainImage::eColor];
			vkImageViews.push_back(colorImgView);

			if (m_createInfo.enableDepth) {
				auto depthImgView = *static_cast<VulkanRenderer*>(m_renderer)->m_swapchainImages[i].views[VulkanRenderer::SwapchainImage::eDepth];
				vkImageViews.push_back(depthImgView);
			}
		}

		m_fbos.push_back(std::make_shared<vk::Framebuffer>(
			device->createFramebuffer(
				vk::FramebufferCreateInfo(
					vk::FramebufferCreateFlags(),
					*m_renderPass,
					static_cast<uint32_t>(vkImageViews.size()),
					vkImageViews.data(),
					static_cast<uint32_t>(w),
					static_cast<uint32_t>(h),
					numLayers
				)
			))
		);
	}

	m_renderArea = std::shared_ptr<vk::Rect2D>(new vk::Rect2D(
		vk::Rect2D(vk::Offset2D(), vk::Extent2D(w, h))
	));
}

static bool test = false;

void orb::VulkanRenderPass::Record()
{
	auto bufferIndex = m_renderer->GetBufferIndex();
	auto vkGraphicsQueue = static_cast<VulkanRenderer*>(m_renderer)->m_graphicsQueue.get();
	auto vkImageAvailableSemaphore = static_cast<VulkanRenderer*>(m_renderer)->m_imageAvailableSemaphore.get();
	auto vkRenderFinishedSemaphore = static_cast<VulkanRenderer*>(m_renderer)->m_renderFinishedSemaphore.get();
	bool isLast = (static_cast<VulkanRenderer*>(m_renderer)->m_renderPasses.back().get() == this);
	std::vector<vk::CommandBuffer> commandBuffers;

	vk::PipelineStageFlags kernelPipelineStageFlags = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	std::vector<vk::ClearValue> clearValues;

	clearValues.push_back(
		vk::ClearColorValue(std::array<float, 4> {
			m_createInfo.clearColor.r * (1.0f / 255.0f),
			m_createInfo.clearColor.g * (1.0f / 255.0f),
			m_createInfo.clearColor.b * (1.0f / 255.0f),
			m_createInfo.clearColor.a * (1.0f / 255.0f)
		})
	);

	if (m_createInfo.enableDepth) {
		clearValues.push_back(vk::ClearDepthStencilValue(1.0f, 0));
	}

	auto RecordCommandBuffer = [&](VulkanCommand* cmd)
	{
		auto vkCmdBuffer = static_cast<VulkanCommand*>(cmd)->m_cmdBuffer.get();
		vkCmdBuffer->reset(vk::CommandBufferResetFlagBits::eReleaseResources);
		vkCmdBuffer->begin(vk::CommandBufferBeginInfo());
		vkCmdBuffer->beginRenderPass(
			vk::RenderPassBeginInfo(
				*m_renderPass,
				*m_fbos[bufferIndex],
				*m_renderArea,
				static_cast<unsigned int>(clearValues.size()),
				clearValues.data()
			),
			vk::SubpassContents::eInline
		);

		std::vector<uint32_t> offsets;
		for (auto& ubo : m_createInfo.ubos) {
			if (ubo->IsDynamic()) {
				offsets.push_back(0);
			}
		}

		vkCmdBuffer->bindPipeline(vk::PipelineBindPoint::eGraphics, *m_pipeline);
		vkCmdBuffer->bindDescriptorSets(
			vk::PipelineBindPoint::eGraphics,
			*m_pipelineLayout,
			0,
			*m_descriptorSets[0].get(),
			offsets
		);

		cmd->Viewport(0.0f, 0.0f, (float)m_createInfo.resolution.x, (float)m_createInfo.resolution.y);
		cmd->Scissor(0, 0, m_createInfo.resolution.x, m_createInfo.resolution.y);
		onRecord.Emit(cmd);
		vkCmdBuffer->endRenderPass();
		vkCmdBuffer->end();

		commandBuffers.push_back(*vkCmdBuffer);
	};

	if (m_createInfo.type == eDrawOnceBackground ||
		m_createInfo.type == eDrawSyncBackground) {
		for (uint32_t t = 0; t < m_threadPool.GetThreadCount(); t++) {
			auto cmd = static_cast<VulkanCommand*>(m_threadData[t].commands.back().get());
			m_threadPool.PushJob(t, [=] {
				RecordCommandBuffer(cmd);
			});
		}

		m_threadPool.Wait();
	}
	else {
		auto cmd = static_cast<VulkanCommand*>(m_threadData.back().commands[bufferIndex].get());
		RecordCommandBuffer(cmd);
	}

	if (m_parent) {
		auto kernel = vk::SubmitInfo(
			1,
			static_cast<VulkanRenderPass*>(m_parent)->m_semaphore.get(),
			&kernelPipelineStageFlags,
			static_cast<uint32_t>(commandBuffers.size()),
			commandBuffers.data(),
			1,
			(isLast) ? vkRenderFinishedSemaphore : m_semaphore.get()
		);

		vkGraphicsQueue->submit(1, &kernel, nullptr);
	}
	else {
		auto kernel = vk::SubmitInfo(
			1,
			vkImageAvailableSemaphore,
			&kernelPipelineStageFlags,
			static_cast<uint32_t>(commandBuffers.size()),
			commandBuffers.data(),
			1,
			(isLast) ? vkRenderFinishedSemaphore : m_semaphore.get()
		);

		auto vkSwapchainFence = static_cast<VulkanRenderer*>(m_renderer)->m_swapchainImages[bufferIndex].fence;
		vkGraphicsQueue->submit(1, &kernel, *vkSwapchainFence);
	}
} 

void orb::VulkanRenderPass::Cleanup()
{
	auto device = static_cast<VulkanRenderer*>(m_renderer)->m_device.get();

	if (m_createInfo.type == eDrawOnceBackground ||
		m_createInfo.type == eDrawSyncBackground) {
		for (uint32_t t = 0; t < m_threadPool.GetThreadCount(); t++) {
			for (auto& cmd : m_threadData[t].commands) {
				cmd->Cleanup();
			}

			m_threadData[t].commands.clear();
		}
	}
	else {
		for (auto& cmd : m_threadData.back().commands) {
			cmd->Cleanup();
		}

		m_threadData.back().commands.clear();
	}

	m_threadData.clear();

	for (auto& fb : m_fbos) {
		device->destroyFramebuffer(*fb);
	}

	m_fbos.clear();

	device->destroyDescriptorPool(*m_descriptorPool);
	device->destroyPipelineLayout(*m_pipelineLayout);
	device->destroyPipeline(*m_pipeline);
	device->destroyRenderPass(*m_renderPass);
}