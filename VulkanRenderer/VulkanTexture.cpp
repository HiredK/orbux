#include "VulkanTexture.h"
#include "VulkanBuffer.h"
#include "VulkanRenderer.h"
#include <Vulkan/vulkan.hpp>

void orb::VulkanTexture::Build(const void* data, const CreateInfo& ci)
{
	auto ConvertUsageFlags = [](uint32_t flags) -> vk::ImageUsageFlags
	{
		vk::ImageUsageFlags result;
		if (flags & eTransferSrc)             result |= vk::ImageUsageFlagBits::eTransferSrc;
		if (flags & eTransferDst)             result |= vk::ImageUsageFlagBits::eTransferDst;
		if (flags & eSampled)                 result |= vk::ImageUsageFlagBits::eSampled;
		if (flags & eStorage)                 result |= vk::ImageUsageFlagBits::eStorage;
		if (flags & eColorAttachment)         result |= vk::ImageUsageFlagBits::eColorAttachment;
		if (flags & eDepthStencilAttachment)  result |= vk::ImageUsageFlagBits::eDepthStencilAttachment;
		if (flags & eTransientAttachment)     result |= vk::ImageUsageFlagBits::eTransientAttachment;
		if (flags & eInputAttachment)         result |= vk::ImageUsageFlagBits::eInputAttachment;
		return result;
	};

	auto ConvertMemoryProperty = [](uint32_t flags) -> uint32_t
	{
		vk::MemoryPropertyFlags result;
		if (flags & Buffer::eDeviceLocal)     result |= vk::MemoryPropertyFlagBits::eDeviceLocal;
		if (flags & Buffer::eHostVisible)     result |= vk::MemoryPropertyFlagBits::eHostVisible;
		if (flags & Buffer::eHostCoherent)    result |= vk::MemoryPropertyFlagBits::eHostCoherent;
		if (flags & Buffer::eHostCached)      result |= vk::MemoryPropertyFlagBits::eHostCached;
		return static_cast<uint32_t>(result);
	};

	auto TextureTypeToVulkan = std::map<Type, vk::ImageViewType>() =
	{
		{ eTexture1D        , vk::ImageViewType::e1D        },
		{ eTexture2D        , vk::ImageViewType::e2D        },
		{ eTexture3D        , vk::ImageViewType::e3D        },
		{ eTextureCube      , vk::ImageViewType::eCube      },
		{ eTexture1DArray   , vk::ImageViewType::e1DArray   },
		{ eTexture2DArray   , vk::ImageViewType::e2DArray   },
		{ eTextureCubeArray , vk::ImageViewType::eCubeArray }
	};

	auto device = static_cast<VulkanRenderer*>(m_renderer)->m_device.get();
	auto graphicsFamilyIndex = static_cast<VulkanRenderer*>(m_renderer)->m_graphicsFamilyIndex;
	auto vkFormat = VulkanRenderer::ConvertToVkFormat(ci.format);

	bool isFloat = false;
	if (ci.format == Format::eR32F ||
		ci.format == Format::eRG32F ||
		ci.format == Format::eRGB32F ||
		ci.format == Format::eRGBA32F) {
		isFloat = true;
	}

	uint32_t channelCount = 0;
	switch (ci.format) {
		case Format::eR32F:
		case Format::eR8:
			channelCount = 1;
			break;
		case Format::eRG32F:
		case Format::eRG8:
			channelCount = 2;
			break;
		case Format::eRGB32F:
			channelCount = 3;
			break;
		case Format::eRGBA32F:
		case Format::eRGBA8:
			channelCount = 4;
			break;
	}

	uint32_t formatSize = (isFloat) ? sizeof(float) : sizeof(char);
	uint32_t size = m_w * m_h * channelCount * formatSize * ci.layerCount;

	m_format = ci.format;
	m_layerCount = ci.layerCount;
	m_clearColor = ci.clearColor;

	m_image = std::make_shared<vk::Image>(
		device->createImage(
			vk::ImageCreateInfo(
				vk::ImageCreateFlags(),
				vk::ImageType::e2D,
				vkFormat,
				vk::Extent3D(m_w, m_h, m_depth),
				1,
				ci.layerCount,
				vk::SampleCountFlagBits::e1,
				vk::ImageTiling::eOptimal,
				ConvertUsageFlags(ci.usage),
				vk::SharingMode::eExclusive,
				1,
				&graphicsFamilyIndex,
				vk::ImageLayout::eUndefined
			)
		)
	);

	auto memReqs = device->getImageMemoryRequirements(*m_image);
	auto memTypeIndex = static_cast<VulkanRenderer*>(m_renderer)->GetMemoryTypeIndex(
		memReqs.memoryTypeBits,
		ConvertMemoryProperty(ci.memProps)
	);

	m_memory = std::make_shared<vk::DeviceMemory>(
		device->allocateMemory(
			vk::MemoryAllocateInfo(
				memReqs.size,
				memTypeIndex
			)
		)
	);

	device->bindImageMemory(*m_image, *m_memory, 0);

	m_imageView = std::make_shared<vk::ImageView>(
		device->createImageView(
			vk::ImageViewCreateInfo(
				vk::ImageViewCreateFlags(),
				*m_image,
				TextureTypeToVulkan[ci.type],
				vkFormat,
				vk::ComponentMapping(),
				vk::ImageSubresourceRange(
					vk::ImageAspectFlagBits::eColor,
					0,
					1,
					0,
					ci.layerCount
				)
			)
		)
	);

	if (ci.usage & eTransferDst)
	{
		auto stagingBuffer = m_renderer->CreateBuffer(size, data, Buffer::CreateInfo::GetStaging());
		auto vkStagingBuffer = static_cast<VulkanBuffer*>(stagingBuffer.get())->m_buffer.get();
		auto vkGraphicsQueue = static_cast<VulkanRenderer*>(m_renderer)->m_graphicsQueue.get();

		vk::BufferImageCopy copyRegion = {};
		copyRegion.imageSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
		copyRegion.imageSubresource.layerCount = 1;
		copyRegion.imageExtent.width = m_w;
		copyRegion.imageExtent.height = m_h;
		copyRegion.imageExtent.depth = 1;

		auto stagingCmdPool = std::make_shared<vk::CommandPool>(
			device->createCommandPool(
				vk::CommandPoolCreateInfo(
					vk::CommandPoolCreateFlags(vk::CommandPoolCreateFlagBits::eResetCommandBuffer),
					graphicsFamilyIndex
				)
			)
		);

		auto stagingCmd = device->allocateCommandBuffers(
			vk::CommandBufferAllocateInfo(
				*stagingCmdPool,
				vk::CommandBufferLevel::ePrimary,
				1
			)
		)[0];

		stagingCmd.begin(vk::CommandBufferBeginInfo());

		stagingCmd.pipelineBarrier(
			vk::PipelineStageFlagBits::eHost,
			vk::PipelineStageFlagBits::eTransfer,
			vk::DependencyFlagBits(),
			nullptr,
			nullptr,
			vk::ImageMemoryBarrier(
				vk::AccessFlags(),
				vk::AccessFlagBits::eTransferWrite,
				vk::ImageLayout::eUndefined,
				vk::ImageLayout::eTransferDstOptimal,
				VK_QUEUE_FAMILY_IGNORED,
				VK_QUEUE_FAMILY_IGNORED,
				*m_image,
				vk::ImageSubresourceRange(
					vk::ImageAspectFlagBits::eColor,
					0, 1, 0, 1
				)
			)
		);

		stagingCmd.copyBufferToImage(
			*vkStagingBuffer,
			*m_image,
			vk::ImageLayout::eTransferDstOptimal,
			copyRegion
		);

		stagingCmd.pipelineBarrier(
			vk::PipelineStageFlagBits::eTransfer,
			vk::PipelineStageFlagBits::eFragmentShader,
			vk::DependencyFlagBits(),
			nullptr,
			nullptr,
			vk::ImageMemoryBarrier(
				vk::AccessFlagBits::eTransferWrite,
				vk::AccessFlagBits::eShaderRead,
				vk::ImageLayout::eTransferDstOptimal,
				vk::ImageLayout::eShaderReadOnlyOptimal,
				VK_QUEUE_FAMILY_IGNORED,
				VK_QUEUE_FAMILY_IGNORED,
				*m_image,
				vk::ImageSubresourceRange(
					vk::ImageAspectFlagBits::eColor,
					0, 1, 0, 1
				)
			)
		);

		stagingCmd.end();
		auto stagingFence = device->createFence(vk::FenceCreateInfo());
		vkGraphicsQueue->submit(
			vk::SubmitInfo(0, nullptr, nullptr, 1, &stagingCmd),
			stagingFence
		);

		device->waitForFences(1, &stagingFence, VK_TRUE, UINT64_MAX);
		device->destroyFence(stagingFence);
		device->freeCommandBuffers(*stagingCmdPool, 1, &stagingCmd);
	}
}

void orb::VulkanTexture::Cleanup()
{
}