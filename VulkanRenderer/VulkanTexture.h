/**
* @file VulkanTexture.h
* @brief
*/

#pragma once

#include "Texture.h"

// Forward declarations:
namespace vk
{
	class Image;
	class ImageView;
	class DeviceMemory;
}

namespace orb
{
	class RENDERER_API VulkanTexture : public Texture
	{
		public:
			friend class VulkanRenderPass;

			//! CTOR/DTOR:
			VulkanTexture(
				Renderer* renderer,
				uint32_t w,
				uint32_t h,
				uint32_t depth = 0
			);

			virtual ~VulkanTexture();

			//! VIRTUALS:
			void Build(const void* data, const CreateInfo& ci) override;
			void Cleanup() override;

		protected:
			//! MEMBERS:
			std::shared_ptr<vk::Image> m_image;
			std::shared_ptr<vk::ImageView> m_imageView;
			std::shared_ptr<vk::DeviceMemory> m_memory;
	};

	////////////////////////////////////////////////////////////////////////////////
	// VulkanTexture inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline VulkanTexture:: VulkanTexture(
		Renderer* renderer,
		uint32_t w,
		uint32_t h, 
		uint32_t depth)
		: Texture(renderer, w, h, depth) {
	}
	/*----------------------------------------------------------------------------*/
	inline VulkanTexture::~VulkanTexture() {
	}

} // orb namespace