/**
* @file Renderer.h
* @brief
*/

#pragma once

#include "Buffer.h"
#include "Command.h"
#include "RenderPass.h"
#include "Texture.h"
#include <memory>
#include <string>
#include <vector>

namespace orb
{
	// Forward declarations:
	class Window;

	class RENDERER_API Renderer
	{
		public:
			//! TYPEDEF/ENUMS:
			enum Type
			{
				eUndefined,
				eDx3D12,
				eVulkan
			};

			enum BufferingType
			{
				eSingle,
				eDouble,
				eTriple
			};

			struct GPUInfo
			{
				//! SERVICES:
				std::string Print() const;

				//! MEMBERS:
				uint32_t vendorID;
				uint32_t deviceID;
				std::string info;
			};

			//! CTOR/DTOR:
			Renderer(const Window& window, BufferingType buffering = eTriple);
			virtual ~Renderer();

			////////////////////////////////////////////////////////////////////////
			// Renderer::CreateBuffer:
			////////////////////////////////////////////////////////////////////////
			/*--------------------------------------------------------------------*/
			virtual std::shared_ptr<Buffer> CreateBuffer(
				uint32_t size,
				const void* data,
				const Buffer::CreateInfo& ci = Buffer::CreateInfo::GetDefault()
			) = 0;

			////////////////////////////////////////////////////////////////////////
			// Renderer::CreateTexture:
			////////////////////////////////////////////////////////////////////////
			/*--------------------------------------------------------------------*/
			virtual std::shared_ptr<Texture> CreateTexture(
				uint32_t w, 
				uint32_t h,
				uint32_t depth, 
				const void* data,
				const Texture::CreateInfo& ci = Texture::CreateInfo::GetDefault()
			) = 0;

			////////////////////////////////////////////////////////////////////////
			// Renderer::CreateRenderPass:
			////////////////////////////////////////////////////////////////////////
			/*--------------------------------------------------------------------*/
			virtual RenderPass* CreateRenderPass(
				RenderPass* parent,
				const RenderPass::CreateInfo& ci
			) = 0;

			virtual void Initialize() = 0;
			virtual void Resize(int w, int h) = 0;
			virtual void Display() = 0;
			virtual void Cleanup() = 0;
			virtual Type GetType() const;

			//! ACCESSORS:
			const Window& GetWindow() const;
			uint32_t GetBufferCount() const;
			uint32_t GetBufferIndex() const;
			const GPUInfo& GetGPUInfo() const;

		protected:
			//! MEMBERS:
			const Window& m_window;
			std::vector<std::shared_ptr<RenderPass>> m_renderPasses;
			BufferingType m_bufferingType;
			uint32_t m_bufferIndex;
			GPUInfo m_gpuInfo;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Renderer::GpuInfo inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline std::string Renderer::GPUInfo::Print() const {
		return (
			"VID=" + std::to_string(vendorID) + " " +
			"PID=" + std::to_string(deviceID) + " / " + info
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Renderer inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Renderer:: Renderer(const Window& window, BufferingType buffering)
		: m_window(window)
		, m_bufferingType(buffering)
		, m_bufferIndex(0)
		, m_gpuInfo({ 0, 0, "Undefined" }) {
	}
	/*----------------------------------------------------------------------------*/
	inline Renderer::~Renderer() {
	}

	/*----------------------------------------------------------------------------*/
	inline Renderer::Type Renderer::GetType() const {
		return eUndefined;
	}
	/*----------------------------------------------------------------------------*/
	inline const Window& Renderer::GetWindow() const {
		return m_window;
	}
	/*----------------------------------------------------------------------------*/
	inline uint32_t Renderer::GetBufferCount() const {
		switch (m_bufferingType) {
			case eSingle: return 1;
			case eDouble: return 2;
			case eTriple: return 3;
			default: break;
		}

		return 0;
	}
	/*----------------------------------------------------------------------------*/
	inline uint32_t Renderer::GetBufferIndex() const {
		return m_bufferIndex;
	}
	/*----------------------------------------------------------------------------*/
	inline const Renderer::GPUInfo& Renderer::GetGPUInfo() const {
		return m_gpuInfo;
	}

} // orb namespace