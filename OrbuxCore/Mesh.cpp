#include "Mesh.h"
#include "Renderer.h"
#include "Logger.h"

void orb::Mesh::Build(
	uint32_t vertexSize,
	const void* vertexData,
	uint32_t indexSize,
	const void* indexData,
	const Buffer::CreateInfo& ci)
{
	if (vertexSize > 0) {
		auto ci = Buffer::CreateInfo::GetDefault();
		ci.usage = ci.usage | Buffer::eVertex;
		ci.memProps = ci.memProps;

		m_vbo = std::shared_ptr<Buffer>(
			m_renderer->CreateBuffer(vertexSize, vertexData, ci)
		);
	}

	if (indexSize > 0) {
		auto ci = Buffer::CreateInfo::GetDefault();
		ci.usage = ci.usage | Buffer::eIndex;
		ci.memProps = ci.memProps;

		m_ibo = std::shared_ptr<Buffer>(
			m_renderer->CreateBuffer(indexSize, indexData, ci)
		);
	}
}

void orb::Mesh::Draw(
	Command* cmd,
	uint32_t instanceCount,
	uint32_t firstIndex,
	int32_t vertexOffset,
	uint32_t firstInstance)
{
	if (m_vbo)
	{
		m_vbo->Bind(cmd);
	}

	if (m_ibo)
	{
		m_ibo->Bind(cmd);

		cmd->DrawIndexed(
			static_cast<uint32_t>(m_indexCount),
			instanceCount,
			firstIndex,
			vertexOffset,
			firstInstance
		);
	}
	else
	{
		cmd->Draw(
			static_cast<uint32_t>(m_indexCount),
			instanceCount,
			vertexOffset,
			firstInstance
		);
	}
}

std::shared_ptr<orb::Mesh> orb::Mesh::BuildCube(Renderer* renderer, const vec3& min, const vec3& max)
{
	Logger::ScopedTimer timer(LOG, "Mesh::BuildCube");

	std::vector<Vertex3D> vertices = 
	{
		{ min.x, min.y, min.z, 0.0f, 0.0f },
		{ min.x, min.y, max.z, 1.0f, 0.0f },
		{ min.x, max.y, max.z, 1.0f, 1.0f },
		{ min.x, max.y, min.z, 0.0f, 1.0f },
		{ max.x, min.y, max.z, 0.0f, 0.0f },
		{ max.x, min.y, min.z, 1.0f, 0.0f },
		{ max.x, max.y, min.z, 1.0f, 1.0f },
		{ max.x, max.y, max.z, 0.0f, 1.0f },
		{ min.x, min.y, min.z, 0.0f, 0.0f },
		{ max.x, min.y, min.z, 1.0f, 0.0f },
		{ max.x, min.y, max.z, 1.0f, 1.0f },
		{ min.x, min.y, max.z, 0.0f, 1.0f },
		{ max.x, max.y, min.z, 0.0f, 0.0f },
		{ min.x, max.y, min.z, 1.0f, 0.0f },
		{ min.x, max.y, max.z, 1.0f, 1.0f },
		{ max.x, max.y, max.z, 0.0f, 1.0f },
		{ min.x, min.y, min.z, 0.0f, 0.0f },
		{ min.x, max.y, min.z, 1.0f, 0.0f },
		{ max.x, max.y, min.z, 1.0f, 1.0f },
		{ max.x, min.y, min.z, 0.0f, 1.0f },
		{ min.x, max.y, max.y, 0.0f, 0.0f },
		{ min.x, min.y, max.y, 1.0f, 0.0f },
		{ max.x, min.y, max.y, 1.0f, 1.0f },
		{ max.x, max.y, max.y, 0.0f, 1.0f }
	};

	std::vector<ivec4> quads =
	{
		{  0,  1,  2,  3 }, 
		{  4,  5,  6,  7 }, 
		{  8,  9, 10, 11 },
		{ 12, 13, 14, 15 }, 
		{ 16, 17, 18, 19 }, 
		{ 20, 21, 22, 23 }
	};

	std::vector<unsigned short> indices;
	for (auto & q : quads)
	{
		indices.push_back(q.x);
		indices.push_back(q.y);
		indices.push_back(q.z);

		indices.push_back(q.x);
		indices.push_back(q.z);
		indices.push_back(q.w);
	}

	std::shared_ptr<orb::Mesh> mesh = std::shared_ptr<Mesh>(new Mesh(renderer));
	mesh->SetIndexCount(indices.size());
	mesh->Build(
		static_cast<uint32_t>(vertices.size() * sizeof(Vertex3D)),
		vertices.data(),
		static_cast<uint32_t>(indices.size() * sizeof(unsigned short)),
		indices.data()
	);

	return mesh;
}

std::shared_ptr<orb::Mesh> orb::Mesh::BuildGrid(Renderer* renderer, unsigned int w, unsigned int h)
{
	Logger::ScopedTimer timer(LOG, "Mesh::BuildGrid");

	std::vector<Vertex3D> vertices;
	std::vector<unsigned short> indices;
	vertices.resize(w * h);

	for (unsigned int i = 0; i < w; ++i)
	for (unsigned int j = 0; j < h; ++j)
	{
		float ox = (float)i / (float)(w - 1);
		float oy = (float)j / (float)(h - 1);

		vertices[i + j * w] = { ox, oy, 0.0f, ox, oy };

		if (i < w - 1 && j < h - 1)
		{
			indices.push_back(i + j * w);
			indices.push_back(i + (j + 1) * w);
			indices.push_back((i + 1) + j * w);

			indices.push_back(i + (j + 1) * w);
			indices.push_back((i + 1) + (j + 1) * w);
			indices.push_back((i + 1) + j * w);
		}
	}

	std::shared_ptr<orb::Mesh> mesh = std::shared_ptr<Mesh>(new Mesh(renderer));
	mesh->SetIndexCount(indices.size());
	mesh->Build(
		static_cast<uint32_t>(vertices.size() * sizeof(Vertex3D)),
		vertices.data(),
		static_cast<uint32_t>(indices.size() * sizeof(unsigned short)),
		indices.data()
	);

	return mesh;
}

std::shared_ptr<orb::Mesh> orb::Mesh::BuildPatch(Renderer* renderer, unsigned int w, unsigned int h)
{
	Logger::ScopedTimer timer(LOG, "Mesh::BuildPatch");

	std::vector<Vertex3D> vertices;
	std::vector<unsigned short> indices;
	vertices.resize(w * h);

	for (unsigned int i = 0; i < w; ++i)
	for (unsigned int j = 0; j < h; ++j)
	{
		float ox = (float)i / (float)(w - 1);
		float oy = (float)j / (float)(h - 1);

		vertices[i + j * w] = { ox, oy, 0.0f, ox, oy };

		if (i < w - 1 && j < h - 1)
		{
			unsigned short p1 = i + j * w;
			unsigned short p2 = p1 + w;
			unsigned short p3 = p2 + 1;
			unsigned short p4 = p1 + 1;

			indices.push_back(p1);
			indices.push_back(p2);
			indices.push_back(p3);
			indices.push_back(p4);
		}
	}

	std::shared_ptr<orb::Mesh> mesh = std::shared_ptr<Mesh>(new Mesh(renderer));
	mesh->SetIndexCount(indices.size());
	mesh->Build(
		static_cast<uint32_t>(vertices.size() * sizeof(Vertex3D)),
		vertices.data(),
		static_cast<uint32_t>(indices.size() * sizeof(unsigned short)),
		indices.data()
	);

	return mesh;
}

std::shared_ptr<orb::Mesh> orb::Mesh::BuildQuad(Renderer* renderer, float w, float h, const Color& color)
{
	Vertex2D data[] =
	{
		{ -w,-h, 0.0f, 0.0f, color.r, color.g, color.b, color.a },
		{  w,-h, 1.0f, 0.0f, color.r, color.g, color.b, color.a },
		{ -w, h, 0.0f, 1.0f, color.r, color.g, color.b, color.a },
		{  w, h, 1.0f, 1.0f, color.r, color.g, color.b, color.a }
	};

	std::shared_ptr<orb::Mesh> mesh = std::shared_ptr<Mesh>(new Mesh(renderer));
	mesh->Build(4 * sizeof(Vertex2D), &data[0]);
	mesh->SetIndexCount(4);
	return mesh;
}

void orb::Mesh::Cleanup()
{
}