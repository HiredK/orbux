/**
* @file Signal.h
* @brief
*/

#pragma once

#include <functional>
#include <map>

// Forward declarations:
namespace luabind {
	namespace adl {
		class object;
	}
}

namespace orb
{
	class ISignal
	{
		public:
			//! VIRTUALS:
			virtual int Connect(const luabind::adl::object& f) { return 0; }
	};

	template <typename... Args> 
	class Signal : public ISignal
	{
		public:
			//! CTOR/DTOR:
			Signal();
			Signal(Signal const& other);
			virtual ~Signal();

#ifndef RENDERER_EXPORTS  
			//! VIRTUALS:
			int Connect(const luabind::adl::object& f) override;
#endif

			//! SERVICES:
			template <typename T> int ConnectMember(T *inst, void (T::*func)(Args...));
			template <typename T> int ConnectMember(T *inst, void (T::*func)(Args...) const);
			int Connect(std::function<void(Args...)> const& slot) const;
			void Disconnect(int id) const;
			void DisconnectAll() const;
			void Emit(Args... p);

		private:
			//! MEMBERS:
			mutable std::map<int, std::function<void(Args...)>> m_slots;
			mutable int m_currentID;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Signal inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	template <typename... Args>
	inline Signal<Args...>:: Signal() 
		: m_currentID(0) {
	}
	/*----------------------------------------------------------------------------*/
	template <typename... Args>
	inline Signal<Args...>:: Signal(Signal const& other) 
		: m_currentID(0) {
	}
	/*----------------------------------------------------------------------------*/
	template <typename... Args>
	inline Signal<Args...>::~Signal() {
	}

	/*----------------------------------------------------------------------------*/
	template <typename... Args>
	template <typename T>
	inline int Signal<Args...>::ConnectMember(T* inst, void (T::*func)(Args...)) {
		return Connect([=](Args... args) { (inst->*func)(args...); });
	}
	/*----------------------------------------------------------------------------*/
	template <typename... Args>
	template <typename T>
	inline int Signal<Args...>::ConnectMember(T* inst, void (T::*func)(Args...) const) {
		return connect([=](Args... args) { (inst->*func)(args...); });
	}
	/*----------------------------------------------------------------------------*/
	template <typename... Args>
	inline int Signal<Args...>::Connect(std::function<void(Args...)> const& slot) const {
		m_slots.insert(std::make_pair(++m_currentID, slot));
		return m_currentID;
	}
	/*----------------------------------------------------------------------------*/
	template <typename... Args>
	inline void Signal<Args...>::Disconnect(int id) const {
		m_slots.erase(id);
	}
	/*----------------------------------------------------------------------------*/
	template <typename... Args>
	inline void Signal<Args...>::DisconnectAll() const {
		m_slots.clear();
	}
	/*----------------------------------------------------------------------------*/
	template <typename... Args>
	inline void Signal<Args...>::Emit(Args... p) {
		for (auto s : m_slots) { s.second(p...); }
	}
}