#include "FileHandle.h"
#include "Logger.h"
#include <physfs.h>

bool orb::FileHandle::Process()
{
	if (PHYSFS_exists(m_path.c_str()))
	{
		if (m_allocated)
		{
			if (m_state != Created)
			{
				LOG() << m_path << ": Loading file";
				m_state = (CreateData()) ? Created : m_state;
				return true;
			}
		}
		else if (m_state == Created)
		{
			LOG() << m_path << ": Unloading file";
			DeleteData();
			m_state = Empty;
			return true;
		}
	}
	else if (m_state != Missing)
	{
		LOG() << m_path << ": Removing file";
		DeleteData();
		m_state = Missing;
		return true;
	}

	return false;
}

bool orb::FileHandle::CreateData()
{
	bool success = true;
	if (m_data.empty())
	{
		PHYSFS_file* handle = PHYSFS_openRead(m_path.c_str());
		PHYSFS_uint32 len = static_cast<PHYSFS_uint32>(PHYSFS_fileLength(handle));
		m_data.resize(len);

		PHYSFS_sint64 length_read = PHYSFS_read(handle, &m_data[0], 1, len);
		PHYSFS_close(handle);

		if (length_read == -1) {
			LOG(Logger::Error) << m_path << ": " << PHYSFS_getLastError();
			success = false;
		}
	}

	return success;
}

void orb::FileHandle::ScanFileForInclude(std::string& input)
{
	std::string prefix = "#include ";
	size_t start = 0;

	auto ExtractPath = [](const std::string& str) -> std::string {
		return str.substr(0, str.find_last_of("\\/"));
	};

	while ((start = input.find(prefix, start)) != std::string::npos) {
		size_t pos = start + prefix.length() + 1;
		size_t len = input.find("\"", pos);
		std::string content = "";

		std::string root = ExtractPath(m_path);
		std::string path = root + "/" + input.substr(pos, len - pos);
		if (PHYSFS_exists(path.c_str()))
		{
			PHYSFS_file* handle = PHYSFS_openRead(path.c_str());
			if (handle) {
				PHYSFS_uint32 length = static_cast<PHYSFS_uint32>(PHYSFS_fileLength(handle));
				content.resize(length);

				PHYSFS_read(handle, &content[0], 1, length);
				ScanFileForInclude(content);
				PHYSFS_close(handle);
			}
		}
		else {
			LOG(Logger::Warning) << path << ": Missing file!";
			break;
		}

		input.replace(start, (len + 1) - start, content);
		start += content.length();
	}
}

void orb::FileHandle::DeleteData()
{
	if (!m_data.empty())
	{
		m_data.clear();
		m_data.shrink_to_fit();
	}
}