#include "Camera.h"

void orb::Camera::Rebuild()
{
	if (m_isOrtho) {
		m_matrices.proj = glm::ortho(m_bounds.x, m_bounds.y, m_bounds.z, m_bounds.w, m_clip.x, m_clip.y);
	}
	else {
		m_matrices.proj = glm::perspective(m_fov, m_aspect, m_clip.x, m_clip.y);
	}

	m_matrices.view = glm::affineInverse(glm::translate(dmat4(), m_position) * glm::mat4_cast(m_rotation));
	m_matrices.viewProj = m_matrices.proj * m_matrices.view;
	onRebuild.Emit(&m_matrices);
}