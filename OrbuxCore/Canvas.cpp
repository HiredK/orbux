#include "Canvas.h"
#include "Window.h"
#include "Mesh.h"
#include "Texture.h"
#include "FileHandle.h"
#include "Logger.h"

#include "SDL/SDL.h"
#include "SDL/SDL_syswm.h"

#include <imgui.h>

void orb::Canvas::Initialize()
{
	m_context = ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();

	io.DisplaySize = ImVec2((float)m_window.GetWidth(), (float)m_window.GetHeight());
	io.ImeWindowHandle = m_window.GetSysWMinfo()->info.win.window;
	io.IniFilename = NULL;

	int bufferCount = m_window.GetRenderer()->GetBufferCount();
	m_meshes.resize(bufferCount);

	unsigned char* tex_pixels = NULL;
	int tex_w, tex_h;
	io.Fonts->GetTexDataAsRGBA32(&tex_pixels, &tex_w, &tex_h);

	m_fontTexture = m_window.GetRenderer()->CreateTexture(
		tex_w,
		tex_h, 
		1, 
		tex_pixels
	);

	m_window.onEvent.Connect([&](const SDL_Event* e)
	{
		ImGuiIO& io = ImGui::GetIO();
		switch (e->type)
		{
			case SDL_TEXTINPUT:
			{
				io.AddInputCharactersUTF8(e->text.text);
				break;
			}

			case SDL_MOUSEBUTTONDOWN:
			{
				if (e->button.button == SDL_BUTTON_LEFT) m_mousePressed[0] = true;
				if (e->button.button == SDL_BUTTON_RIGHT) m_mousePressed[1] = true;
				if (e->button.button == SDL_BUTTON_MIDDLE) m_mousePressed[2] = true;
				break;
			}

			case SDL_KEYDOWN:
			case SDL_KEYUP:
			{
				int key = e->key.keysym.sym & ~SDLK_SCANCODE_MASK;
				io.KeysDown[key] = (e->type == SDL_KEYDOWN);
				io.KeyShift = ((SDL_GetModState() & KMOD_SHIFT) != 0);
				io.KeyCtrl = ((SDL_GetModState() & KMOD_CTRL) != 0);
				io.KeyAlt = ((SDL_GetModState() & KMOD_ALT) != 0);
				io.KeySuper = ((SDL_GetModState() & KMOD_GUI) != 0);
				break;
			}
		}
	});

	{
		ImGuiStyle * style = &ImGui::GetStyle();

		style->WindowPadding = ImVec2(15, 15);
		style->WindowRounding = 5.0f;
		style->FramePadding = ImVec2(5, 5);
		style->FrameRounding = 4.0f;
		style->ItemSpacing = ImVec2(12, 8);
		style->ItemInnerSpacing = ImVec2(8, 6);
		style->IndentSpacing = 25.0f;
		style->ScrollbarSize = 15.0f;
		style->ScrollbarRounding = 9.0f;
		style->GrabMinSize = 5.0f;
		style->GrabRounding = 3.0f;

		style->Colors[ImGuiCol_Text] = ImVec4(0.80f, 0.80f, 0.83f, 1.00f);
		style->Colors[ImGuiCol_TextDisabled] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
		style->Colors[ImGuiCol_WindowBg] = ImVec4(0.06f, 0.05f, 0.07f, 0.90f);
		style->Colors[ImGuiCol_ChildWindowBg] = ImVec4(0.07f, 0.07f, 0.09f, 0.90f);
		style->Colors[ImGuiCol_PopupBg] = ImVec4(0.07f, 0.07f, 0.09f, 0.90f);
		style->Colors[ImGuiCol_Border] = ImVec4(0.80f, 0.80f, 0.83f, 0.88f);
		style->Colors[ImGuiCol_BorderShadow] = ImVec4(0.92f, 0.91f, 0.88f, 0.00f);
		style->Colors[ImGuiCol_FrameBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		style->Colors[ImGuiCol_FrameBgHovered] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
		style->Colors[ImGuiCol_FrameBgActive] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_TitleBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		style->Colors[ImGuiCol_TitleBgCollapsed] = ImVec4(1.00f, 0.98f, 0.95f, 0.75f);
		style->Colors[ImGuiCol_TitleBgActive] = ImVec4(0.07f, 0.07f, 0.09f, 1.00f);
		style->Colors[ImGuiCol_MenuBarBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		style->Colors[ImGuiCol_ScrollbarBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		style->Colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.80f, 0.80f, 0.83f, 0.31f);
		style->Colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
		//style->Colors[ImGuiCol_ComboBg] = ImVec4(0.19f, 0.18f, 0.21f, 1.00f);
		style->Colors[ImGuiCol_CheckMark] = ImVec4(0.80f, 0.80f, 0.83f, 0.31f);
		style->Colors[ImGuiCol_SliderGrab] = ImVec4(0.80f, 0.80f, 0.83f, 0.31f);
		style->Colors[ImGuiCol_SliderGrabActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
		style->Colors[ImGuiCol_Button] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		style->Colors[ImGuiCol_ButtonHovered] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
		style->Colors[ImGuiCol_ButtonActive] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_Header] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
		style->Colors[ImGuiCol_HeaderHovered] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_HeaderActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
		style->Colors[ImGuiCol_Column] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_ColumnHovered] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
		style->Colors[ImGuiCol_ColumnActive] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_ResizeGrip] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
		style->Colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
		style->Colors[ImGuiCol_ResizeGripActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
		//style->Colors[ImGuiCol_CloseButton] = ImVec4(0.40f, 0.39f, 0.38f, 0.16f);
		//style->Colors[ImGuiCol_CloseButtonHovered] = ImVec4(0.40f, 0.39f, 0.38f, 0.39f);
		//style->Colors[ImGuiCol_CloseButtonActive] = ImVec4(0.40f, 0.39f, 0.38f, 1.00f);
		style->Colors[ImGuiCol_PlotLines] = ImVec4(0.40f, 0.39f, 0.38f, 0.63f);
		style->Colors[ImGuiCol_PlotLinesHovered] = ImVec4(0.25f, 1.00f, 0.00f, 1.00f);
		style->Colors[ImGuiCol_PlotHistogram] = ImVec4(0.40f, 0.39f, 0.38f, 0.63f);
		style->Colors[ImGuiCol_PlotHistogramHovered] = ImVec4(0.25f, 1.00f, 0.00f, 1.00f);
		style->Colors[ImGuiCol_TextSelectedBg] = ImVec4(0.25f, 1.00f, 0.00f, 0.43f);
		style->Colors[ImGuiCol_ModalWindowDarkening] = ImVec4(1.00f, 0.98f, 0.95f, 0.73f);
	}
}

void orb::Canvas::AddFont(const std::string& name, FileHandle* handle)
{
	std::vector<char> raw;
	raw.resize(handle->GetSize());
	memcpy(&raw[0], handle->GetData(), raw.size());

	ImGuiIO& io = ImGui::GetIO();
	ImFont* pFont = io.Fonts->AddFontFromMemoryTTF(raw.data(), 14, 14.0f);
	m_fonts[name] = pFont;

	unsigned char* pixels;
	int width, height, bytes_per_pixels;
	io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height, &bytes_per_pixels);

	m_fontTexture = m_window.GetRenderer()->CreateTexture(
		width,
		height,
		1,
		pixels
	);
}

void orb::Canvas::NewFrame()
{
	ImGuiIO& io = ImGui::GetIO();
	io.DisplaySize = ImVec2((float)m_window.GetWidth(), (float)m_window.GetHeight());

	double currentTime = SDL_GetPerformanceCounter() / (double)SDL_GetPerformanceFrequency();
	io.DeltaTime = m_currentTime > 0.0 ? (float)(currentTime - m_currentTime) : (float)(1.0f / 60.0f);
	m_currentTime = currentTime;
	m_framerate = io.Framerate;

	int mx, my;
	Uint32 mouseMask = SDL_GetMouseState(&mx, &my);
	io.MousePos = ImVec2((float)mx, (float)my);

	io.MouseDown[0] = m_mousePressed[0] || (mouseMask & SDL_BUTTON(SDL_BUTTON_LEFT)) != 0;
	io.MouseDown[1] = m_mousePressed[1] || (mouseMask & SDL_BUTTON(SDL_BUTTON_RIGHT)) != 0;
	io.MouseDown[2] = m_mousePressed[2] || (mouseMask & SDL_BUTTON(SDL_BUTTON_MIDDLE)) != 0;
	m_mousePressed[0] = m_mousePressed[1] = m_mousePressed[2] = false;
	SDL_ShowCursor(io.MouseDrawCursor ? 0 : 1);

	ImGui::NewFrame();

	if (!m_fonts.empty()) {
		auto font = m_fonts.begin()->second;
		ImGui::PushFont(font);
	}

	onFrame.Emit(1.0f);

	if (!m_fonts.empty()) {
		ImGui::PopFont();
	}

	ImGui::Render();
}

void orb::Canvas::Draw(Command* cmd)
{
	auto drawData = ImGui::GetDrawData();
	if (drawData->TotalVtxCount == 0 ||
		drawData->TotalIdxCount == 0) {
		return;
	}

	size_t vertexSize = drawData->TotalVtxCount * sizeof(ImDrawVert);
	size_t indexSize = drawData->TotalIdxCount * sizeof(ImDrawIdx);
	int vertexOffset = 0;
	int indexOffset = 0;

	auto bufferIndex = m_window.GetRenderer()->GetBufferIndex();

	if (!m_meshes[bufferIndex] ||
		m_meshes[bufferIndex]->GetVBO()->GetSize() < vertexSize ||
		m_meshes[bufferIndex]->GetIBO()->GetSize() < indexSize)
	{
		if (m_meshes[bufferIndex]) {
			m_meshes[bufferIndex]->Cleanup();
		}

		auto meshCI = Buffer::CreateInfo::GetDefault();
		meshCI.usage = 0;
		meshCI.memProps = Buffer::eHostVisible | Buffer::eHostCached;

		m_meshes[bufferIndex] = std::shared_ptr<Mesh>(new Mesh(m_window.GetRenderer()));
		m_meshes[bufferIndex]->Build(
			static_cast<uint32_t>(vertexSize),
			nullptr,
			static_cast<uint32_t>(indexSize),
			nullptr,
			meshCI
		);
	}

	auto vertexDst = (ImDrawVert*)m_meshes[bufferIndex]->GetVBO()->Map(static_cast<uint32_t>(vertexSize), 0);
	auto indexDst = (ImDrawIdx*)m_meshes[bufferIndex]->GetIBO()->Map(static_cast<uint32_t>(indexSize), 0);

	for (int n = 0; n < drawData->CmdListsCount; n++)
	{
		const ImDrawList* drawCmdList = drawData->CmdLists[n];
		memcpy(vertexDst, drawCmdList->VtxBuffer.Data, drawCmdList->VtxBuffer.Size * sizeof(ImDrawVert));
		memcpy(indexDst, drawCmdList->IdxBuffer.Data, drawCmdList->IdxBuffer.Size * sizeof(ImDrawIdx));
		vertexDst += drawCmdList->VtxBuffer.Size;
		indexDst += drawCmdList->IdxBuffer.Size;
	}

	m_meshes[bufferIndex]->GetVBO()->Unmap();
	m_meshes[bufferIndex]->GetIBO()->Unmap();

	for (int n = 0; n < drawData->CmdListsCount; n++)
	{
		const ImDrawList* cmd_list = drawData->CmdLists[n];
		for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
		{
			const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
			if (pcmd->UserCallback) {
				pcmd->UserCallback(cmd_list, pcmd);
			}
			else
			{
				/*cmd->Scissor(
					static_cast<int>(pcmd->ClipRect.x),
					static_cast<int>(pcmd->ClipRect.y),
					static_cast<int>(pcmd->ClipRect.z),
					static_cast<int>(pcmd->ClipRect.w)
				);*/

				m_meshes[bufferIndex]->SetIndexCount(pcmd->ElemCount);
				m_meshes[bufferIndex]->Draw(cmd, 1, indexOffset, vertexOffset);
			}

			indexOffset += pcmd->ElemCount;
		}

		vertexOffset += cmd_list->VtxBuffer.Size;
	}
}

bool orb::Canvas::IsMouseHover() const
{
	ImGuiIO& io = ImGui::GetIO();
	return io.WantCaptureMouse;
}

void orb::Canvas::Cleanup()
{
	ImGui::DestroyContext(m_context);
}