/**
* @file Canvas.h
* @brief
*/

#pragma once

#include "Signal.h"
#include <memory>
#include <vector>
#include <array>

// Forward declarations:
struct ImGuiContext;
struct ImFont;

namespace orb
{
	class Command;
	class Mesh;
	class Texture;
	class FileHandle;
	class Window;

	class Canvas
	{
		public:
			//! CTOR/DTOR:
			Canvas(const Window& window);
			virtual ~Canvas();

			//! SERVICES:
			void Initialize();
			void AddFont(const std::string& name, FileHandle* handle);
			void NewFrame();
			void Draw(Command* cmd);
			void Cleanup();

			//! ACCESSORS:
			bool IsMouseHover() const;
			Texture* GetFontTexture() const;
			float GetFramerate() const;

			//! SIGNALS:
			Signal<float> onFrame;

		protected:
			//! MEMBERS:
			const Window& m_window;
			ImGuiContext* m_context;
			std::vector<std::shared_ptr<Mesh>> m_meshes;
			std::map<std::string, ImFont*> m_fonts;
			std::shared_ptr<Texture> m_fontTexture;
			bool m_mousePressed[3];
			double m_currentTime;
			float m_framerate;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Canvas inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Canvas:: Canvas(const Window& window)
		: m_window(window)
		, m_context(nullptr)
		, m_mousePressed{ false, false, false }
		, m_currentTime(0.0)
		, m_framerate(0.0f) {
	}
	/*----------------------------------------------------------------------------*/
	inline Canvas::~Canvas() {
	}

	/*----------------------------------------------------------------------------*/
	inline Texture* Canvas::GetFontTexture() const {
		return m_fontTexture.get();
	}
	/*----------------------------------------------------------------------------*/
	inline float Canvas::GetFramerate() const {
		return m_framerate;
	}
}