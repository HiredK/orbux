/**
* @file FileHandle.h
* @brief
*/

#pragma once

#include <algorithm>
#include <memory>
#include <vector>

namespace orb
{
	// Forward declarations:
	class Engine;

	class FileHandle
	{
		public:
			//! TYPEDEF/ENUMS:
			typedef std::vector<std::shared_ptr<FileHandle>> List;
			enum State { Empty, Created, Missing };

			//! CTOR/DTOR:
			FileHandle(Engine* engine_instance, const std::string& path);
			virtual ~FileHandle();

			//! SERVICES:
			bool Process();

			//! OPERATORS:
			friend bool operator==(FileHandle& a, FileHandle& b);
			friend bool operator!=(FileHandle& a, FileHandle& b);

			//! ACCESSORS:
			Engine* GetEngineInstance() const;
			const std::string& GetPath() const;
			const void* GetData() const;
			std::size_t GetSize() const;
			void SetAllocated(bool allocated);
			bool IsAllocated() const;
			State GetState() const;

		protected:
			//! VIRTUALS:
			virtual bool CreateData();
			virtual void DeleteData();

			//! SERVICES:
			void ScanFileForInclude(std::string& input);

			//! MEMBERS:
			Engine* m_engine_instance;
			std::string m_path;
			std::string m_data;
			bool m_allocated;
			State m_state;
	};

	////////////////////////////////////////////////////////////////////////////////
	// FileHandle inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline FileHandle:: FileHandle(Engine* engine_instance, const std::string& path)
		: m_engine_instance(engine_instance)
		, m_path(path)
		, m_state(Empty)
		, m_allocated(false) {
	}
	/*----------------------------------------------------------------------------*/
	inline FileHandle::~FileHandle() {
	}

	/*----------------------------------------------------------------------------*/
	inline bool operator==(FileHandle& a, FileHandle& b) {
		return (a.m_path == b.m_path);
	}
	/*----------------------------------------------------------------------------*/
	inline bool operator!=(FileHandle& a, FileHandle& b) {
		return !(a == b);
	}

	/*----------------------------------------------------------------------------*/
	inline Engine* FileHandle::GetEngineInstance() const {
		return m_engine_instance;
	}
	/*----------------------------------------------------------------------------*/
	inline const std::string& FileHandle::GetPath() const {
		return m_path;
	}
	/*----------------------------------------------------------------------------*/
	inline const void* FileHandle::GetData() const {
		return &m_data[0];
	}
	/*----------------------------------------------------------------------------*/
	inline std::size_t FileHandle::GetSize() const {
		return m_data.size();
	}
	/*----------------------------------------------------------------------------*/
	inline FileHandle::State FileHandle::GetState() const {
		return m_state;
	}
	/*----------------------------------------------------------------------------*/
	inline void FileHandle::SetAllocated(bool allocated) {
		m_allocated = allocated;
	}
	/*----------------------------------------------------------------------------*/
	inline bool FileHandle::IsAllocated() const {
		return m_allocated;
	}

} // orb namespace