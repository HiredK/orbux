/**
* @file Engine.h
* @brief
*/

#pragma once

////////////////////////////////////////
// Version:

#define ENGINE_VER_MAJOR 1
#define ENGINE_VER_MINOR 0
#define ENGINE_VER_PATCH 0

#include "Exports.h"
#include "System.h"

namespace orb
{
	class Engine : public System
	{
		public:
			//! CTOR/DTOR:
			Engine();
			virtual ~Engine();

			//! VIRTUALS:
			bool Init(int argc, char** argv) override;
			void Cleanup() override;

			//! SERVICES:
			int Execute(int argc, char** argv);

			//! ACCESSORS:
			template<typename T> T* GetSystem();

		private:
			//! INTERNAL:
			std::string PrintSDL() const;

			//! MEMBERS:
			System::List m_systems;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Engine inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Engine:: Engine()
		: System(nullptr) {

		m_version.major = ENGINE_VER_MAJOR;
		m_version.minor = ENGINE_VER_MINOR;
		m_version.patch = ENGINE_VER_PATCH;
	}
	/*----------------------------------------------------------------------------*/
	inline Engine::~Engine() {
	}

	/*----------------------------------------------------------------------------*/
	template<typename T> inline T* Engine::GetSystem()
	{
		auto it = std::find_if(m_systems.begin(), m_systems.end(), 
			[](const std::shared_ptr<System>& system) -> bool {
			return (dynamic_cast<T*>(system.get()) != nullptr);
		});

		return (it != m_systems.end()) ? (T*)(*it).get() : nullptr;
	}

} // orb namespace