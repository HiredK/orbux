/**
* @file Buffer.h
* @brief
*/

#pragma once

#include "Exports.h"
#include <inttypes.h>

namespace orb
{
	// Forward declarations:
	class Renderer;
	class Command;

	class RENDERER_API Buffer
	{
		public:
			//! TYPEDEF/ENUMS:
			enum UsageFlags
			{
				eTransferSrc     = 1 << 1,
				eTransferDst     = 1 << 2,
				eUniformTexel    = 1 << 3,
				eStorageTexel    = 1 << 4,
				eUniform         = 1 << 5,
				eStorage         = 1 << 6,
				eIndex           = 1 << 7,
				eVertex          = 1 << 8,
				eIndirect        = 1 << 9
			};

			enum MemoryPropertyFlags
			{
				eDeviceLocal     = 1 << 1,
				eHostVisible     = 1 << 2,
				eHostCoherent    = 1 << 3,
				eHostCached      = 1 << 4,
				eLazilyAllocated = 1 << 5
			};

			struct CreateInfo
			{
				//! SERVICES:
				static CreateInfo GetDefault();
				static CreateInfo GetStaging();

				//! MEMBERS:
				uint32_t usage;
				uint32_t memProps;
				bool dynamic;
			};

			//! CTOR/DTOR:
			Buffer(Renderer* renderer);
			virtual ~Buffer();

			//! VIRTUALS:
			virtual void Build(uint32_t size, const void* data, const CreateInfo& ci) = 0;
			virtual void* Map(uint32_t size, uint32_t offset) = 0;
			virtual void Unmap() = 0;
			virtual void Flush() = 0;
			virtual void Bind(Command* cmd) = 0;
			virtual void Cleanup() = 0;

			//! ACCESSORS:
			void* GetMappedMemory() const;
			uint32_t GetSize() const;
			bool IsDynamic() const;
			bool IsMapped() const;

		protected:
			//! MEMBERS:
			Renderer* m_renderer;
			uint32_t m_size;
			uint32_t m_usage;
			bool m_isDynamic;
			bool m_isMapped;
			void* m_mapped;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Buffer::CreateInfo inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Buffer::CreateInfo Buffer::CreateInfo::GetDefault() {
		return CreateInfo() = { 
			eTransferDst, 
			eDeviceLocal,
			false
		};
	}
	/*----------------------------------------------------------------------------*/
	inline Buffer::CreateInfo Buffer::CreateInfo::GetStaging() {
		return CreateInfo() = { 
			eTransferSrc, 
			eHostVisible,
			false
		};
	}

	////////////////////////////////////////////////////////////////////////////////
	// Buffer inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Buffer:: Buffer(Renderer* renderer)
		: m_renderer(renderer)
		, m_size(0)
		, m_usage(eTransferDst)
		, m_isDynamic(false)
		, m_isMapped(false)
		, m_mapped(nullptr) {
	}
	/*----------------------------------------------------------------------------*/
	inline Buffer::~Buffer() {
	}

	/*----------------------------------------------------------------------------*/
	inline void* Buffer::GetMappedMemory() const {
		return m_mapped;
	}
	/*----------------------------------------------------------------------------*/
	inline uint32_t Buffer::GetSize() const {
		return m_size;
	}
	/*----------------------------------------------------------------------------*/
	inline bool Buffer::IsDynamic() const {
		return m_isDynamic;
	}
	/*----------------------------------------------------------------------------*/
	inline bool Buffer::IsMapped() const {
		return m_isMapped;
	}
}