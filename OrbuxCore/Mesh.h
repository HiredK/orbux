/**
* @file Mesh.h
* @brief
*/

#pragma once

#include "Buffer.h"
#include "Color.h"
#include "Format.h"
#include "MathDef.h"
#include <map>
#include <memory>

namespace orb
{
	// Forward declarations:
	class Renderer;
	class Command;

	class Mesh
	{
		public:
			//! TYPEDEF/ENUMS:
			enum Topology
			{
				ePointList,
				eLineList,
				eLineStrip,
				eTriangleList,
				eTriangleStrip,
				eTriangleFan,
				eLineListWithAdjacency,
				eLineStripWithAdjacency,
				eTriangleListWithAdjacency,
				eTriangleStripWithAdjacency,
				ePatchList
			};

			struct Vertex2D
			{
				float x, y;
				float s, t;
				unsigned char r, g, b, a;
			};

			struct Vertex3D
			{
				float x, y, z;
				float s, t;
			};

			struct VertexAttribute
			{
				std::string name;
				uint32_t offset;
				Format format;
			};

			typedef std::map<uint32_t, VertexAttribute> VertexInput;

			//! CTOR/DTOR:
			Mesh(Renderer* renderer);
			virtual ~Mesh();

			//! VIRTUALS:
			virtual void Build(
				uint32_t vertexSize = 0,
				const void* vertexData = nullptr,
				uint32_t indexSize = 0,
				const void* indexData = nullptr,
				const Buffer::CreateInfo& ci = Buffer::CreateInfo::GetDefault()
			);

			virtual void Draw(
				Command* cmd,
				uint32_t instanceCount = 1,
				uint32_t firstIndex = 0,
				int32_t vertexOffset = 0,
				uint32_t firstInstance = 0
			);

			virtual void Cleanup();

			//! SERVICES:
			static VertexInput GetVertexInput2D();
			static VertexInput GetVertexInput3D();

			static std::shared_ptr<Mesh> BuildCube(Renderer* renderer, const vec3& min, const vec3& max);
			static std::shared_ptr<Mesh> BuildCube(Renderer* renderer, float size);
			static std::shared_ptr<Mesh> BuildGrid(Renderer* renderer, unsigned int w, unsigned int h);
			static std::shared_ptr<Mesh> BuildPatch(Renderer* renderer, unsigned int w, unsigned int h);
			static std::shared_ptr<Mesh> BuildQuad(Renderer* renderer, float w, float h, const Color& color);
			static std::shared_ptr<Mesh> BuildQuad(Renderer* renderer);

			//! ACCESSORS:
			Buffer* GetVBO() const;
			Buffer* GetIBO() const;
			void SetIndexCount(size_t count);
			size_t GetIndexCount() const;

		protected:
			//! MEMBERS:
			Renderer* m_renderer;
			std::shared_ptr<Buffer> m_vbo;
			std::shared_ptr<Buffer> m_ibo;
			size_t m_indexCount;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Mesh inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Mesh:: Mesh(Renderer* renderer)
		: m_renderer(renderer)
		, m_indexCount(0) {
	}
	/*----------------------------------------------------------------------------*/
	inline Mesh::~Mesh() {
	}

	/*----------------------------------------------------------------------------*/
	inline Mesh::VertexInput Mesh::GetVertexInput2D() {
		return {
			{ 0, { "POSITION", offsetof(Mesh::Vertex2D, x), Format::eRG32F } },
			{ 1, { "TEXCOORD", offsetof(Mesh::Vertex2D, s), Format::eRG32F } },
			{ 2, { "COLOR"   , offsetof(Mesh::Vertex2D, r), Format::eRGBA8 } }
		};
	}
	/*----------------------------------------------------------------------------*/
	inline Mesh::VertexInput Mesh::GetVertexInput3D() {
		return {
			{ 0, { "POSITION", offsetof(Mesh::Vertex3D, x), Format::eRGB32F } },
			{ 1, { "TEXCOORD", offsetof(Mesh::Vertex3D, s), Format::eRG32F  } }
		};
	}

	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<Mesh> Mesh::BuildCube(Renderer* renderer, float size) {
		return Mesh::BuildCube(renderer, vec3(size * 0.5f), vec3(-size * 0.5f));
	}
	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<Mesh> Mesh::BuildQuad(Renderer* renderer) {
		return Mesh::BuildQuad(renderer, 1.0f, 1.0f, Color::White);
	}

	/*----------------------------------------------------------------------------*/
	inline Buffer* Mesh::GetVBO() const {
		return m_vbo.get();
	}
	/*----------------------------------------------------------------------------*/
	inline Buffer* Mesh::GetIBO() const {
		return m_ibo.get();
	}
	/*----------------------------------------------------------------------------*/
	inline void Mesh::SetIndexCount(size_t count) {
		m_indexCount = count;
	}
	/*----------------------------------------------------------------------------*/
	inline size_t Mesh::GetIndexCount() const {
		return m_indexCount;
	}
}