/**
* @file ThreadPool.h
* @brief Basic C++11 based thread pool with per-thread job queues.
*/

#pragma once

#include <vector>
#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <functional>

// make_unique is not available in C++11
// Taken from Herb Sutter's blog (https://herbsutter.com/gotw/_102/)
template<typename T, typename ...Args>
std::unique_ptr<T> make_unique(Args&& ...args)
{
	return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

namespace orb
{
	class ThreadPool
	{
		public:
			struct Thread
			{
				//! CTOR/DTOR:
				Thread();
				virtual ~Thread();

				//! SERVICES:
				void PushJob(std::function<void()> func);
				void Wait();

				//! INTERNAL
				void QueueLoop();

				//! MEMBERS:
				std::thread worker;
				std::queue<std::function<void()>> jobQueue;
				std::mutex queueMutex;
				std::condition_variable condition;
				bool destroying;
			};

			//! CTOR/DTOR:
			ThreadPool();
			ThreadPool(const ThreadPool&) = delete;
			ThreadPool& operator=(const ThreadPool&) = delete;
			virtual ~ThreadPool();

			//! SERVICES:
			void SetThreadCount(unsigned int count);
			unsigned int GetThreadCount() const;
			void PushJob(unsigned int index, std::function<void()> func);
			void Wait();

		private:
			//! MEMBERS:
			std::vector<std::unique_ptr<Thread>> m_threads;
	};

	////////////////////////////////////////////////////////////////////////////////
	// ThreadPool::Thread inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline ThreadPool::Thread:: Thread()
		: destroying(false)
	{
		worker = std::thread(&Thread::QueueLoop, this);
	}
	/*----------------------------------------------------------------------------*/
	inline ThreadPool::Thread::~Thread()
	{
		if (worker.joinable())
		{
			Thread::Wait();

			queueMutex.lock();
			destroying = true;
			condition.notify_one();
			queueMutex.unlock();
			worker.join();
		}
	}
	/*----------------------------------------------------------------------------*/
	inline void ThreadPool::Thread::PushJob(std::function<void()> func)
	{
		std::lock_guard<std::mutex> lock(queueMutex);
		jobQueue.push(std::move(func));
		condition.notify_one();
	}
	/*----------------------------------------------------------------------------*/
	inline void ThreadPool::Thread::Wait()
	{
		std::unique_lock<std::mutex> lock(queueMutex);
		condition.wait(lock, [this]() {
			return jobQueue.empty();
		});
	}
	/*----------------------------------------------------------------------------*/
	inline void ThreadPool::Thread::QueueLoop()
	{
		while (true)
		{
			std::function<void()> job;
			{
				std::unique_lock<std::mutex> lock(queueMutex);
				condition.wait(lock, [this] {
					return !jobQueue.empty() || destroying;
				});

				if (destroying) {
					break;
				}

				job = jobQueue.front();
			}

			job();

			{
				std::lock_guard<std::mutex> lock(queueMutex);
				jobQueue.pop();
				condition.notify_one();
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// ThreadPool inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline orb::ThreadPool:: ThreadPool() {
	}
	/*----------------------------------------------------------------------------*/
	inline orb::ThreadPool::~ThreadPool() {
	}

	/*----------------------------------------------------------------------------*/
	inline void orb::ThreadPool::SetThreadCount(unsigned int count)
	{
		m_threads.clear();
		for (unsigned int i = 0; i < count; i++) {
			m_threads.push_back(make_unique<Thread>());
		}
	}
	/*----------------------------------------------------------------------------*/
	inline unsigned int orb::ThreadPool::GetThreadCount() const {
		return static_cast<unsigned int>(m_threads.size());
	}
	/*----------------------------------------------------------------------------*/
	inline void orb::ThreadPool::PushJob(unsigned int index,
		std::function<void()> func)
	{
		m_threads[index]->PushJob(std::move(func));
	}
	/*----------------------------------------------------------------------------*/
	inline void orb::ThreadPool::Wait()
	{
		for (auto &thread : m_threads) {
			thread->Wait();
		}
	}

} // orb namespace