#include "FileSystem.h"
#include "FileFactory.h"
#include "InputParser.h"
#include "Logger.h"
#include <physfs.h>

bool orb::FileSystem::Init(int argc, char** argv)
{
	LOG() << "Init FileSystem v" << m_version.Print() << " (Using PhysFS v" << FileSystem::PrintPhysFS() << ")";

	if (PHYSFS_init(argv[0]) &&
		PHYSFS_addToSearchPath(PHYSFS_getBaseDir(), 1) &&
		PHYSFS_setWriteDir(PHYSFS_getBaseDir()))
	{
		auto input = std::unique_ptr<InputParser>(new InputParser(argc, argv));
		for (auto& path : input->GetOption("-mount")) {
			FileSystem::Mount(path);
		}

		return true;
	}

	LOG(Logger::Error) << PHYSFS_getLastError();
	return false;
}

orb::FileSystem::Group* orb::FileSystem::Mount(const std::string& path)
{
	Group* group = nullptr;
	auto it = std::find_if(m_groups.begin(), m_groups.end(),
		[&path](const std::shared_ptr<Group>& group) -> bool {
		return (group->path == path);
	});

	if (it == m_groups.end())
	{
		if (PHYSFS_mount(path.c_str(), path.c_str(), 1)) {
			m_groups.push_back(std::shared_ptr<Group>(new Group{ {}, path }));
			group = m_groups.back().get();
		}

		if (group == nullptr) {
			LOG(Logger::Error) << path << ": " << PHYSFS_getLastError();
		}
		else {
			LOG() << path << ": Added to search path";
			Discover(group, path + "/");
		}
	}
	else {
		LOG() << path << ": Already mounted";
		group = (*it).get();
	}

	return group;
}

orb::FileHandle* orb::FileSystem::Search(const std::string& path, bool allocate)
{
	std::vector<FileHandle*> results;

	for (auto& group : m_groups) {
		auto search = group->files.find(FileFactory::ExtractExtensionFromPath(path));
		if (search != group->files.end())
		{
			auto it = std::find_if(search->second.begin(), search->second.end(),
				[&group, &path](const std::shared_ptr<FileHandle>& file) -> bool {
				return (
					file->GetPath() == group->path + "/" + path ||
					file->GetPath() == path
				);
			});

			if (it != search->second.end()) {
				results.push_back((*it).get());
				continue;
			}
		}
	}

	if (!results.empty())
	{
		if (allocate) {
			results.back()->SetAllocated(true);
			results.back()->Process();
		}

		return results.back();
	}

	return nullptr;
}

void orb::FileSystem::Discover(Group* group, const std::string& path)
{
	char** rc = PHYSFS_enumerateFiles(path.c_str());
	for (char **i = rc; *i != 0; ++i)
	{
		std::string current = path + *i;
		if (!PHYSFS_isDirectory(current.c_str()))
		{
			std::string ext = FileFactory::ExtractExtensionFromPath(current);
			auto it = std::find_if(group->files[ext].begin(), group->files[ext].end(),
				[&current](const std::shared_ptr<FileHandle>& file) -> bool {
				return file->GetPath() == current;
			});

			if (it == group->files[ext].end()) {
				FileHandle* file = FileFactory::Detect(m_engine_instance, current);
				if (file == nullptr) {
					file = new FileHandle(m_engine_instance, current);
				}

				group->files[ext].push_back(std::shared_ptr<FileHandle>(file));
			}
		}
		else {
			Discover(group, path + *i + '/');
		}
	}

	PHYSFS_freeList(rc);
}

std::string orb::FileSystem::PrintPhysFS() const
{
	return (
		std::to_string(PHYSFS_VER_MAJOR) + "." +
		std::to_string(PHYSFS_VER_MINOR) + "." +
		std::to_string(PHYSFS_VER_PATCH)
	);
}

void orb::FileSystem::Cleanup()
{
	if (!PHYSFS_deinit()) {
		LOG(Logger::Error) << PHYSFS_getLastError();
	}
}