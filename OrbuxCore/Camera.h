/**
* @file Camera.h
* @brief
*/

#pragma once

#include "MathDef.h"
#include "Signal.h"

namespace orb
{
	class Camera
	{
		public:
			//! TYPEDEF/ENUMS:
			struct Matrices
			{
				dmat4 proj;
				dmat4 view;
				dmat4 viewProj;
			};

			//! CTOR/DTOR:
			Camera();
			Camera(float left, float right, float bottom, float top, float near, float far);
			Camera(float fov, float aspect, float near, float far);
			Camera(const Camera& other);
			virtual ~Camera();

			//! SERVICES:
			void Rebuild();

			//! ACCESSORS:
			dmat4 GetViewMatrix() const;
			dmat4 GetProjMatrix() const;
			dmat4 GetViewProjMatrix() const;
			void SetFieldOfView(float fov);
			float GetFieldOfView() const;
			void SetAspect(float aspect);
			float GetAspect() const;
			void SetPosition(const dvec3& pos);
			dvec3& GetPosition();
			void SetRotation(const dquat& rot);
			dquat& GetRotation();

			//! SIGNALS:
			Signal<const Matrices*> onRebuild;

		private:
			//! MEMBERS:
			mutable Matrices m_matrices;
			vec4 m_bounds;
			float m_fov;
			float m_aspect;
			vec2 m_clip;
			dvec3 m_position;
			dquat m_rotation;
			bool m_isOrtho;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Camera inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Camera:: Camera()
		: m_bounds(vec4(0.0f, 1.0f, 1.0f, 0.0f))
		, m_fov(glm::radians(60.0f))
		, m_clip(vec2(0.0f, 100.0f))
		, m_position(dvec3(0.0))
		, m_rotation(dquat(1.0, 0.0, 0.0, 0.0))
		, m_isOrtho(false) {
	}
	/*----------------------------------------------------------------------------*/
	inline Camera::Camera(float left, float right, float bottom, float top, float near, float far)
		: Camera()
	{
		m_bounds = vec4(left, right, bottom, top);
		m_clip = vec2(near, far);
		m_isOrtho = true;
		Camera::Rebuild();
	}
	/*----------------------------------------------------------------------------*/
	inline Camera::Camera(float fov, float aspect, float near, float far)
		: Camera()
	{
		m_fov = fov;
		m_aspect = aspect;
		m_clip = vec2(near, far);
		m_isOrtho = false;
		Camera::Rebuild();
	}
	/*----------------------------------------------------------------------------*/
	inline Camera::Camera(const Camera& other)
		: Camera()
	{
		m_bounds = other.m_bounds;
		m_fov = other.m_fov;
		m_aspect = other.m_aspect;
		m_clip = other.m_clip;
		m_position = other.m_position;
		m_rotation = other.m_rotation;
		m_isOrtho = other.m_isOrtho;
		Camera::Rebuild();
	}
	/*----------------------------------------------------------------------------*/
	inline Camera::~Camera() {
	}

	/*----------------------------------------------------------------------------*/
	inline dmat4 Camera::GetViewMatrix() const {
		return m_matrices.view;
	}
	/*----------------------------------------------------------------------------*/
	inline dmat4 Camera::GetProjMatrix() const {
		return m_matrices.proj;
	}
	/*----------------------------------------------------------------------------*/
	inline dmat4 Camera::GetViewProjMatrix() const {
		return m_matrices.viewProj;
	}
	/*----------------------------------------------------------------------------*/
	inline void Camera::SetFieldOfView(float fov) {
		m_fov = fov;
	}
	/*----------------------------------------------------------------------------*/
	inline float Camera::GetFieldOfView() const {
		return m_fov;
	}
	/*----------------------------------------------------------------------------*/
	inline void Camera::SetAspect(float aspect) {
		m_aspect = aspect;
	}
	/*----------------------------------------------------------------------------*/
	inline float Camera::GetAspect() const {
		return m_aspect;
	}
	/*----------------------------------------------------------------------------*/
	inline void Camera::SetPosition(const dvec3& pos) {
		m_position = pos;
	}
	/*----------------------------------------------------------------------------*/
	inline dvec3& Camera::GetPosition() {
		return m_position;
	}
	/*----------------------------------------------------------------------------*/
	inline void Camera::SetRotation(const dquat& rot) {
		m_rotation = rot;
	}
	/*----------------------------------------------------------------------------*/
	inline dquat& Camera::GetRotation() {
		return m_rotation;
	}
}