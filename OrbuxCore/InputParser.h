/**
* @file InputParser.h
* @brief Header only command line input parser.
*/

#pragma once

#include <string>
#include <vector>

namespace orb
{
	class InputParser
	{
		public:
			//! CTOR/DTOR:
			InputParser(int argc, char** argv);
			virtual ~InputParser();

			//! SERVICES:
			std::vector<std::string> GetOption(const std::string& option) const;
			bool OptionExists(const std::string& option) const;

		private:
			//! MEMBERS:
			std::vector<std::string> m_tokens;
	};

	////////////////////////////////////////////////////////////////////////////////
	// InputParser inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline InputParser:: InputParser(int argc, char** argv) {
		for (int i = 1; i < argc; ++i) {
			m_tokens.push_back(argv[i]);
		}
	}
	/*----------------------------------------------------------------------------*/
	inline InputParser::~InputParser() {
		m_tokens.clear();
	}

	/*----------------------------------------------------------------------------*/
	inline std::vector<std::string> InputParser::GetOption(const std::string& option) const
	{
		std::vector<std::string> result;
		std::vector<std::string>::const_iterator it = m_tokens.begin();
		while (it != m_tokens.end()) {
			if ((*it).find(option) != std::string::npos) {
				if (++it != m_tokens.end()) {
					result.push_back(*it);
				}
			}
			else {
				++it;
			}
		}

		return result;
	}
	/*----------------------------------------------------------------------------*/
	inline bool InputParser::OptionExists(const std::string& option) const {
		return std::find(m_tokens.begin(), m_tokens.end(), option) != m_tokens.end();
	}

} // orb namespace