/**
* @file Terrain.h
* @brief
*/

#pragma once

#define MAX_TILE_INSTANCES 150

#include "Frustum.h"
#include "Camera.h"
#include "Mesh.h"
#include <vector>

namespace orb
{
	// Forward declarations:
	class Renderer;
	class Command;
	class Buffer;

	class Terrain
	{
		public:
			friend class TileProducer;

			//! TYPEDEF/ENUMS:
			enum Face
			{
				ePosX,
				eNegX,
				ePosY,
				eNegY,
				ePosZ,
				eNegZ
			};

			struct Coord
			{
				int tx, ty;       // The logical x, y coordinate (between 0 and 2^level)
				double ox, oy;    // The physical x, y coordinate
				double length;    // The physical length
				float zmin, zmax; // The minimum/maximum elevation
			};

			struct CreateInfo
			{
				//! SERVICES:
				static CreateInfo GetDefault();

				//! MEMBERS:
				unsigned int maxLevel;
				unsigned int gridSize;
				bool spherical;
			};

			//! CTOR/DTOR:
			Terrain(Renderer* renderer, double size);
			virtual ~Terrain();

			////////////////////////////////////////////////////////////////////////
			// Terrain::Build:
			////////////////////////////////////////////////////////////////////////
			/*--------------------------------------------------------------------*/
			static std::shared_ptr<Terrain> Build(
				Renderer* renderer,
				double size,
				const CreateInfo& ci
			);

			//! SERVICES:
			void Initialize(const CreateInfo& ci);
			void Draw(Command* cmd);
			void Cleanup();

			//! ACCESSORS:
			std::shared_ptr<TileProducer> AddTileProducer(TileProducer* tp);
			Buffer* GetDynamicUBO() const;
			Renderer* GetRenderer() const;

		protected:
			struct Tile
			{
				std::unique_ptr<Tile> children[4];
				Tile* parent;
				Coord coord;
				int level;
				Frustum::Visibility visibility;
				bool occluded;
			};

			struct DynamicUniformData
			{
				mat4 localToWorld;
				mat4 screenQuadCorners;
				mat4 screenQuadVerticals;
				vec4 screenQuadCornerNorms;
				vec4 offset;
				vec4 camera;
				float splitDist;
				float radius;
			};

			struct Deformation
			{
				virtual dvec3 LocalToDeformed(const dvec3& local) = 0;
				virtual dmat4 LocalToDeformedDifferential(const dvec3& local, bool clamp) = 0;
				virtual dvec3 DeformedToLocal(const dvec3& deformed) = 0;
				virtual Box2D DeformedToLocalBounds(const dvec3& center, double radius) = 0;
				virtual dmat4 DeformedToTangentFrame(const dvec3& deformed) = 0;
				virtual Frustum::Visibility GetVisibility(
					const dvec3& pos, const Frustum& frustum, const Box3D& box) = 0;
				virtual DynamicUniformData GetUniformData(
					const dmat4& ltw, const dmat4& lts, int level, const Coord& coord) = 0;
			};

			//! MEMBERS:
			Renderer* m_renderer;
			std::unique_ptr<Deformation> m_deformation;
			std::unique_ptr<Tile> m_root;
			std::shared_ptr<Mesh> m_mesh;
			std::vector<std::shared_ptr<TileProducer>> m_tileProducers;
			std::shared_ptr<Buffer> m_dynamicUBO;
			DynamicUniformData* m_dynamicData;
			double m_size;
			unsigned int m_maxLevel;
			bool m_isSpherical;
			Frustum m_localFrustum;
			int m_horizonSize;
			float* m_horizon;
			bool m_splitInvisibleTiles;
			bool m_horizonCulling;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Terrain::CreateInfo inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Terrain::CreateInfo Terrain::CreateInfo::GetDefault() {
		return CreateInfo() = { 8, 25, false };
	}

	////////////////////////////////////////////////////////////////////////////////
	// Terrain inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Terrain:: Terrain(Renderer* renderer, double size)
		: m_renderer(renderer)
		, m_size(size)
		, m_maxLevel(0)
		, m_isSpherical(false)
		, m_horizonSize(256)
		, m_horizon(new float[m_horizonSize])
		, m_splitInvisibleTiles(false)
		, m_horizonCulling(true) {
	}
	/*----------------------------------------------------------------------------*/
	inline Terrain::~Terrain() {
	}

	/*----------------------------------------------------------------------------*/
	inline Buffer* Terrain::GetDynamicUBO() const {
		return m_dynamicUBO.get();
	}
	/*----------------------------------------------------------------------------*/
	inline Renderer* Terrain::GetRenderer() const {
		return m_renderer;
	}
}