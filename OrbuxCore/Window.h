/**
* @file Window.h
* @brief
*/

#pragma once

#include "Signal.h"
#include "Canvas.h"
#include "Renderer.h"
#include <memory>

// Forward declarations:
struct SDL_Window;
struct SDL_SysWMinfo;
union SDL_Event;

namespace orb
{
	class Engine;

	class Window
	{
		public:
			//! TYPEDEF/ENUMS:
			enum UsageFlags
			{
				eFullscreen        = 1 << 1,
				eShown             = 1 << 2,
				eHidden            = 1 << 3,
				eBorderless        = 1 << 4,
				eResizable         = 1 << 5,
				eMinimized         = 1 << 6,
				eMaximized         = 1 << 7,
				eGrabbed           = 1 << 8,
				eInputFocus        = 1 << 9,
				eMouseFocus        = 1 << 10,
				eFullscreenDesktop = 1 << 11,
				eForeign           = 1 << 12,
				eHeighDPI          = 1 << 13,
				eMouseCapture      = 1 << 14,
				eAlwaysOnTop       = 1 << 15,
				eSkipTaskbar       = 1 << 16,
				eUtility           = 1 << 17,
				eTooltip           = 1 << 18,
				ePopupMenu         = 1 << 19
			};

			//! CTOR/DTOR:
			Window(Engine* engine_instance);
			virtual ~Window();

			//! SERVICES:
			void Open(
				int w,
				int h, 
				const std::string& title, 
				Renderer::Type type, 
				uint32_t usage = eShown | eResizable
			);

			void PollEvent();
			void Display();
			void Close();

			//! ACCESSORS:
			Renderer* GetRenderer() const;
			SDL_SysWMinfo* GetSysWMinfo() const;
			bool IsKeyPressed(int key) const;
			bool IsMouseButtonPressed(unsigned char button) const;
			bool IsFullscreen() const;
			bool IsFocused() const;
			int GetWidth() const;
			int GetHeight() const;
			Canvas* GetCanvas() const;

			//! SIGNALS:
			Signal<const SDL_Event*> onEvent;
			Signal<const SDL_Event*, const dvec2&> onMouseMotion;

		protected:
			//! MEMBERS:
			Engine* m_engine_instance;
			std::shared_ptr<Renderer> m_renderer;
			SDL_Window* m_SDLWindow;
			SDL_SysWMinfo* m_sysWMinfo;
			std::map<int, bool> m_keyPressed;
			std::map<unsigned char, bool> m_mouseButtonPressed;
			std::shared_ptr<Canvas> m_canvas;
			uint32_t m_usage;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Window inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Window:: Window(Engine* engine_instance)
		: m_engine_instance(engine_instance)
		, m_SDLWindow(nullptr)
		, m_sysWMinfo(nullptr)
		, m_usage(0) {
	}
	/*----------------------------------------------------------------------------*/
	inline Window::~Window() {
		Window::Close();
	}

	/*----------------------------------------------------------------------------*/
	inline Renderer* Window::GetRenderer() const {
		return m_renderer.get();
	}
	/*----------------------------------------------------------------------------*/
	inline SDL_SysWMinfo* Window::GetSysWMinfo() const {
		return m_sysWMinfo;
	}
	/*----------------------------------------------------------------------------*/
	inline bool Window::IsFullscreen() const {
		return m_usage & eFullscreen;
	}
	/*----------------------------------------------------------------------------*/
	inline bool Window::IsKeyPressed(int key) const {
		if (m_keyPressed.find(key) != m_keyPressed.end()) {
			return (*m_keyPressed.find(key)).second;
		}

		return false;
	}
	/*----------------------------------------------------------------------------*/
	inline bool Window::IsMouseButtonPressed(unsigned char button) const {
		if (m_mouseButtonPressed.find(button) != m_mouseButtonPressed.end()) {
			return (*m_mouseButtonPressed.find(button)).second;
		}

		return false;
	}
	/*----------------------------------------------------------------------------*/
	inline Canvas* Window::GetCanvas() const {
		return m_canvas.get();
	}

} // orb namespace