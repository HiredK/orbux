/**
* @file Exports.h
* @brief
*/

#pragma once

#pragma warning( disable: 4251 ) // std::shared_ptr usage across dlls

#ifdef RENDERER_EXPORTS  
#define RENDERER_API __declspec(dllexport)   
#else  
#define RENDERER_API __declspec(dllimport)   
#endif