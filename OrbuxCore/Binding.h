/**
* @file Binding.h
* @brief
*/

#pragma once

#include "Binding_Core.h"
#include "Binding_Event.h"
#include "Binding_FileSystem.h"
#include "Binding_GUI.h"
#include "Binding_Logger.h"
#include "Binding_Math.h"
#include "Binding_Rendering.h"
#include "Engine.h"

namespace orb
{
	////////////////////////////////////////////////////////////////////////////////
	// BindLuaContext:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	static lua_State* BuildLuaContext(Engine* engine_instance)
	{
		lua_State* L = luaL_newstate();
		luaL_openlibs(L);

		Binding_Logger_OverridePrintLib(L);
		luabind::open(L);

		luabind::module(L)
		[
			Binding_Core(),
			Binding_Event(),
			Binding_FileSystem(),
			Binding_GUI(),
			Binding_Logger(),
			Binding_Math(),
			Binding_Rendering()
		];

		luabind::globals(L)["orb"]["Color"]["Black"] = Color::Black;
		luabind::globals(L)["orb"]["Color"]["White"] = Color::White;
		luabind::globals(L)["orb"]["Color"]["Red"] = Color::Red;
		luabind::globals(L)["orb"]["Color"]["Green"] = Color::Green;
		luabind::globals(L)["orb"]["Color"]["Blue"] = Color::Blue;
		luabind::globals(L)["orb"]["Color"]["Yellow"] = Color::Yellow;
		luabind::globals(L)["orb"]["Color"]["Magenta"] = Color::Magenta;
		luabind::globals(L)["orb"]["Color"]["Cyan"] = Color::Cyan;
		luabind::globals(L)["orb"]["Color"]["Transparent"] = Color::Transparent;

		luabind::globals(L)["orb"]["Undefined"] = (int)Format::eUndefined;
		luabind::globals(L)["orb"]["R32F"] = (int)Format::eR32F;
		luabind::globals(L)["orb"]["RG32F"] = (int)Format::eRG32F;
		luabind::globals(L)["orb"]["RGB32F"] = (int)Format::eRGB32F;
		luabind::globals(L)["orb"]["RGBA32F"] = (int)Format::eRGBA32F;
		luabind::globals(L)["orb"]["R8"] = (int)Format::eR8;
		luabind::globals(L)["orb"]["RG8"] = (int)Format::eRG8;
		luabind::globals(L)["orb"]["RGBA8"] = (int)Format::eRGBA8;

		luabind::globals(L)["FileSystem"] = engine_instance->GetSystem<FileSystem>();
		luabind::globals(L)["Engine"] = engine_instance;
		return L;
	}

} // orb namespace