/**
* @file Binding_Math.h
* @brief
*/

#pragma once

#include "Binding_Core.h"
#include "MathDef.h"

namespace orb
{
	/*----------------------------------------------------------------------------*/
	template<typename T>
	static luabind::object matrix_to_table(lua_State* L, const T& v)
	{
		auto* raw = static_cast<const T::value_type*>(glm::value_ptr(v));
		luabind::object output = luabind::newtable(L);

		for (glm::length_t i = 0; i < v.length() * v[0].length(); ++i) {
			output[i + 1] = raw[i];
		}

		return output;
	}
	/*----------------------------------------------------------------------------*/
	template<typename T>
	static T matrix_from_table(const luabind::object& table)
	{
		std::vector<T::value_type> raw;
		for (luabind::iterator i(table), end; i != end; ++i) {
			raw.push_back(luabind::object_cast<T::value_type>(*i));
		}

		switch (raw.size()) {
			case  9: return static_cast<T>(glm::make_mat3(raw.data()));
			case 16: return static_cast<T>(glm::make_mat4(raw.data()));
			default:
				LOG(Logger::Warning) << "Invalid table size!";
				return T();
		}
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Math()
	{
		return (
			luabind::namespace_("math")
			[
				luabind::def("column", (dvec4(*)(const dmat4&, int)) &glm::column),
				luabind::def("row", (dvec4(*)(const dmat4&, int)) &glm::row),
				luabind::def("mat3_cast", (dmat3(*)(const dquat&)) &glm::mat3_cast),
				luabind::def("mat4_cast", (dmat4(*)(const dquat&)) &glm::mat4_cast),

				luabind::def("row_major", (dmat3(*)(const dmat3&)) &glm::rowMajor3),
				luabind::def("row_major", (dmat4(*)(const dmat4&)) &glm::rowMajor4),

				luabind::def("normalize", (dvec3(*)(const dvec3&)) &glm::normalize),
				luabind::def("normalize", (dquat(*)(const dquat&)) &glm::normalize),

				luabind::def("pow", (double(*)(double, double)) &glm::pow),

				luabind::def("degrees", (double(*)(double)) &glm::degrees),
				luabind::def("degrees", (dvec3(*)(const dvec3&)) &glm::degrees),
				luabind::def("radians", (double(*)(double)) &glm::radians),
				luabind::def("radians", (dvec3(*)(const dvec3&)) &glm::radians),

				luabind::def("translate", (dmat4(*)(const dmat4&, const dvec3&)) &glm::translate),
				luabind::def("angle_axis", (dquat(*)(const double&, const dvec3&)) &glm::angleAxis),
				luabind::def("rotation", (dquat(*)(const dvec3&, const dvec3&)) &glm::rotation),
				luabind::def("rotate", (dmat4(*)(const dmat4&, double, const dvec3&)) &glm::rotate),
				luabind::def("rotate", (dvec3(*)(const dquat&, const dvec3&)) &glm::rotate)
			],

			luabind::class_<dvec2>("vec2")
			.def(luabind::constructor<double, double>())
			.def(luabind::constructor<double>())
			.def(luabind::constructor<const dvec2&>())
			.def(luabind::constructor<const dvec3&>())
			.def(luabind::constructor<const dvec4&>())
			.def(luabind::constructor<>())
			.def(luabind::self + luabind::other<double>())
			.def(luabind::self + luabind::other<dvec2>())
			.def(luabind::self - luabind::other<double>())
			.def(luabind::self - luabind::other<dvec2>())
			.def(luabind::self * luabind::other<double>())
			.def(luabind::self * luabind::other<dvec2>())
			.def(luabind::self / luabind::other<double>())
			.def(luabind::self / luabind::other<dvec2>())
			.def_readwrite("x", &dvec2::x)
			.def_readwrite("y", &dvec2::y),

			luabind::class_<dvec3>("vec3")
			.def(luabind::constructor<double, double, double>())
			.def(luabind::constructor<double>())
			.def(luabind::constructor<const dvec2&, double>())
			.def(luabind::constructor<double, const dvec2&>())
			.def(luabind::constructor<const dvec3&>())
			.def(luabind::constructor<const dvec4&>())
			.def(luabind::constructor<>())
			.def(luabind::self + luabind::other<double>())
			.def(luabind::self + luabind::other<dvec3>())
			.def(luabind::self - luabind::other<double>())
			.def(luabind::const_self - luabind::other<const dvec3&>())
			.def(luabind::self * luabind::other<double>())
			.def(luabind::self * luabind::other<dvec3>())
			.def(luabind::self / luabind::other<double>())
			.def(luabind::self / luabind::other<dvec3>())
			.def(luabind::self * luabind::other<dmat3>())
			.def_readwrite("x", &dvec3::x)
			.def_readwrite("y", &dvec3::y)
			.def_readwrite("z", &dvec3::z),

			luabind::class_<dvec4>("vec4")
			.def(luabind::constructor<double, double, double, double>())
			.def(luabind::constructor<double>())
			.def(luabind::constructor<const dvec2&, double, double>())
			.def(luabind::constructor<double, double, const dvec2&>())
			.def(luabind::constructor<const dvec2&, const dvec2&>())
			.def(luabind::constructor<const dvec3&, double>())
			.def(luabind::constructor<double, const dvec3&>())
			.def(luabind::constructor<const dvec4&>())
			.def(luabind::constructor<>())
			.def(luabind::self + luabind::other<double>())
			.def(luabind::self + luabind::other<dvec4>())
			.def(luabind::self - luabind::other<double>())
			.def(luabind::self - luabind::other<dvec4>())
			.def(luabind::self * luabind::other<double>())
			.def(luabind::self * luabind::other<dvec4>())
			.def(luabind::self / luabind::other<double>())
			.def(luabind::other<double>() / luabind::self)
			.def(luabind::self / luabind::other<dvec4>())
			.def_readwrite("x", &dvec4::x)
			.def_readwrite("y", &dvec4::y)
			.def_readwrite("z", &dvec4::z)
			.def_readwrite("w", &dvec4::w),

			luabind::class_<dquat>("quat")
			.def(luabind::constructor<double, double, double, double>())
			.def(luabind::constructor<const dvec3&>())
			.def(luabind::constructor<const dmat4&>())
			.def(luabind::constructor<const dquat&>())
			.def(luabind::constructor<>())
			.def(luabind::self * luabind::other<double>())
			.def(luabind::self * luabind::other<dquat>())
			.def(luabind::self * luabind::other<dvec3>())
			.def_readwrite("x", &dquat::x)
			.def_readwrite("y", &dquat::y)
			.def_readwrite("z", &dquat::z)
			.def_readwrite("w", &dquat::w),

			luabind::class_<dmat3>("mat3")
			.scope
			[
				luabind::def("to_table", (luabind::object(*)(lua_State* L, const dmat3&)) &matrix_to_table<dmat3>),
				luabind::def("from_table", (dmat3(*)(const luabind::object&)) &matrix_from_table<dmat3>)
			]
			.def(luabind::constructor<const dmat3&>())
			.def(luabind::constructor<const dmat4&>())
			.def(luabind::constructor<>())
			.def(luabind::self * luabind::other<dmat3>())
			.def(luabind::self * luabind::other<dvec3>()),

			luabind::class_<dmat4>("mat4")
			.scope
			[
				luabind::def("to_table", (luabind::object(*)(lua_State* L, const dmat4&)) &matrix_to_table<dmat4>),
				luabind::def("from_table", (dmat4(*)(const luabind::object&)) &matrix_from_table<dmat4>)
			]
			.def(luabind::constructor<const dmat4&>())
			.def(luabind::constructor<const dmat3&>())
			.def(luabind::constructor<>())
			.def(luabind::self * luabind::other<dmat4>())
			.def(luabind::self * luabind::other<dvec4>())
		);
	}

} // orb namespace