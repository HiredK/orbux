/**
* @file ScriptObject.h
* @brief
*/

#pragma once

#include "Signal.h"
#include <cstdint>
#include <thread>
#include <mutex>
#include <vector>

namespace orb
{
	// Forward declarations:
	class Script;

	class ScriptObject
	{
		public:
			//! TYPEDEF/ENUMS:
			enum Type
			{
				eStatic,
				eDynamic,
				eTypeCount 
			};

			//! CTORS/DTORS:
			ScriptObject(const std::string& name, Type type);
			virtual ~ScriptObject();

			//! VIRTUALS:
			virtual void Update(float dt) {}

			//! SERVICES:
			static void PushOwner(Script* owner);
			static void PopStack();
			static void UpdateAll(float dt);

			//! OPERATORS:
			friend bool operator==(ScriptObject& a, ScriptObject& b);
			friend bool operator!=(ScriptObject& a, ScriptObject& b);

			//! SIGNALS:
			Signal<float> onUpdate;

		protected:
			struct Reference
			{
				typedef std::map<std::thread::id, Reference> Map;
				std::vector<ScriptObject*> references[eTypeCount];
				bool changed;
			};

			//! SERVICES:
			static std::uint64_t GenerateID();

			//! MEMBERS:
			static std::map<std::thread::id, std::vector<Script*>> OwnerStack;
			static Reference::Map References;
			static std::mutex ReferenceMutex;
			Script* m_owner;

		private:
			std::uint64_t m_id;
			std::string m_name;
			Type m_type;
			bool m_updated;
	};

	////////////////////////////////////////////////////////////////////////////////
	// ScriptObject inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline bool operator==(ScriptObject& a, ScriptObject& b) {
		return (a.m_id == b.m_id);
	}
	/*----------------------------------------------------------------------------*/
	inline bool operator!=(ScriptObject& a, ScriptObject& b) {
		return !(a == b);
	}

	/*----------------------------------------------------------------------------*/
	inline std::uint64_t ScriptObject::GenerateID() {
		static std::uint64_t id = 0;
		return id++;
	}

} // orb namespace