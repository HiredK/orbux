/**
* @file FileSystem.h
* @brief http://icculus.org/physfs/
*/

#pragma once

////////////////////////////////////////
// Current Version:

#define FILESYSTEM_VER_MAJOR 1
#define FILESYSTEM_VER_MINOR 0
#define FILESYSTEM_VER_PATCH 0

#include "System.h"
#include "FileHandle.h"
#include <map>

namespace orb
{
	class FileSystem : public System
	{
		public:
			//! TYPEDEF/ENUMS:
			struct Group
			{
				//! TYPEDEF/ENUMS:
				typedef std::vector<std::shared_ptr<Group>> List;

				//! MEMBERS:
				std::map<std::string, FileHandle::List> files;
				std::string path;
			};

			//! CTOR/DTOR:
			FileSystem(Engine* engine_instance);
			virtual ~FileSystem();

			//! VIRTUALS:
			bool Init(int argc, char** argv) override;
			void Cleanup() override;

			//! SERVICES:
			Group* Mount(const std::string& path);
			FileHandle* Search(const std::string& path, bool allocate = false);

		private:
			//! INTERNAL:
			void Discover(Group* group, const std::string& path);
			std::string PrintPhysFS() const;

			//! MEMBERS:
			Group::List m_groups;
	};

	////////////////////////////////////////////////////////////////////////////////
	// FileSystem inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline FileSystem:: FileSystem(Engine* engine_instance)
		: System(engine_instance) {

		m_version.major = FILESYSTEM_VER_MAJOR;
		m_version.minor = FILESYSTEM_VER_MINOR;
		m_version.patch = FILESYSTEM_VER_PATCH;
	}
	/*----------------------------------------------------------------------------*/
	inline FileSystem::~FileSystem() {
	}

} // orb namespace