#include "Shader.h"
#include "Logger.h"
#include "ShaderConverter.h"
#include <picojson.h>

REGISTER_FILE_IMPL(orb::Shader, "shader")

bool orb::Shader::CreateData()
{
	bool success = true;
	if (!m_root)
	{
		Logger::ScopedTimer timer(LOG, "Shader::CreateData");

		FileHandle::CreateData();
		m_data.erase(std::remove(m_data.begin(), m_data.end(), '\n'), m_data.end());
		m_root = std::shared_ptr<picojson::value>(new picojson::value());
		const char* data = static_cast<const char*>(m_data.c_str());
		auto err = picojson::parse(*m_root, data, data + m_data.size());
		FileHandle::DeleteData();

		if (!err.empty()) {
			LOG(Logger::Error) << m_path << ": " << err;
			success = false;
		}

		if (success)
		{
			auto TextToFormat = std::map<std::string, Format>() =
			{
				{ "hlsl"   , eHLSL           },
				{ "glsl"   , eGLSL           }
			};

			auto TextToStage = std::map<std::string, Stage>() =
			{
				{ "shared" , eShared         },
				{ "vs"     , eVertex         },
				{ "tcs"    , eTessControl    },
				{ "tes"    , eTessEvaluation },
				{ "gs"     , eGeometry       },
				{ "fs"     , eFragment       },
				{ "cs"     , eCompute        }
			};

			const auto& shaders = m_root->get("shaders").get<picojson::array>();
			for (const auto& shader : shaders)
			{
				std::string format = shader.get("format").get<std::string>();
				std::string type = shader.get("type").get<std::string>();
				std::string data = shader.get("data").get<std::string>();

				m_stagesData[TextToStage[type]][TextToFormat[format]] = data;
			}

			for (unsigned int i = 0; i < eFormatNum; ++i) {
				const std::string shared = m_stagesData[eShared][i];
				for (unsigned int j = 0; j < eStageNum; ++j) {
					if (i == eSPIRV || j == eShared || 
						m_stagesData[j][i].empty()) {
						continue;
					}

					std::stringstream ss;
					if (i == eGLSL) ss << "#version 450\n";
					ss << shared;
					ss << m_stagesData[j][i];

					m_stagesData[j][i] = ss.str();
				}
			}

			for (unsigned int i = 0; i < eStageNum; ++i) {
				if (i == eShared ||
					m_stagesData[i][eHLSL].empty() ||
					m_stagesData[i][eGLSL].empty() == false) {
					continue;
				}

				std::string GLSL;

				try {
					GLSL = ShaderConverter::HLSLToGLSL(m_stagesData[i][eHLSL], (Stage)i);
				}
				catch (const ShaderConverter::Exception& e) {
					LOG(Logger::Error) << m_path << ": " << e.what();
				}

				m_stagesData[i][eGLSL] = GLSL;
			}

			for (unsigned int i = 0; i < eStageNum; ++i) {
				if (i == eShared || 
					m_stagesData[i][eGLSL].empty()) {
					continue;
				}

				std::string SPIRV;

				try {
					SPIRV = ShaderConverter::GLSLToSPIRV(m_stagesData[i][eGLSL], (Stage)i);
				}
				catch (const ShaderConverter::Exception& e) {
					LOG(Logger::Error) << m_path << ": " << e.what();
				}

				m_stagesData[i][eSPIRV] = SPIRV;
			}
		}
	}

	return success;
}

void orb::Shader::DeleteData()
{
}