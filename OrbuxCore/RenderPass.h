/**
* @file RenderPass.h
* @brief
*/

#pragma once

#include "Signal.h"
#include "Camera.h"
#include "Command.h"
#include "Mesh.h"
#include "Texture.h"
#include "ThreadPool.h"
#include <vector>

namespace orb
{
	// Forward declarations:
	class Renderer;
	class Drawable;
	class Camera;
	class Shader;

	class RENDERER_API RenderPass
	{
		public:
			//! TYPEDEF/ENUMS:
			enum Type
			{
				eDrawOnce,
				eDrawOnceBackground,
				eDrawSync,
				eDrawSyncBackground
			};

			enum BlendFactor
			{
				eZero,
				eOne,
				eSrcColor,
				eOneMinusSrcColor,
				eDstColor,
				eOneMinusDstColor,
				eSrcAlpha,
				eOneMinusSrcAlpha,
				eDstAlpha,
				eOneMinusDstAlpha,
				eConstantColor,
				eOneMinusConstantColor,
				eConstantAlpha,
				eOneMinusConstantAlpha,
				eSrcAlphaSaturate,
				eSrc1Color,
				eOneMinusSrc1Color,
				eSrc1Alpha,
				eOneMinusSrc1Alpha
			};

			enum BlendOp
			{
				eAdd,
				eSubtract,
				eReverseSubtract,
				eMin,
				eMax
			};

			struct Blending
			{
				//! SERVICES:
				static Blending GetDefault();
				static Blending GetAdditive();
				static Blending GetAdditiveAlpha();

				//! MEMBERS:
				bool blendEnable;
				BlendFactor srcColorBlendFactor;
				BlendFactor dstColorBlendFactor;
				BlendOp colorBlendOp;
				BlendFactor srcAlphaBlendFactor;
				BlendFactor dstAlphaBlendFactor;
				BlendOp alphaBlendOp;
			};

			struct Attachment
			{
				Texture* input;
				Blending blending;
			};

			struct CreateInfo
			{
				//! SERVICES:
				static CreateInfo GetDefault();

				//! MEMBERS:
				Type type;
				Camera* camera;
				Shader* shader;
				Mesh::Topology topology;
				Mesh::VertexInput vertexInput;
				uint32_t vertexStride;
				ivec2 resolution;
				Color clearColor;
				bool enableDepth;
				uint32_t numThreads;
				std::vector<Attachment> attachments;
				std::vector<Texture*> samplers;
				std::vector<Buffer*> ubos;
			};

			//! CTOR/DTOR:
			RenderPass(Renderer* renderer, RenderPass* parent);
			virtual ~RenderPass();

			//! VIRTUALS:
			virtual void Build(const CreateInfo& ci) = 0;
			virtual void Resize(int w, int h) = 0;
			virtual void Record() = 0;
			virtual void Cleanup() = 0;

			//! ACCESSORS:
			Renderer* GetRenderer() const;
			Camera* GetCamera() const;
			Type GetType() const;

			//! SIGNALS:
			Signal<Command*> onRecord;

		protected:
			struct CameraData
			{
				mat4 proj;
				mat4 view;
				mat4 viewProj;
				mat4 invProj;
				mat4 invView;
				vec2 viewport;
			};

			//! MEMBERS:
			Renderer* m_renderer;
			RenderPass* m_parent;
			CreateInfo m_createInfo;
			std::shared_ptr<Buffer> m_cameraUBO;
			CameraData m_cameraData;
			ThreadPool m_threadPool;
	};

	////////////////////////////////////////////////////////////////////////////////
	// RenderPass::Blending inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline RenderPass::Blending RenderPass::Blending::GetDefault() {
		return Blending() = { 
			false,
			BlendFactor::eZero,
			BlendFactor::eOne,
			BlendOp::eAdd,
			BlendFactor::eZero,
			BlendFactor::eZero,
			BlendOp::eAdd
		};
	}
	/*----------------------------------------------------------------------------*/
	inline RenderPass::Blending RenderPass::Blending::GetAdditive() {
		return Blending() = {
			true,
			BlendFactor::eSrcAlpha,
			BlendFactor::eOneMinusSrcAlpha,
			BlendOp::eAdd,
			BlendFactor::eOne,
			BlendFactor::eOne,
			BlendOp::eAdd
		};
	}
	/*----------------------------------------------------------------------------*/
	inline RenderPass::Blending RenderPass::Blending::GetAdditiveAlpha() {
		return Blending() = {
			true,
			BlendFactor::eSrcAlpha,
			BlendFactor::eOneMinusSrcAlpha,
			BlendOp::eAdd,
			BlendFactor::eOneMinusSrcAlpha,
			BlendFactor::eZero,
			BlendOp::eAdd,
		};
	}

	////////////////////////////////////////////////////////////////////////////////
	// RenderPass::CreateInfo inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline RenderPass::CreateInfo RenderPass::CreateInfo::GetDefault() {
		return CreateInfo() = {
			eDrawSync,
			new Camera(),
			nullptr,
			Mesh::eTriangleList,
			Mesh::GetVertexInput2D(),
			sizeof(Mesh::Vertex2D),
			ivec2(1, 1),
			Color(0, 0, 0, 255),
			false,
			1
		};
	}

	////////////////////////////////////////////////////////////////////////////////
	// RenderPass inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline RenderPass:: RenderPass(Renderer* renderer, RenderPass* parent)
		: m_renderer(renderer)
		, m_parent(parent) {
	}
	/*----------------------------------------------------------------------------*/
	inline RenderPass::~RenderPass() {
	}

	/*----------------------------------------------------------------------------*/
	inline Renderer* RenderPass::GetRenderer() const {
		return m_renderer;
	}
	/*----------------------------------------------------------------------------*/
	inline Camera* RenderPass::GetCamera() const {
		return m_createInfo.camera;
	}
	/*----------------------------------------------------------------------------*/
	inline RenderPass::Type RenderPass::GetType() const {
		return m_createInfo.type;
	}

} // orb namespace