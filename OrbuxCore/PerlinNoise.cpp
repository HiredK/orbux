#include "PerlinNoise.h"
#include "Renderer.h"
#include <vector>

void orb::PerlinNoise::Seed(uint32_t seed)
{
	int* P = new int[m_size + m_size];
	std::srand(seed);

	{
		uint32_t i, j, k;
		for (i = 0; i < m_size; i++) {
			P[i] = i;
		}

		while (--i != 0) {
			k = P[i];
			j = (uint32_t)glm::linearRand(0.0f, (float)m_size);
			P[i] = P[j];
			P[j] = k;
		}

		for (i = 0; i < m_size; ++i) {
			P[m_size + i] = P[i];
		}
	}

	{
		std::vector<float> pixels;
		pixels.resize(m_size * m_size * 4);

		for (uint32_t i = 0; i < m_size; ++i)
		for (uint32_t j = 0; j < m_size; ++j)
		{
			int A = P[i] + j;
			int AA = P[A];
			int AB = P[A + 1];

			int B = P[i + 1] + j;
			int BA = P[B];
			int BB = P[B + 1];

			float* pixel = &pixels[(i + j * m_size) * 4];
			*pixel++ = AA / 255.0f;
			*pixel++ = AB / 255.0f;
			*pixel++ = BA / 255.0f;
			*pixel++ = BB / 255.0f;
		}

		auto ci = Texture::CreateInfo::GetDefault();
		ci.format = Format::eRGBA32F;

		m_permutationTable = m_renderer->CreateTexture(
			m_size,
			m_size,
			1, 
			pixels.data(), 
			ci
		);
	}

	{
		const float GRADIENT3[] = {
			1, 1, 0,-1, 1, 0, 1,-1, 0,-1,-1, 0,
			1, 0, 1,-1, 0, 1, 1, 0,-1,-1, 0,-1,
			0, 1, 1, 0,-1, 1, 0, 1,-1, 0,-1,-1,
			1, 1, 0, 0,-1, 1,-1, 1, 0, 0,-1,-1,
		};

		std::vector<float> pixels;
		pixels.resize(m_size * 4);

		for (uint32_t i = 0; i < m_size; ++i) {
			uint32_t idx = P[i] % 16;

			float* pixel = &pixels[i * 4];
			*pixel++ = GRADIENT3[idx * 3 + 0];
			*pixel++ = GRADIENT3[idx * 3 + 1];
			*pixel++ = GRADIENT3[idx * 3 + 2];
			*pixel++ = 1.0f;
		}

		auto ci = Texture::CreateInfo::GetDefault();
		ci.format = Format::eRGBA32F;

		m_gradientVectors = m_renderer->CreateTexture(
			m_size,
			1,
			1,
			pixels.data(),
			ci
		);
	}
}