#include "ScriptObject.h"
#include "Script.h"
#include "Binding_Core.h"
#include <sstream>
#include <iomanip>

std::map<std::thread::id, std::vector<orb::Script*>> orb::ScriptObject::OwnerStack;
orb::ScriptObject::Reference::Map orb::ScriptObject::References;
std::mutex orb::ScriptObject::ReferenceMutex;

orb::ScriptObject::ScriptObject(const std::string& name, Type type)
	: m_owner(ScriptObject::OwnerStack[std::this_thread::get_id()].back())
	, m_id(ScriptObject::GenerateID())
	, m_name(name)
	, m_type(type)
	, m_updated(false)
{
	std::ostringstream os;
	os << name << "_" << std::setfill('0') << std::setw(4) << m_id;
	m_name = os.str();

	std::thread::id tid = std::this_thread::get_id();
	References[tid].references[type].push_back(this);

	if (type == ScriptObject::eDynamic) {
		std::unique_lock<std::mutex> lock(ReferenceMutex);
		References[tid].changed = true;
	}
}

orb::ScriptObject::~ScriptObject()
{
	std::thread::id tid = std::this_thread::get_id();
	std::vector<ScriptObject*>::const_iterator it = References[tid].references[m_type].begin();
	while (it != References[tid].references[m_type].end())
	{
		if ((*it) == this)
		{
			References[tid].references[m_type].erase(it);
			if (m_type == ScriptObject::eDynamic) {
				std::unique_lock<std::mutex> lock(ReferenceMutex);
				References[tid].changed = true;
			}

			break;
		}

		++it;
	}
}

void orb::ScriptObject::PushOwner(Script* owner)
{
	std::thread::id tid = std::this_thread::get_id();
	if (OwnerStack[tid].empty() ||
		OwnerStack[tid].back()->GetPath() != owner->GetPath())
	{
		std::unique_lock<std::mutex> lock(ReferenceMutex);
		OwnerStack[tid].push_back(owner);
	}
}

void orb::ScriptObject::PopStack()
{
	std::thread::id tid = std::this_thread::get_id();
	if (OwnerStack[tid].size() > 1)
	{
		std::unique_lock<std::mutex> lock(ReferenceMutex);
		OwnerStack[tid].pop_back();
	}
}

void orb::ScriptObject::UpdateAll(float dt)
{
	std::thread::id tid = std::this_thread::get_id();
	std::vector<ScriptObject*>::const_iterator it = References[tid].references[eDynamic].begin();
	while (it != References[tid].references[eDynamic].end())
	{
		ScriptObject* ref = (*it);
		if (!ref->m_updated)
		{
			ref->Update(dt);
			ref->onUpdate.Emit(dt);
			ref->m_updated = true;

			{
				std::unique_lock<std::mutex> lock(ReferenceMutex);
				if (References[tid].changed) {
					it = References[tid].references[eDynamic].begin();
					ref->m_owner->CollectGarbage();
					References[tid].changed = false;
					continue;
				}
			}
		}

		it++;
	}

	it = References[tid].references[eDynamic].begin();
	while (it != References[tid].references[eDynamic].end()) {
		(*it)->m_updated = false;
		++it;
	}
}