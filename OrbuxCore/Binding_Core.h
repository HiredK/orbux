/**
* @file Binding_Core.h
* @brief
*/

#pragma once

#include <luabind/luabind.hpp>
#include <luabind/operator.hpp>
#include <luabind/out_value_policy.hpp>
#include <lua.hpp>

#include "Signal.h"
#include "ScriptObject.h"
#include "Engine.h"

namespace orb
{
	////////////////////////////////////////////////////////////////////////////////
	// Binding_Core_Signal:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	template <typename... Args>
	int orb::Signal<Args...>::Connect(const luabind::adl::object& f)
	{
		return Connect([=](Args... args) {
			luabind::call_function<void>(f, args...);
		});
	}
	/*----------------------------------------------------------------------------*/
	template <typename... Args>
	int Connect_wrapper(const Signal<Args...>& self, const luabind::adl::object& f)
	{
		return self.Connect([=](Args... args) {
			luabind::call_function<void>(f, args...);
		});
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Core_Signal()
	{
		return (
			luabind::class_<ISignal>("Signal")
			.def("connect", &ISignal::Connect)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_Core_ScriptObject:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct ScriptObject_wrapper : ScriptObject, luabind::wrap_base
	{
		ScriptObject_wrapper(const std::string& name, Type type)
			: ScriptObject(name, type) {
		}

		ScriptObject_wrapper(const std::string& name)
			: ScriptObject_wrapper(name, Type::eStatic) {
		}

		virtual void Update(float dt)
		{
			try {
				ScriptObject::PushOwner(m_owner);
				luabind::wrap_base::call<void>("__update", dt);
				ScriptObject::PopStack();
			}
			catch (...) {
			}
		}
		static void Update_default(ScriptObject* ptr, float dt) {
			ptr->ScriptObject::Update(dt);
		}
	};

	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Core_ScriptObject()
	{
		return (
			luabind::class_<ScriptObject, luabind::no_bases, std::shared_ptr<ScriptObject>, ScriptObject_wrapper>("ScriptObject")
			.scope
			[
				luabind::class_<Signal<float>, ISignal>("OnUpdateSignal")
			]
			.enum_("Type")
			[
				luabind::value("Static", ScriptObject::eStatic),
				luabind::value("Dynamic", ScriptObject::eDynamic)
			]

			.def(luabind::constructor<const std::string&, ScriptObject::Type>())
			.def(luabind::constructor<const std::string&>())
			.def(luabind::self == luabind::other<ScriptObject&>())

			.def_readonly("on_update", &ScriptObject::onUpdate)
			.def("__update", &ScriptObject::Update, &ScriptObject_wrapper::Update_default)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_Engine:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Engine()
	{
		return (
			luabind::class_<Engine>("Engine")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_Core:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Core()
	{
		return (
			luabind::namespace_("orb")
			[
				Binding_Core_Signal(),
				Binding_Core_ScriptObject(),
				Binding_Engine()
			]
		);
	}

} // orb namespace