/**
* @file Script.h
* @brief
*/

#pragma once

#include "FileFactory.h"
#include "FileHandle.h"

// Forward declarations:
struct lua_State;

namespace orb
{
	class Script : public FileHandle
	{
		public:
			REGISTER_FILE(Script);

			//! CTOR/DTOR:
			Script(Engine* engine_instance, const std::string& path);
			virtual ~Script();

			//! SERVICES:
			void CollectGarbage();

		protected:
			//! VIRTUALS:
			bool CreateData() override;
			void DeleteData() override;

		private:
			//! MEMBERS:
			lua_State* m_state;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Script inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Script:: Script(Engine* engine_instance, const std::string& path)
		: FileHandle(engine_instance, path)
		, m_state(nullptr) {
	}
	/*----------------------------------------------------------------------------*/
	inline Script::~Script() {
	}

} // orb namespace