/**
* @file Binding_GUI.h
* @brief
*/

#pragma once

#include "Binding_Core.h"
#include "MathDef.h"
#include "Canvas.h"
#include "Texture.h"
#include <imgui.h>

namespace orb
{
	////////////////////////////////////////////////////////////////////////////////
	// Binding_GUI_Canvas:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_GUI_Canvas()
	{
		return (
			luabind::class_<Canvas>("Canvas")
			.scope
			[
				luabind::class_<Signal<float>, ISignal>("OnFrameSignal")
			]
			.property("font_texture", &Canvas::GetFontTexture)
			.property("fps", &Canvas::GetFramerate)

			.def("add_font", &Canvas::AddFont)
			.def("draw", &Canvas::Draw)

			.def_readonly("on_frame", &Canvas::onFrame)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_GUI:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct GUI
	{
		static bool Button(const std::string& label)
		{
			return ImGui::Button(label.c_str());
		}

		static bool ColorEdit4(const std::string& label, Color* value)
		{
			float col[4] = { 
				value->r * (1.0f/255.0f),
				value->g * (1.0f/255.0f),
				value->b * (1.0f/255.0f), 
				value->a * (1.0f/255.0f) 
			};

			bool res = ImGui::ColorEdit4(label.c_str(), col, ImGuiColorEditFlags_Float);
			value->r = unsigned char(col[0] * 255.0f);
			value->g = unsigned char(col[1] * 255.0f);
			value->b = unsigned char(col[2] * 255.0f);
			value->a = unsigned char(col[3] * 255.0f);
			return res;
		}

		static Color ColorEdit3(const std::string& label, const luabind::object& value)
		{
			std::vector<float> raw;
			raw.resize(3);

			raw[0] = luabind::object_cast<float>(value[0]);
			raw[1] = luabind::object_cast<float>(value[1]);
			raw[2] = luabind::object_cast<float>(value[2]);
			ImGui::ColorEdit3(label.c_str(), raw.data(), ImGuiColorEditFlags_Float);

			Color color;
			color.r = unsigned char(raw[0] * 255.0f);
			color.g = unsigned char(raw[1] * 255.0f);
			color.b = unsigned char(raw[2] * 255.0f);
			return color;
		}

		static void SameLine()
		{
			ImGui::SameLine();
		}

		static void Text(const std::string& label)
		{
			ImGui::Text(label.c_str());
		}

		static void Begin(const std::string& label, bool* value, const vec2& size, float bgAlphaOverride, int windowFlags) {
			ImGui::Begin(label.c_str(), value, ImVec2(size.x, size.y), bgAlphaOverride, windowFlags);
		}
		static void Begin(const std::string& label, bool* value, int windowFlags) {
			ImGui::Begin(label.c_str(), value, windowFlags);
		}
		static void Begin(const std::string& label, bool* value) {
			ImGui::Begin(label.c_str(), value);
		}
		static void Begin(const std::string& label) {
			ImGui::Begin(label.c_str());
		}

		static void SliderFloat(const std::string& label, float* value, float min, float max)  {
			ImGui::SliderFloat(label.c_str(), value, min, max);
		}

		static void SetNextWindowPos(const vec2& pos) {
			ImGui::SetNextWindowPos(ImVec2(pos.x, pos.y));
		}

		static void SetNextWindowSize(const vec2& size) {
			ImGui::SetNextWindowSize(ImVec2(size.x, size.y));
		}

		static void PlotHistogram(const std::string& label, const luabind::object& table, const vec2& size, float min, float max)
		{
			std::vector<float> values;
			for (luabind::iterator i(table), end; i != end; ++i) {
				values.push_back(luabind::object_cast<float>(*i));
			}

			ImGui::PlotHistogram(
				label.c_str(), 
				values.data(),
				static_cast<int>(values.size()), 
				0, 
				"",
				min, 
				max, 
				ImVec2(size.x, size.y)
			);
		}

		static void Checkbox(const std::string& label, bool* value) {
			ImGui::Checkbox(label.c_str(), value);
		}
	};

	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_GUI()
	{
		return (
			luabind::namespace_("orb")
			[
				Binding_GUI_Canvas(),

				luabind::class_<GUI>("GUI")
				.enum_("WindowFlags")
				[
					luabind::value("NoTitleBar"                , ImGuiWindowFlags_NoTitleBar),
					luabind::value("NoResize"                  , ImGuiWindowFlags_NoResize),
					luabind::value("NoMove"                    , ImGuiWindowFlags_NoMove),
					luabind::value("NoScrollbar"               , ImGuiWindowFlags_NoScrollbar),
					luabind::value("NoScrollWithMouse"         , ImGuiWindowFlags_NoScrollWithMouse),
					luabind::value("NoCollapse"                , ImGuiWindowFlags_NoCollapse),
					luabind::value("AlwaysAutoResize"          , ImGuiWindowFlags_AlwaysAutoResize),
					luabind::value("NoSavedSettings"           , ImGuiWindowFlags_NoSavedSettings),
					luabind::value("NoInputs"                  , ImGuiWindowFlags_NoInputs),
					luabind::value("MenuBar"                   , ImGuiWindowFlags_MenuBar),
					luabind::value("HorizontalScrollbar"       , ImGuiWindowFlags_HorizontalScrollbar),
					luabind::value("NoFocusOnAppearing"        , ImGuiWindowFlags_NoFocusOnAppearing),
					luabind::value("NoBringToFrontOnFocus"     , ImGuiWindowFlags_NoBringToFrontOnFocus),
					luabind::value("AlwaysVerticalScrollbar"   , ImGuiWindowFlags_AlwaysVerticalScrollbar),
					luabind::value("AlwaysHorizontalScrollbar" , ImGuiWindowFlags_AlwaysHorizontalScrollbar),
					luabind::value("AlwaysUseWindowPadding"    , ImGuiWindowFlags_AlwaysUseWindowPadding),
					luabind::value("ResizeFromAnySide"         , ImGuiWindowFlags_ResizeFromAnySide),
					luabind::value("NoNavInputs"               , ImGuiWindowFlags_NoNavInputs),
					luabind::value("NoNavFocus"                , ImGuiWindowFlags_NoNavFocus),
					luabind::value("NoNav"                     , ImGuiWindowFlags_NoNav),
					luabind::value("NoFocusOnAppearing"        , ImGuiWindowFlags_NoFocusOnAppearing),
					luabind::value("NoBringToFrontOnFocus"     , ImGuiWindowFlags_NoBringToFrontOnFocus),
					luabind::value("AlwaysVerticalScrollbar"   , ImGuiWindowFlags_AlwaysVerticalScrollbar)
				]
				.scope
				[
					luabind::def("button", &GUI::Button),
					luabind::def("color_edit4", &GUI::ColorEdit4),
					luabind::def("color_edit3", &GUI::ColorEdit3),
					luabind::def("same_line", &GUI::SameLine),
					luabind::def("text", &GUI::Text),
					luabind::def("begin", (void(*)(const std::string&, bool*, const vec2&, float, int)) &GUI::Begin, luabind::out_value<2>()),
					luabind::def("begin", (void(*)(const std::string&, bool*, int)) &GUI::Begin, luabind::out_value<2>()),
					luabind::def("begin", (void(*)(const std::string&, bool*)) &GUI::Begin, luabind::out_value<2>()),
					luabind::def("begin", (void(*)(const std::string&)) &GUI::Begin),
					luabind::def("stop", &ImGui::End),
					luabind::def("slider_float", (void(*)(const std::string&, float*, float, float)) &GUI::SliderFloat, luabind::out_value<2>()),
					luabind::def("set_next_window_pos", &GUI::SetNextWindowPos),
					luabind::def("set_next_window_size", &GUI::SetNextWindowSize),
					luabind::def("plot_histogram", &GUI::PlotHistogram),
					luabind::def("checkbox", (void(*)(const std::string&, bool*)) &GUI::Checkbox, luabind::out_value<2>()),
					luabind::def("seperator", &ImGui::Separator)
				]
			]
		);
	}

} // orb namespace