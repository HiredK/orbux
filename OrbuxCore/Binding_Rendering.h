/**
* @file Binding_Rendering.h
* @brief
*/

#pragma once

#include "Binding_Core.h"
#include "Camera.h"
#include "Color.h"
#include "Mesh.h"
#include "PerlinNoise.h"
#include "Terrain.h"
#include "TileProducer.h"
#include "Window.h"
#include "Logger.h"

#include "SDL/SDL.h"

namespace orb
{
	////////////////////////////////////////////////////////////////////////////////
	// Binding_Rendering_Buffer:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<Buffer> Binding_Rendering_Buffer_FromRaw(
		Renderer* renderer,
		const luabind::object& data,
		uint32_t size)
	{
		Buffer::CreateInfo ci = Buffer::CreateInfo::GetDefault();
		ci.usage = Buffer::eUniform;
		ci.memProps = Buffer::eHostVisible;

		std::vector<char> raw;
		raw.resize(size);

		for (uint32_t i = 0; i < raw.size(); ++i) {
			raw[i] = luabind::object_cast<char>(data[i]);
		}

		return renderer->CreateBuffer(
			static_cast<uint32_t>(raw.size()) * sizeof(char), 
			reinterpret_cast<void*>(raw.data()),
			ci
		);
	}
	/*----------------------------------------------------------------------------*/
	inline void Binding_Rendering_Buffer_Write(
		Buffer* self,
		const luabind::object& data,
		uint32_t size)
	{
		std::vector<char> raw;
		raw.resize(size);

		for (uint32_t i = 0; i < raw.size(); ++i) {
			raw[i] = luabind::object_cast<char>(data[i]);
		}

		void* mapped = self->Map(size, 0);
		memcpy(mapped, raw.data(), raw.size() * sizeof(char));
		self->Unmap();
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Rendering_Buffer()
	{
		return (
			luabind::class_<Buffer>("Buffer")
			.scope
			[
				luabind::def("from_raw", Binding_Rendering_Buffer_FromRaw)
			]
			.def("write", &Binding_Rendering_Buffer_Write)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_Rendering_Camera:
	////////////////////////////////////////////////////////////////////////////////
	struct CameraWrapper : ScriptObject_wrapper, Camera
	{
		CameraWrapper(float left, float right, float bottom, float top, float near, float far)
			: ScriptObject_wrapper("camera", ScriptObject::eDynamic)
			, Camera(left, right, bottom, top, near, far) {
		}
		CameraWrapper(float fov, float aspect, float near, float far)
			: ScriptObject_wrapper("camera", ScriptObject::eDynamic)
			, Camera(fov, aspect, near, far) {
		}
		CameraWrapper(const Camera& other)
			: ScriptObject_wrapper("camera", ScriptObject::eDynamic)
			, Camera(other) {
		}
	};

	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Rendering_Camera()
	{
		return (
			luabind::class_<CameraWrapper, ScriptObject, std::shared_ptr<CameraWrapper>>("Camera")
			.scope
			[
				luabind::class_<Camera::Matrices>("Matrices"),
				luabind::class_<Signal<const Camera::Matrices*>, ISignal>("OnRebuildSignal")
			]

			.def(luabind::constructor<float, float, float, float>())
			.def(luabind::constructor<float, float, float, float, float, float>())
			.def(luabind::constructor<const Camera&>())

			.property("fov", &Camera::GetFieldOfView, &Camera::SetFieldOfView)
			.property("aspect", &Camera::GetAspect, &Camera::SetAspect)
			.property("rotation", (dquat&(Camera::*)()) &Camera::GetRotation, &Camera::SetRotation)
			.property("position", (dvec3&(Camera::*)()) &Camera::GetPosition, &Camera::SetPosition)
			.property("rotation", (dquat&(Camera::*)()) &Camera::GetRotation, &Camera::SetRotation)
			.def("rebuild", &Camera::Rebuild)

			.def_readonly("on_rebuild", &Camera::onRebuild),

			luabind::class_<Camera, CameraWrapper>("__CameraWrapper")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_Rendering_Color:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Rendering_Color()
	{
		return (
			luabind::class_<Color>("Color")
			.def(luabind::constructor<unsigned char, unsigned char, unsigned char, unsigned char>())
			.def_readwrite("r", &Color::r)
			.def_readwrite("g", &Color::g)
			.def_readwrite("b", &Color::b)
			.def_readwrite("a", &Color::a)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_Rendering_Command:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Rendering_Command()
	{
		return (
			luabind::class_<Command>("Command")
			.def("test", &Command::Test)
			.def("viewport", &Command::Viewport)
			.def("scissor", &Command::Scissor)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_Rendering_RenderPass:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Rendering_RenderPass()
	{
		return (
			luabind::class_<RenderPass>("RenderPass")
			.enum_("Type")
			[
				luabind::value("DrawOnce", RenderPass::eDrawOnce),
				luabind::value("DrawOnceBackground", RenderPass::eDrawOnceBackground),
				luabind::value("DrawSync", RenderPass::eDrawSync),
				luabind::value("DrawSyncBackground", RenderPass::eDrawSyncBackground)
			]
			.scope
			[
				// Use of a wrapper call here because onRecord
				// is called from an external dll. 
				luabind::class_<Signal<Command*>, ISignal>("OnRecordSignal")
				.def("connect", &Connect_wrapper<Command*>)
			]

			.def_readonly("on_record", &RenderPass::onRecord)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_Rendering_Renderer:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline RenderPass* Binding_Rendering_Renderer_CreateRenderPass(
		Renderer* self, 
		const luabind::object& settings)
	{
		RenderPass::CreateInfo ci = RenderPass::CreateInfo::GetDefault();
		ci.resolution.x = self->GetWindow().GetWidth();
		ci.resolution.y = self->GetWindow().GetHeight();

		for (luabind::iterator i(settings), end; i != end; ++i) {
			auto k = luabind::object_cast<std::string>(i.key());
			if (k == "type") {
				ci.type = luabind::object_cast<RenderPass::Type>(*i);
			}
			if (k == "camera") {
				ci.camera = luabind::object_cast<Camera*>(*i);
			}
			if (k == "shader") {
				ci.shader = luabind::object_cast<Shader*>(*i);
			}
			if (k == "topology") {
				ci.topology = luabind::object_cast<Mesh::Topology>(*i);
			}
			if (k == "resolution") {
				ci.resolution = (ivec2)luabind::object_cast<dvec2>(*i);
			}
			if (k == "clear_color") {
				ci.clearColor = *luabind::object_cast<Color*>(*i);
			}
			if (k == "enable_depth") {
				ci.enableDepth = luabind::object_cast<bool>(*i);
			}
			if (k == "num_threads") {
				ci.numThreads = luabind::object_cast<uint32_t>(*i);
			}
			if (k == "attachments") {
				for (luabind::iterator j(*i), end; j != end; ++j) 
				{
					RenderPass::Attachment attachment;
					attachment.blending = RenderPass::Blending::GetDefault();
					attachment.input = nullptr;

					for (luabind::iterator l(*j), end; l != end; ++l)
					{
						k = luabind::object_cast<std::string>(l.key());
						if (k == "input") {
							attachment.input = luabind::object_cast<Texture*>(*l);
						}
						if (k == "use_additive_blending") {
							bool enable = luabind::object_cast<bool>(*l);
							if (enable) {
								attachment.blending = RenderPass::Blending::GetAdditive();
							}
						}
						if (k == "use_additive_alpha_blending") {
							bool enable = luabind::object_cast<bool>(*l);
							if (enable) {
								attachment.blending = RenderPass::Blending::GetAdditiveAlpha();
							}
						}
					}

					ci.attachments.push_back(attachment);
				}
			}
			if (k == "samplers") {
				for (luabind::iterator j(*i), end; j != end; ++j) {
					auto tex = luabind::object_cast<Texture*>(*j);
					ci.samplers.push_back(tex);
				}
			}
			if (k == "ubos") {
				for (luabind::iterator j(*i), end; j != end; ++j) {
					auto ubo = luabind::object_cast<Buffer*>(*j);
					ci.ubos.push_back(ubo);
				}
			}
			if (k == "use_vertex_2d") {
				bool enable = luabind::object_cast<bool>(*i);
				if (enable) {
					ci.vertexInput = Mesh::GetVertexInput2D();
					ci.vertexStride = sizeof(Mesh::Vertex2D);
				}
			}
			if (k == "use_vertex_3d") {
				bool enable = luabind::object_cast<bool>(*i);
				if (enable) {
					ci.vertexInput = Mesh::GetVertexInput3D();
					ci.vertexStride = sizeof(Mesh::Vertex3D);
				}
			}
		}

		auto renderPass = self->CreateRenderPass(nullptr, ci);
		if (ci.camera) {
			ci.camera->Rebuild();
		}

		return renderPass;
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Rendering_Renderer()
	{
		return (
			luabind::class_<Renderer>("Renderer")
			.enum_("Type")
			[
				luabind::value("Undefined", Renderer::eUndefined),
				luabind::value("Dx3D12", Renderer::eDx3D12),
				luabind::value("Vulkan", Renderer::eVulkan)
			]

			.property("type", &Renderer::GetType)
			.def("create_render_pass", &Binding_Rendering_Renderer_CreateRenderPass)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_Rendering_Mesh:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline void Binding_Rendering_Mesh_Draw(Mesh* self, Command* cmd) {
		self->Draw(cmd);
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Rendering_Mesh()
	{
		return (
			luabind::class_<Mesh>("Mesh")
			.enum_("Topology")
			[
				luabind::value("PointList", Mesh::ePointList),
				luabind::value("LineList", Mesh::eLineList),
				luabind::value("LineStrip", Mesh::eLineStrip),
				luabind::value("TriangleList", Mesh::eTriangleList),
				luabind::value("TriangleStrip", Mesh::eTriangleStrip),
				luabind::value("TriangleFan", Mesh::eTriangleFan),
				luabind::value("LineListWithAdjacency", Mesh::eLineListWithAdjacency),
				luabind::value("LineStripWithAdjacency", Mesh::eLineStripWithAdjacency),
				luabind::value("TriangleListWithAdjacency", Mesh::eTriangleListWithAdjacency),
				luabind::value("TriangleStripWithAdjacency", Mesh::eTriangleStripWithAdjacency),
				luabind::value("PatchList", Mesh::ePatchList)
			]

			.scope
			[
				luabind::def("build_cube", (std::shared_ptr<Mesh>(*)(Renderer*, float)) &Mesh::BuildCube),
				luabind::def("build_quad", (std::shared_ptr<Mesh>(*)(Renderer*)) &Mesh::BuildQuad)
			]

			.def("draw", &Binding_Rendering_Mesh_Draw)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_Rendering_Texture:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<Texture> Binding_Rendering_Texture_FromEmpty(
		Renderer* renderer,
		uint32_t w,
		uint32_t h,
		const luabind::object& settings)
	{
		Logger::ScopedTimer timer(LOG, "Texture::FromEmpty");

		Texture::CreateInfo ci = Texture::CreateInfo::GetDefault();
		ci.usage = Texture::eSampled | Texture::eColorAttachment;

		for (luabind::iterator i(settings), end; i != end; ++i) {
			auto k = luabind::object_cast<std::string>(i.key());
			if (k == "type") {
				ci.type = luabind::object_cast<Texture::Type>(*i);
			}
			if (k == "format") {
				ci.format = luabind::object_cast<Format>(*i);
			}
			if (k == "layer_count") {
				ci.layerCount = luabind::object_cast<uint32_t>(*i);
			}
			if (k == "clear_color") {
				ci.clearColor = *luabind::object_cast<Color*>(*i);
			}
		}

		auto texture = renderer->CreateTexture(w, h, 1, nullptr, ci);
		return texture;
	}
	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<Texture> Binding_Rendering_Texture_FromEmpty(
		Renderer* renderer,
		uint32_t w,
		uint32_t h)
	{
		Texture::CreateInfo ci = Texture::CreateInfo::GetDefault();
		ci.usage = Texture::eSampled | Texture::eColorAttachment;

		auto texture = renderer->CreateTexture(w, h, 1, nullptr, ci);
		return texture;
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Rendering_Texture()
	{
		return (
			luabind::class_<Texture>("Texture")

			.enum_("Type")
			[
				luabind::value("Texture1D", Texture::eTexture1D),
				luabind::value("Texture2D", Texture::eTexture2D),
				luabind::value("Texture3D", Texture::eTexture3D),
				luabind::value("TextureCube", Texture::eTextureCube),
				luabind::value("Texture1DArray", Texture::eTexture1DArray),
				luabind::value("Texture2DArray", Texture::eTexture2DArray),
				luabind::value("TextureCubeArray", Texture::eTextureCubeArray)
			]

			.enum_("Format")
			[
				luabind::value("Undefined", (int)Format::eUndefined),
				luabind::value("R32F", (int)Format::eR32F),
				luabind::value("RG32F", (int)Format::eRG32F),
				luabind::value("RGB32F", (int)Format::eRGB32F),
				luabind::value("RGBA32F", (int)Format::eRGBA32F),
				luabind::value("R8", (int)Format::eR8),
				luabind::value("RG8", (int)Format::eRG8),
				luabind::value("RGBA8", (int)Format::eRGBA8)
			]

			.scope
			[
				luabind::def("from_empty", (std::shared_ptr<Texture>(*)(Renderer*, uint32_t, uint32_t, const luabind::object& settings)) Binding_Rendering_Texture_FromEmpty),
				luabind::def("from_empty", (std::shared_ptr<Texture>(*)(Renderer*, uint32_t, uint32_t)) Binding_Rendering_Texture_FromEmpty)
			]

			.property("w", &Texture::GetWidth)
			.property("h", &Texture::GetHeight)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_Rendering_PerlinNoise:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Rendering_PerlinNoise() {
		return (
			luabind::class_<PerlinNoise>("PerlinNoise")
			.def(luabind::constructor<Renderer*, uint32_t>())

			.property("permutation_table", &PerlinNoise::GetPermutationTable)
			.property("gradient_vectors", &PerlinNoise::GetGradientVectors)
			.def("seed", &PerlinNoise::Seed)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_Rendering_Terrain:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<Terrain> Binding_Rendering_Terrain_Build(
		Renderer* renderer,
		double size)
	{
		Terrain::CreateInfo ci = Terrain::CreateInfo::GetDefault();
		return Terrain::Build(renderer, size, ci);
	}
	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<Terrain> Binding_Rendering_Terrain_Build(
		Renderer* renderer,
		double size,
		const luabind::object& settings)
	{
		Terrain::CreateInfo ci = Terrain::CreateInfo::GetDefault();
		for (luabind::iterator i(settings), end; i != end; ++i) {
			auto k = luabind::object_cast<std::string>(i.key());
			if (k == "spherical") {
				ci.spherical = luabind::object_cast<bool>(*i);
			}
		}

		return Terrain::Build(renderer, size, ci);
	}

	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<TileProducer> Binding_Rendering_Terrain_AddTileProducer(
		Terrain* self, uint32_t tileSize, uint32_t border)
	{
		return self->AddTileProducer(
			new TileProducer(self->GetRenderer(), tileSize, border)
		);
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Rendering_Terrain() {
		return (
			luabind::class_<Terrain>("Terrain")
			.scope
			[
				luabind::class_<Terrain::Coord>("Coord")
				.def_readonly("tx", &Terrain::Coord::tx)
				.def_readonly("ty", &Terrain::Coord::ty)
				.def_readonly("length", &Terrain::Coord::length),

				luabind::def("build", (std::shared_ptr<Terrain>(*)(Renderer*, double)) Binding_Rendering_Terrain_Build),
				luabind::def("build", (std::shared_ptr<Terrain>(*)(Renderer*, double, const luabind::object&)) Binding_Rendering_Terrain_Build)
			]

			.property("dynamic_ubo", &Terrain::GetDynamicUBO)
			.def("add_tile_producer", (std::shared_ptr<TileProducer>(*)(Terrain*, uint32_t, uint32_t)) &Binding_Rendering_Terrain_AddTileProducer)
			.def("add_tile_producer", (std::shared_ptr<TileProducer>(Terrain::*)(TileProducer*)) &Terrain::AddTileProducer)
			.def("draw", &Terrain::Draw)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_Rendering_TileProducer:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Rendering_TileProducer() {
		return (
			luabind::class_<TileProducer>("TileProducer")
			.scope
			[
				luabind::class_<TileProducer::TileData>("TileData")
				.def_readonly("level", &TileProducer::TileData::level)
				.def_readonly("coord", &TileProducer::TileData::coord)
				.def_readonly("layer", &TileProducer::TileData::layer)
				.def_readonly("parent_layer", &TileProducer::TileData::parentLayer)
				.def_readonly("local_to_world", &TileProducer::TileData::localToWorld),

				luabind::class_<Signal<TileProducer::TileData*>, ISignal>("OnProduceSignal")
			]

			.def_readonly("on_produce", &TileProducer::onProduce)
			.property("dynamic_ubo", &TileProducer::GetDynamicUBO)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_Rendering_Window:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct WindowWrapper : ScriptObject_wrapper, Window
	{
		WindowWrapper(int w, int h, const std::string& title, Renderer::Type type, uint32_t usage)
			: ScriptObject_wrapper("window", ScriptObject::eDynamic)
			, Window(ScriptObject::m_owner->GetEngineInstance())
		{
			Window::Open(w, h, title, type, usage);
			Window::onEvent.Connect([&](const SDL_Event* e) {
				if (e->type == SDL_QUIT) {
					m_owner->SetAllocated(false);
				}
			});
		}

		void Update(float dt) override
		{
			Window::PollEvent();
			ScriptObject_wrapper::Update(dt);
		}
	};

	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Rendering_Window()
	{
		return (
			luabind::class_<WindowWrapper, ScriptObject, std::shared_ptr<WindowWrapper>>("Window")

			.enum_("UsageFlags")
			[
				luabind::value("Fullscreen", Window::eFullscreen),
				luabind::value("Shown", Window::eShown),
				luabind::value("Hidden", Window::eHidden),
				luabind::value("Borderless", Window::eBorderless),
				luabind::value("Resizable", Window::eResizable),
				luabind::value("Minimized", Window::eMinimized),
				luabind::value("Maximized", Window::eMaximized),
				luabind::value("Grabbed", Window::eGrabbed),
				luabind::value("InputFocus", Window::eInputFocus),
				luabind::value("MouseFocus", Window::eMouseFocus),
				luabind::value("FullscreenDesktop", Window::eFullscreenDesktop),
				luabind::value("Foreign", Window::eForeign),
				luabind::value("HeighDPI", Window::eHeighDPI),
				luabind::value("MouseCapture", Window::eMouseCapture),
				luabind::value("AlwaysOnTop", Window::eAlwaysOnTop),
				luabind::value("SkipTaskbar", Window::eSkipTaskbar),
				luabind::value("Utility", Window::eUtility),
				luabind::value("Tooltip", Window::eTooltip),
				luabind::value("PopupMenu", Window::ePopupMenu)
			]

			.scope
			[
				luabind::class_<Signal<const SDL_Event*>, ISignal>("OnEventSignal"),
				luabind::class_<Signal<const SDL_Event*, const dvec2&>, ISignal>("OnMouseMotionSignal")
			]

			.def(luabind::constructor<int, int, const std::string&, Renderer::Type, uint32_t>())
			.property("renderer", &Window::GetRenderer)
			.property("canvas", &Window::GetCanvas)
			.property("w", &Window::GetWidth)
			.property("h", &Window::GetHeight)
			.def("is_key_pressed", &Window::IsKeyPressed)
			.def("is_mouse_button_pressed", &Window::IsMouseButtonPressed)
			.def("display", &Window::Display)
			.def("close", &Window::Close)

			.def_readonly("on_event", &Window::onEvent)
			.def_readonly("on_mouse_motion", &Window::onMouseMotion),

			luabind::class_<Window, WindowWrapper>("__WindowWrapper")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_Rendering:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Rendering()
	{
		return (
			luabind::namespace_("orb")
			[
				Binding_Rendering_Buffer(),
				Binding_Rendering_Camera(),
				Binding_Rendering_Color(),
				Binding_Rendering_Command(),
				Binding_Rendering_RenderPass(),
				Binding_Rendering_Renderer(),
				Binding_Rendering_Mesh(),
				Binding_Rendering_Texture(),
				Binding_Rendering_PerlinNoise(),
				Binding_Rendering_Terrain(),
				Binding_Rendering_TileProducer(),
				Binding_Rendering_Window()
			]
		);
	}

} // orb namespace