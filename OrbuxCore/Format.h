/**
* @file Format.h
* @brief
*/

#pragma once

namespace orb
{
	enum class Format
	{
		eUndefined,
		eR32F,
		eRG32F,
		eRGB32F,
		eRGBA32F,
		eR8,
		eRG8,
		eRGBA8
	};
}