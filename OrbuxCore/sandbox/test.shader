{
    "shaders" : [
    {
        "format" : "hlsl",
        "type" : "shared",
        "data" :
        "
            cbuffer CameraUBO : register(b0)
            {
                float4x4 proj;
                float4x4 view;
                float4x4 viewProj;
                float4x4 invProj;
                float4x4 invView;
                float2 viewport;
            };
        "
    },
    {
        "format" : "hlsl",
        "type" : "vs",
        "data" :
        "
            struct VS_INPUT
            {
                float3 pos : POSITION;
                float2 tex : TEXCOORD;
            };

            struct VS_OUTPUT
            {
                float4 pos : SV_POSITION;
                float2 tex : TEXCOORD;
            };
            
            VS_OUTPUT main(VS_INPUT input)
            {
                VS_OUTPUT output;
                output.pos = mul(float4(input.pos, 1.0f), viewProj);
                output.tex = input.tex;
                return output;
            }
        "
    },
    {
        "format" : "hlsl",
        "type" : "fs",
        "data" :
        "
            struct VS_OUTPUT
            {
                float4 pos : SV_POSITION;
                float2 tex : TEXCOORD;
            };
            
            struct GBuffer
            {
                float4 albedo : SV_Target0;
            };
            
            GBuffer main(VS_OUTPUT input)
            {
                GBuffer output;
                output.albedo = float4(1, 0, 0, 1);
                return output;
            }
        "
    }]
}