--[[
- @file client.lua
- @brief
]]

#include "camera_first_person.h"

class 'client' (orb.Window)

function client:__init(w, h, title, type, usage)
    orb.Window.__init(self, w, h, title, type, usage)
    
    self.camera_ = camera_first_person(math.radians(80.0), w/h, 0.1, 2000000.0)
    --self.camera_.position = vec3(0, 6360000.0 + 100, -5)  
    
    self.on_mouse_motion:connect(function(e, motion)    
        if self:is_mouse_button_pressed(orb.Mouse.Left) then
            self.camera_:rotate(motion)
        end
    end)
    
    self.on_update:connect(function(dt)
    
        local speed = 5.0
        if self:is_key_pressed(orb.Keyboard.LShift) then
            speed = speed * 50
        end
        
        if self:is_key_pressed(orb.Keyboard.W) then
            self.camera_:move_forward(speed)
        end
        if self:is_key_pressed(orb.Keyboard.A) then
            self.camera_:move_left(speed)
        end
        if self:is_key_pressed(orb.Keyboard.S) then
            self.camera_:move_backward(speed)
        end
        if self:is_key_pressed(orb.Keyboard.D) then
            self.camera_:move_right(speed)
        end
    end)
    
    local pass = self.renderer:create_render_pass({
        shader = FileSystem:search("test.shader", true),
        camera = self.camera_,
        topology = orb.Mesh.TriangleList,
        clear_color = orb.Color.Red,
        use_vertex_3d = true
    })
    
    pass.on_record:connect(function(cmd)
    end)
end

function client:__update(dt)
    self:display()
end

instance = client(1280, 720, "Engine", orb.Renderer.Dx3D12, orb.Window.Resizable)