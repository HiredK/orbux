{
    "shaders" : [
    {
        "format" : "glsl",
        "type" : "shared",
        "data" :
        "
            layout(std140, row_major, binding = 0) uniform CameraUBO
            {
                mat4 proj;
                mat4 view;
                mat4 viewProj;
                mat4 invProj;
                mat4 invView;
                vec2 viewport;
            };
            
            layout(std140, row_major, binding = 1) uniform TileUBO
            {
                vec4 tileWSD;
                vec4 offset;
                vec4 coarseLevelOSL;
                float noiseAmplitude;
                float noiseFrequency;
                float currentLayer;
                float parentLayer;
            };
                 
            layout(binding = 2) uniform sampler2D permutationTable;
            layout(binding = 3) uniform sampler2D gradientVectors;
            layout(binding = 4) uniform sampler2DArray tex0;
            
            float mdot(mat4 a, mat4 b) {
                return dot(a[0], b[0]) + dot(a[1], b[1]) + dot(a[2], b[2]) + dot(a[3], b[3]);
            }

            vec4 perm2d(vec2 uv) {
                return texture(permutationTable, uv);
            }

            float gradperm(float x, vec3 p) {
                vec3 g = texture(gradientVectors, vec2(x, 0)).rgb;
                return dot(g, p);
            }

            vec3 fade(vec3 t) {
                return t * t * t * (t * (t * 6 - 15) + 10);
            }
            
            float inoise(vec3 p)
            {
                vec3 P = mod(floor(p), 256.0);
                p -= floor(p);
                vec3 f = fade(p);
                
                P = P / 256.0;
                const float one = 1.0 / 256.0;            
                vec4 AA = perm2d(P.xy) + P.z;
                
                return mix( mix( mix( gradperm(AA.x, p ),
                                         gradperm(AA.z, p + vec3(-1, 0, 0) ), f.x),
                                   mix( gradperm(AA.y, p + vec3(0, -1, 0) ),
                                         gradperm(AA.w, p + vec3(-1, -1, 0) ), f.x), f.y),
                             mix( mix( gradperm(AA.x+one, p + vec3(0, 0, -1) ),
                                         gradperm(AA.z+one, p + vec3(-1, 0, -1) ), f.x),
                                   mix( gradperm(AA.y+one, p + vec3(0, -1, -1) ),
                                         gradperm(AA.w+one, p + vec3(-1, -1, -1) ), f.x), f.y), f.z);
            }
        "
    },
    {
        "format" : "glsl",
        "type" : "vs",
        "data" :
        "
            layout(location = 0) in vec2 pos;
            layout(location = 1) in vec2 tex;
            layout(location = 2) in vec4 col;
            
            layout(location = 0) out vec2 gs_TEXCOORD0;
            layout(location = 1) out vec2 gs_TEXCOORD1;

            void main()
            {
                gl_Position = vec4(pos, 0.0f, 1.0f);
                gs_TEXCOORD0 = tex;
                gs_TEXCOORD1 = tex * tileWSD.x;
            }
        "
    },
    {
        "format" : "glsl",
        "type" : "gs",
        "data" :
        "
            layout (triangles) in;
            layout (triangle_strip, max_vertices = 3) out;
            
            layout(location = 0) in vec2 gs_TEXCOORD0[];
            layout(location = 1) in vec2 gs_TEXCOORD1[];
            
            layout(location = 0) out vec2 fs_TEXCOORD0;
            layout(location = 1) out vec2 fs_TEXCOORD1;
            
            void main()
            {
                for (int i = 0; i < gl_in.length(); i++)
                {
                    gl_Position = gl_in[i].gl_Position;
                    gl_Layer = int(currentLayer);
                    
                    fs_TEXCOORD0 = gs_TEXCOORD0[i];
                    fs_TEXCOORD1 = gs_TEXCOORD1[i];
                    EmitVertex();
                }
                
                EndPrimitive();
            }
        "
    },
    {
        "format" : "glsl",
        "type" : "fs",
        "data" :
        "
            const mat4 slopexMatrix[4] = mat4[4](
                mat4(
                    0.0, 0.0, 0.0, 0.0,
                    1.0, 0.0,-1.0, 0.0,
                    0.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0
                ),
                mat4(
                    0.0, 0.0, 0.0, 0.0,
                    0.5, 0.5,-0.5,-0.5,
                    0.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0
                ),
                mat4(
                    0.0, 0.0, 0.0, 0.0,
                    0.5, 0.0,-0.5, 0.0,
                    0.5, 0.0,-0.5, 0.0,
                    0.0, 0.0, 0.0, 0.0
                ),
                mat4(
                    0.0, 0.0, 0.0, 0.0,
                    0.25, 0.25, -0.25, -0.25,
                    0.25, 0.25, -0.25, -0.25,
                    0.0, 0.0, 0.0, 0.0
                )
            );
            
            const mat4 slopeyMatrix[4] = mat4[4](
                mat4(
                    0.0, 1.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0,
                    0.0,-1.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0
                ),
                mat4(
                    0.0, 0.5, 0.5, 0.0,
                    0.0, 0.0, 0.0, 0.0,
                    0.0,-0.5,-0.5, 0.0,
                    0.0, 0.0, 0.0, 0.0
                ),
                mat4(
                    0.0, 0.5, 0.0, 0.0,
                    0.0, 0.5, 0.0, 0.0,
                    0.0,-0.5, 0.0, 0.0,
                    0.0,-0.5, 0.0, 0.0
                ),
                mat4(
                    0.0, 0.25, 0.25, 0.0,
                    0.0, 0.25, 0.25, 0.0,
                    0.0, -0.25, -0.25, 0.0,
                    0.0, -0.25, -0.25, 0.0
                )
            );
            
            const mat4 curvatureMatrix[4] = mat4[4](
                mat4(
                    0.0, -1.0, 0.0, 0.0,
                    -1.0, 4.0, -1.0, 0.0,
                    0.0, -1.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0
                ),
                mat4(
                    0.0, -0.5, -0.5, 0.0,
                    -0.5, 1.5, 1.5, -0.5,
                    0.0, -0.5, -0.5, 0.0,
                    0.0, 0.0, 0.0, 0.0
                ),
                mat4(
                    0.0, -0.5, 0.0, 0.0,
                    -0.5, 1.5, -0.5, 0.0,
                    -0.5, 1.5, -0.5, 0.0,
                    0.0, -0.5, 0.0, 0.0
                ),
                mat4(
                    0.0, -0.25, -0.25, 0.0,
                    -0.25, 0.5, 0.5, -0.25,
                    -0.25, 0.5, 0.5, -0.25,
                    0.0, -0.25, -0.25, 0.0
                )
            );
            
            const mat4 upsampleMatrix[4] = mat4[4](
                mat4(
                    0.0, 0.0, 0.0, 0.0,
                    0.0, 1.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0
                ),
                mat4(
                    0.0, 0.0, 0.0, 0.0,
                    -1.0/16.0, 9.0/16.0, 9.0/16.0, -1.0/16.0,
                    0.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0
                ),
                mat4(
                    0.0, -1.0/16.0, 0.0, 0.0,
                    0.0, 9.0/16.0, 0.0, 0.0,
                    0.0, 9.0/16.0, 0.0, 0.0,
                    0.0, -1.0/16.0, 0.0, 0.0
                ),
                mat4(
                    1.0/256.0, -9.0/256.0, -9.0/256.0, 1.0/256.0,
                    -9.0/256.0, 81.0/256.0, 81.0/256.0, -9.0/256.0,
                    -9.0/256.0, 81.0/256.0, 81.0/256.0, -9.0/256.0,
                    1.0/256.0, -9.0/256.0, -9.0/256.0, 1.0/256.0
                )
            );
            
            layout(location = 0) in vec2 gs_TEXCOORD0;
            layout(location = 1) in vec2 gs_TEXCOORD1;
        
            layout(location = 0) out vec4 SV_Target0;

            void main()
            {
                vec2 p_uv = floor(gs_TEXCOORD1) * 0.5;
                vec2 uv = (p_uv - fract(p_uv) + vec2(0.5, 0.5)) * coarseLevelOSL.z + coarseLevelOSL.xy;
                float zf = 0.0f;
                
                mat4 cz = mat4(
                    texture(tex0, vec3(uv + vec2(0.0, 0.0) * coarseLevelOSL.z, parentLayer)).x,
                    texture(tex0, vec3(uv + vec2(1.0, 0.0) * coarseLevelOSL.z, parentLayer)).x,
                    texture(tex0, vec3(uv + vec2(2.0, 0.0) * coarseLevelOSL.z, parentLayer)).x,
                    texture(tex0, vec3(uv + vec2(3.0, 0.0) * coarseLevelOSL.z, parentLayer)).x,
                    texture(tex0, vec3(uv + vec2(0.0, 1.0) * coarseLevelOSL.z, parentLayer)).x,
                    texture(tex0, vec3(uv + vec2(1.0, 1.0) * coarseLevelOSL.z, parentLayer)).x,
                    texture(tex0, vec3(uv + vec2(2.0, 1.0) * coarseLevelOSL.z, parentLayer)).x,
                    texture(tex0, vec3(uv + vec2(3.0, 1.0) * coarseLevelOSL.z, parentLayer)).x,
                    texture(tex0, vec3(uv + vec2(0.0, 2.0) * coarseLevelOSL.z, parentLayer)).x,
                    texture(tex0, vec3(uv + vec2(1.0, 2.0) * coarseLevelOSL.z, parentLayer)).x,
                    texture(tex0, vec3(uv + vec2(2.0, 2.0) * coarseLevelOSL.z, parentLayer)).x,
                    texture(tex0, vec3(uv + vec2(3.0, 2.0) * coarseLevelOSL.z, parentLayer)).x,
                    texture(tex0, vec3(uv + vec2(0.0, 3.0) * coarseLevelOSL.z, parentLayer)).x,
                    texture(tex0, vec3(uv + vec2(1.0, 3.0) * coarseLevelOSL.z, parentLayer)).x,
                    texture(tex0, vec3(uv + vec2(2.0, 3.0) * coarseLevelOSL.z, parentLayer)).x,
                    texture(tex0, vec3(uv + vec2(3.0, 3.0) * coarseLevelOSL.z, parentLayer)).x
                );
                
                int i = int(dot(fract(p_uv), vec2(2.0, 4.0)));
                vec3 n = vec3(mdot(cz, slopexMatrix[i]), mdot(cz, slopeyMatrix[i]), 2.0 * tileWSD.y);
                float slope = length(n.xy) / n.z;
                float curvature = mdot(cz, curvatureMatrix[i]) / tileWSD.y;
                float noiseAmp = max(clamp(4.0 * curvature, 0.0, 1.5), clamp(2.0 * slope - 0.5, 0.1, 4.0));
    
                float u = (0.5 + 2) / (tileWSD.x - 1 - 2 * 2);
                vec2 pos = gs_TEXCOORD0 * (1.0 + u * 2.0) - u;
                pos = pos * offset.z + offset.xy;
                
                vec3 P = vec3(pos, offset.w);
                mat3 ltw = mat3(1, 0, 0, 0, 0, 1, 0, 1, 0);
                vec3 p = normalize(ltw * P).xyz;
    
                float noise = inoise(p * noiseFrequency);
                
                if (noiseAmplitude < 0.0) {
                    zf -= noiseAmplitude * noise;
                }
                else {
                    zf += noiseAmp * noiseAmplitude * noise;
                }
                
                float zc = zf;
                if (coarseLevelOSL.x != -1.0) 
                {
                    zf = zf + mdot(cz, upsampleMatrix[i]);

                    vec2 ij = floor(gs_TEXCOORD1 - vec2(2, 2));
                    vec4 uvc = vec4(2 + 0.5, 2 + 0.5, 2 + 0.5, 2 + 0.5);
                    uvc += tileWSD.z * floor((ij / (2.0 * tileWSD.z)).xyxy + vec4(0.5, 0.0, 0.0, 0.5));
                    
                    float zc1 = texture(tex0, vec3(uvc.xy * coarseLevelOSL.z + coarseLevelOSL.xy, parentLayer)).x;
                    float zc3 = texture(tex0, vec3(uvc.zw * coarseLevelOSL.z + coarseLevelOSL.xy, parentLayer)).x;
                    
                    zc = (zc1 + zc3) * 0.5;
                }
                
                SV_Target0 = vec4(zf, zc, 0, 1);
            }
        "
    }]
}