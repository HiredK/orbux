{
    "shaders" : [
    {
        "format" : "glsl",
        "type" : "shared",
        "data" :
        "
            layout(std140, row_major, binding = 0) uniform CameraUBO
            {
                mat4 proj;
                mat4 view;
                mat4 viewProj;
                mat4 invProj;
                mat4 invView;
                vec2 viewport;
            };
            
            layout(std140, row_major, binding = 1) uniform DynamicUBO
            {
                mat4 localToWorld;
                mat4 screenQuadCorners;
                mat4 screenQuadVerticals;
                vec4 screenQuadCornerNorms;
                vec4 offset;
                vec4 camera;
                float splitDist;
                float radius;
            };
            
            layout(std140, row_major, binding = 2) uniform HeightProducerUBO
            {
                vec4 heightTileSize;
                vec4 heightTileCoords;
                float heightLevel;
                float heightLayer;
            };
            
            layout(binding = 3) uniform sampler2DArray tex0;
            
            vec4 getSphericalPos(vec2 pos, vec2 uv, out vec3 deformed)
            {
                float d1 = splitDist + 1.0f;
                float d2 = 2.0f * splitDist;
                
                vec2 tile_uv = heightTileCoords.xy + uv * heightTileSize.xy;
                vec2 zfc = texture(tex0, vec3(tile_uv, heightLayer)).rg;
                
                vec2 vert = abs(camera.xy - pos);
                float d = max(max(vert.x, vert.y), camera.z);
                float blend = clamp((d - d1) / (d2 - d1), 0.0, 1.0);
                
                float R = radius;
                mat4 C = screenQuadCorners;
                mat4 N = screenQuadVerticals;
                vec4 L = screenQuadCornerNorms;
                vec3 P = vec3(pos * offset.z + offset.xy, R);
                
                vec4 uvUV = vec4(pos, vec2(1.0, 1.0) - pos);
                vec4 alpha = uvUV.zxzx * uvUV.wwyy;
                vec4 alphaPrime = alpha * L / dot(alpha, L);
                
                float h = zfc.x * (1.0 - blend) + zfc.y * blend;
                float k = min(length(P) / dot(alpha, L) * 1.0000003, 1.0);
                float hPrime = (h + R * (1.0 - k)) / k;
                
                deformed = (R + h) * normalize(mat3(localToWorld) * P);
                return (C + hPrime * N) * alphaPrime;
            }

            vec4 getPlanarPos(vec2 pos, vec2 uv, out vec3 deformed)
            {
                vec3 P = vec3(pos * offset.z + offset.xy, 0.0);         
                vec4 uvUV = vec4(pos, vec2(1.0) - pos);
                vec4 alpha = uvUV.zxzx * uvUV.wwyy;
                float h = 0.000000;
                
                deformed = vec3(P.x, h, P.y);
                
                return ((screenQuadCorners + h * screenQuadVerticals) * alpha);
            }
        "
    },
    {
        "format" : "glsl",
        "type" : "vs",
        "data" :
        "
            layout(location = 0) in vec3 pos;
            layout(location = 1) in vec2 tex;  
            
            layout(location = 0) out vec2 tcs_POSITION0;
            layout(location = 1) out vec2 tcs_TEXCOORD0;
            layout(location = 2) out vec3 tcs_DEFORMED0;

            void main()
            {
                vec3 deformed;
                gl_Position = (radius > 0.0f) ? getSphericalPos(pos.xy, tex, deformed) : getPlanarPos(pos.xy, tex, deformed);
                tcs_POSITION0 = pos.xy;
                tcs_TEXCOORD0 = tex;
                tcs_DEFORMED0 = deformed;
            }
        "
    },
    {
        "format" : "glsl",
        "type" : "tcs",
        "data" :
        "
            layout(vertices = 4) out;     
            
            layout(location = 0) in vec2 tcs_POSITION0[];
            layout(location = 1) in vec2 tcs_TEXCOORD0[];
            layout(location = 2) in vec3 tcs_DEFORMED0[];   
            
            layout(location = 0) out vec2 tes_POSITION0[];
            layout(location = 1) out vec2 tes_TEXCOORD0[];

            float getLevel(float d) {
                return clamp(80000 / d, 1, 25);
            }

            void main()
            {
                if(gl_InvocationID == 0)
                {
                    vec3 wpos = vec3(invView[3]);
                    float d0 = distance(wpos, tcs_DEFORMED0[0].xyz);
                    float d1 = distance(wpos, tcs_DEFORMED0[1].xyz);
                    float d2 = distance(wpos, tcs_DEFORMED0[2].xyz);
                    float d3 = distance(wpos, tcs_DEFORMED0[3].xyz);
                    
                    gl_TessLevelOuter[0] = getLevel(mix(d3, d0, 0.5));
                    gl_TessLevelOuter[1] = getLevel(mix(d0, d1, 0.5));
                    gl_TessLevelOuter[2] = getLevel(mix(d1, d2, 0.5));
                    gl_TessLevelOuter[3] = getLevel(mix(d2, d3, 0.5));
                    
                    float l = max(max(gl_TessLevelOuter[0], gl_TessLevelOuter[1]), max(gl_TessLevelOuter[2], gl_TessLevelOuter[3]));
                    gl_TessLevelInner[0] = l;
                    gl_TessLevelInner[1] = l;
                }
                
                tes_POSITION0[gl_InvocationID] = tcs_POSITION0[gl_InvocationID];
                tes_TEXCOORD0[gl_InvocationID] = tcs_TEXCOORD0[gl_InvocationID];
            }
        "
    },
    {
        "format" : "glsl",
        "type" : "tes",
        "data" :
        "
            layout(quads, fractional_odd_spacing, ccw) in;      
            
            layout(location = 0) in vec2 tes_POSITION0[];
            layout(location = 1) in vec2 tes_TEXCOORD0[];   
            
            layout(location = 0) out vec2 fs_TEXCOORD0;
            layout(location = 1) out vec3 fs_DEFORMED0;
            
            vec2 interpolate(vec2 bl, vec2 br, vec2 tr, vec2 tl)
            {
                float u = gl_TessCoord.x;
                float v = gl_TessCoord.y;
                
                vec2 b = mix(bl, br, u);
                vec2 t = mix(tl, tr, u);
                return mix(b, t, v);
            }
            
            void main()
            {
                vec2 pos = interpolate(tes_POSITION0[0], tes_POSITION0[1], tes_POSITION0[2], tes_POSITION0[3]);
                vec2 tex = interpolate(tes_TEXCOORD0[0], tes_TEXCOORD0[1], tes_TEXCOORD0[2], tes_TEXCOORD0[3]);
                
                vec3 deformed;
                gl_Position = (radius > 0.0f) ? getSphericalPos(pos, tex, deformed) : getPlanarPos(pos, tex, deformed);             
                fs_TEXCOORD0 = tex;
                fs_DEFORMED0 = deformed;
            }
        "
    },
    {
        "format" : "glsl",
        "type" : "fs",
        "data" :
        "
            layout(location = 0) in vec2 fs_TEXCOORD0;
            layout(location = 1) in vec3 fs_DEFORMED0;   
            
            layout(location = 0) out vec4 SV_Target0;

            void main()
            {
                vec2 uv = heightTileCoords.xy + fs_TEXCOORD0 * heightTileSize.xy;
                vec2 zfc = texture(tex0, vec3(uv, heightLayer)).rg;
                float h = clamp(abs(zfc.x) * 0.0001, 0, 1);
                
                SV_Target0 = (zfc.x < 0.0) ? vec4(0, 0, h, 1) : vec4(h, 0, 0, 1);
            }
        "
    }]
}