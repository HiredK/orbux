--[[
- @file client.lua
- @brief
]]

#include "camera_first_person.h"

local ffi = require("ffi")
ffi.cdef
[[
    typedef struct
    {
        float tileWSD[4];
        float offset[4];
        float coarseLevelOSL[4];
        float noiseAmplitude;
        float noiseFrequency;
        float currentLayer;
        float parentLayer;
        
    } TileData;
]]

class 'client' (orb.Window)

function client:__init(w, h, title, type, usage)
    orb.Window.__init(self, w, h, title, type, usage)
    
    self.camera_ = camera_first_person(math.radians(80.0), w/h, 0.1, 2000000.0)
    self.camera_.position = vec3(0, 6360000.0 + 100, -5)  
    self.quad_ = orb.Mesh.build_quad(self.renderer)
    
    self.terrain_ = orb.Terrain.build(self.renderer, 6360000.0, { spherical = true })
    self.terrain_tasks_ = {}
    
    self.perlin_noise_ = orb.PerlinNoise(self.renderer, 256)
    self.perlin_noise_:seed(123234) 
    
    self.noise_amp_ = { -3250.0, -1590.0, -1125.0, -795.0, -561.0, -397.0, -140.0, -100.0, 15.0, 8.0, 5.0, 2.5, 1.5, 1.0 }    
	self.noise_amp_factor_ = 3.2
	self.noise_frequency_ = 40.0
    
    self.texture_ = orb.Texture.from_empty(self.renderer, 101, 101, {
        type = orb.Texture.Texture2DArray,
        format = orb.Texture.RG32F,
        layer_count = 512
    })
    
    self.terrain_hp_ = self.terrain_:add_tile_producer(101, 2)
    self.terrain_hp_.on_produce:connect(function(tile_data)
    
        local task = {}
        task.level = tile_data.level
        task.coord = tile_data.coord
        task.layer = tile_data.layer
        task.parent_layer = tile_data.parent_layer
        
        table.insert(self.terrain_tasks_, task)
    end)
    
    self.on_update:connect(function(dt)
    
        if table.getn(self.terrain_tasks_) > 0 then
        
            local num_threads = 1

            local ubo_data = ffi.new("TileData")
            local ubo = orb.Buffer.from_raw(self.renderer, ffi.cast("char*", ubo_data), ffi.sizeof("TileData"))  
            
            local pass2 = self.renderer:create_render_pass({
                shader = FileSystem:search("height.shader", true),
                type = orb.RenderPass.DrawOnceBackground,
                num_threads = num_threads,
                topology = orb.Mesh.TriangleStrip,
                resolution = vec2(101, 101),
                attachments = {
                    {
                        input = self.texture_
                    }
                },
                ubos = {
                    ubo
                },
                samplers = {
                    self.perlin_noise_.permutation_table,
                    self.perlin_noise_.gradient_vectors,
                    self.texture_
                }
            })
            
            pass2.on_record:connect(function(cmd)
            
                local task = self.terrain_tasks_[1]
                
                local tile_size = 101
                local level = task.level
                local s = tile_size - (1 + 2 * 2)
                local tx = task.coord.tx
                local ty = task.coord.ty
                local l = task.coord.length                  
                local rs = (level < 14) and self.noise_amp_[level + 1] or 0.0
                
                local dx = (tx % 2) * (s / 2)
                local dy = (ty % 2) * (s / 2)
                
                ubo_data.tileWSD[0] = tile_size
                ubo_data.tileWSD[1] = l / bit.lshift(1, level) / s
                ubo_data.tileWSD[2] = s / (25 - 1)
                ubo_data.tileWSD[3] = 0.0
                
                ubo_data.offset[0] = (tx / bit.lshift(1, level) - 0.5) * l
                ubo_data.offset[1] = (ty / bit.lshift(1, level) - 0.5) * l
                ubo_data.offset[2] = l / bit.lshift(1, level)
                ubo_data.offset[3] = l / 2.0
                
                if level > 0 then
                    ubo_data.coarseLevelOSL[0] = dx / tile_size
                    ubo_data.coarseLevelOSL[1] = dy / tile_size
                    ubo_data.coarseLevelOSL[2] = 1.0 / tile_size
                    ubo_data.coarseLevelOSL[3] = 0.0
                else
                    ubo_data.coarseLevelOSL[0] = -1.0
                    ubo_data.coarseLevelOSL[1] = -1.0
                    ubo_data.coarseLevelOSL[2] = -1.0
                    ubo_data.coarseLevelOSL[3] = -1.0
                end
                
                ubo_data.noiseAmplitude = rs * self.noise_amp_factor_
                ubo_data.noiseFrequency = self.noise_frequency_ * bit.lshift(1, level)               
                ubo_data.currentLayer = task.layer
                ubo_data.parentLayer = task.parent_layer
                
                ubo:write(ffi.cast("char*", ubo_data), ffi.sizeof("TileData"))
                self.quad_:draw(cmd)
                
                table.remove(self.terrain_tasks_, 1)
            end)
        end
    end)
    
    local pass = self.renderer:create_render_pass({
        shader = FileSystem:search("terrain.shader", true),
        camera = self.camera_,
        topology = orb.Mesh.PatchList,
        use_vertex_3d = true,
        enable_depth = true,
        ubos = {
            self.terrain_.dynamic_ubo,
            self.terrain_hp_.dynamic_ubo
        },
        samplers = {
            self.texture_
        }
    })
    
    pass.on_record:connect(function(cmd)
        self.terrain_:draw(cmd)
    end)
    
    self.on_mouse_motion:connect(function(e, motion)    
        if self:is_mouse_button_pressed(orb.Mouse.Left) then
            self.camera_:rotate(motion)
        end
    end)
    
    self.on_update:connect(function(dt)
    
        local speed = 5.0
        if self:is_key_pressed(orb.Keyboard.LShift) then
            speed = speed * 50
        end
        
        if self:is_key_pressed(orb.Keyboard.W) then
            self.camera_:move_forward(speed)
        end
        if self:is_key_pressed(orb.Keyboard.A) then
            self.camera_:move_left(speed)
        end
        if self:is_key_pressed(orb.Keyboard.S) then
            self.camera_:move_backward(speed)
        end
        if self:is_key_pressed(orb.Keyboard.D) then
            self.camera_:move_right(speed)
        end
    end)
end

function client:__update(dt)
    self:display()
end

instance = client(1280, 720, "Engine", orb.Renderer.Vulkan, orb.Window.Shown)