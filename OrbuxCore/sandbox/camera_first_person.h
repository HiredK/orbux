--[[
- @file camera_first_person.h
- @brief
]]

class 'camera_first_person' (orb.Camera)

function camera_first_person:__init(fov, aspect, near, far)
	orb.Camera.__init(self, fov, aspect, near, far)
    
    self.local_up_ = vec3(0, 1, 0) 
    self.local_rotation_ = quat(1, 0, 0, 0)
    self.local_ = mat4()
    self.euler_ = vec3()
    
    self.velocity_damp_ = 0.00001
	self.velocity_ = vec3()   
    self.rotation_damp_ = 0.000005
end

function camera_first_person:move(speed, i)

    local up = math.normalize(math.rotation(vec3(0, 1, 0), self.local_up_))
    local lookat = math.rotate(up, vec3(math.column(self.local_, i)))
    self.velocity_ = self.velocity_ - lookat * speed
end

function camera_first_person:move_forward(speed)
    self:move(-speed, 2)
end

function camera_first_person:move_backward(speed)
    self:move( speed, 2)
end

function camera_first_person:move_right(speed)
    self:move(-speed, 0)
end

function camera_first_person:move_left(speed)
    self:move( speed, 0)
end

function camera_first_person:rotate(motion)

	self.euler_.x = self.euler_.x + (motion.y * 0.000001)
    self.euler_.y = self.euler_.y + (motion.x * 0.000001)
end

function camera_first_person:__update(dt)

    local up = math.normalize(math.rotation(vec3(0, 1, 0), self.local_up_))
	local rx = math.angle_axis(math.degrees(self.euler_.x * dt), vec3(math.column(self.local_, 0)))
	local ry = math.angle_axis(math.degrees(self.euler_.y * dt), vec3(0, 1, 0))
	local rz = math.angle_axis(math.degrees(self.euler_.z * dt), vec3(math.column(self.local_, 2)))
    self.local_rotation_ = math.normalize(rz * ry * rx * self.local_rotation_) 
   
    self.position = self.position + (self.velocity_ * dt)
    self.rotation = up * self.local_rotation_
    
    self.velocity_ = self.velocity_ * math.pow(self.velocity_damp_, dt)
    self.euler_ = self.euler_ * math.pow(self.rotation_damp_, dt * 0.001)   
    self.local_ = math.translate(mat4(), self.position) * math.mat4_cast(self.local_rotation_)
    self:rebuild()
end