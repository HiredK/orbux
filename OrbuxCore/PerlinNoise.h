/**
* @file PerlinNoise.h
* @brief
*/

#pragma once

#include "Texture.h"
#include "MathDef.h"
#include <memory>

namespace orb
{
	// Forward declarations:
	class Renderer;

	class PerlinNoise
	{
		public:
			//! CTOR/DTOR:
			PerlinNoise(Renderer* renderer, uint32_t size = 256);
			virtual ~PerlinNoise();

			//! SERVICES:
			void Seed(uint32_t seed);

			//! ACCESSORS:
			Texture* GetPermutationTable() const;
			Texture* GetGradientVectors() const;
			uint32_t GetSize() const;

		private:
			//! MEMBERS:
			Renderer* m_renderer;
			std::shared_ptr<Texture> m_permutationTable;
			std::shared_ptr<Texture> m_gradientVectors;
			uint32_t m_size;
	};

	////////////////////////////////////////////////////////////////////////////////
	// PerlinNoise inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline PerlinNoise:: PerlinNoise(Renderer* renderer, uint32_t size)
		: m_renderer(renderer)
		, m_size(size) {
	}
	/*----------------------------------------------------------------------------*/
	inline PerlinNoise::~PerlinNoise() {
	}

	/*----------------------------------------------------------------------------*/
	inline Texture* PerlinNoise::GetPermutationTable() const {
		return m_permutationTable.get();
	}
	/*----------------------------------------------------------------------------*/
	inline Texture* PerlinNoise::GetGradientVectors() const {
		return m_gradientVectors.get();
	}
	/*----------------------------------------------------------------------------*/
	inline uint32_t PerlinNoise::GetSize() const {
		return m_size;
	}
}