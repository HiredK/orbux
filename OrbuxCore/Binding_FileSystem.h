/**
* @file Binding_FileSystem.h
* @brief
*/

#pragma once

#include "Binding_Core.h"
#include "FileSystem.h"
#include "Script.h"
#include "Shader.h"

namespace orb
{
	////////////////////////////////////////////////////////////////////////////////
	// Binding_FileSystem:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_FileSystem()
	{
		return (
			luabind::namespace_("orb")
			[
				luabind::class_<FileHandle>("FileHandle")
				.def(luabind::self == luabind::other<FileHandle&>())
				.property("path", &FileHandle::GetPath),

				luabind::class_<Script, FileHandle>("Script"),

				luabind::class_<Shader, FileHandle>("Shader"),

				luabind::class_<FileSystem>("FileSystem")
				.def("search", (FileHandle*(FileSystem::*)(const std::string&, bool)) &FileSystem::Search)
				.def("search", (FileHandle*(FileSystem::*)(const std::string&)) &FileSystem::Search)
			]
		);
	}

} // orb namespace