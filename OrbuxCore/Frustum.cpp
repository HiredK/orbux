#include "Frustum.h"

void orb::Frustum::Rebuild(const dmat4& proj, const dmat4& view)
{
	double clip[4][4];
	clip[0][0] = view[0][0] * proj[0][0] + view[0][1] * proj[1][0] + view[0][2] * proj[2][0] + view[0][3] * proj[3][0];
	clip[0][1] = view[0][0] * proj[0][1] + view[0][1] * proj[1][1] + view[0][2] * proj[2][1] + view[0][3] * proj[3][1];
	clip[0][2] = view[0][0] * proj[0][2] + view[0][1] * proj[1][2] + view[0][2] * proj[2][2] + view[0][3] * proj[3][2];
	clip[0][3] = view[0][0] * proj[0][3] + view[0][1] * proj[1][3] + view[0][2] * proj[2][3] + view[0][3] * proj[3][3];

	clip[1][0] = view[1][0] * proj[0][0] + view[1][1] * proj[1][0] + view[1][2] * proj[2][0] + view[1][3] * proj[3][0];
	clip[1][1] = view[1][0] * proj[0][1] + view[1][1] * proj[1][1] + view[1][2] * proj[2][1] + view[1][3] * proj[3][1];
	clip[1][2] = view[1][0] * proj[0][2] + view[1][1] * proj[1][2] + view[1][2] * proj[2][2] + view[1][3] * proj[3][2];
	clip[1][3] = view[1][0] * proj[0][3] + view[1][1] * proj[1][3] + view[1][2] * proj[2][3] + view[1][3] * proj[3][3];

	clip[2][0] = view[2][0] * proj[0][0] + view[2][1] * proj[1][0] + view[2][2] * proj[2][0] + view[2][3] * proj[3][0];
	clip[2][1] = view[2][0] * proj[0][1] + view[2][1] * proj[1][1] + view[2][2] * proj[2][1] + view[2][3] * proj[3][1];
	clip[2][2] = view[2][0] * proj[0][2] + view[2][1] * proj[1][2] + view[2][2] * proj[2][2] + view[2][3] * proj[3][2];
	clip[2][3] = view[2][0] * proj[0][3] + view[2][1] * proj[1][3] + view[2][2] * proj[2][3] + view[2][3] * proj[3][3];

	clip[3][0] = view[3][0] * proj[0][0] + view[3][1] * proj[1][0] + view[3][2] * proj[2][0] + view[3][3] * proj[3][0];
	clip[3][1] = view[3][0] * proj[0][1] + view[3][1] * proj[1][1] + view[3][2] * proj[2][1] + view[3][3] * proj[3][1];
	clip[3][2] = view[3][0] * proj[0][2] + view[3][1] * proj[1][2] + view[3][2] * proj[2][2] + view[3][3] * proj[3][2];
	clip[3][3] = view[3][0] * proj[0][3] + view[3][1] * proj[1][3] + view[3][2] * proj[2][3] + view[3][3] * proj[3][3];

	m_data[eRight][A] = clip[0][3] - clip[0][0];
	m_data[eRight][B] = clip[1][3] - clip[1][0];
	m_data[eRight][C] = clip[2][3] - clip[2][0];
	m_data[eRight][D] = clip[3][3] - clip[3][0];
	Normalize(eRight);

	m_data[eLeft][A] = clip[0][3] + clip[0][0];
	m_data[eLeft][B] = clip[1][3] + clip[1][0];
	m_data[eLeft][C] = clip[2][3] + clip[2][0];
	m_data[eLeft][D] = clip[3][3] + clip[3][0];
	Normalize(eLeft);

	m_data[eBottom][A] = clip[0][3] + clip[0][1];
	m_data[eBottom][B] = clip[1][3] + clip[1][1];
	m_data[eBottom][C] = clip[2][3] + clip[2][1];
	m_data[eBottom][D] = clip[3][3] + clip[3][1];
	Normalize(eBottom);

	m_data[eTop][A] = clip[0][3] - clip[0][1];
	m_data[eTop][B] = clip[1][3] - clip[1][1];
	m_data[eTop][C] = clip[2][3] - clip[2][1];
	m_data[eTop][D] = clip[3][3] - clip[3][1];
	Normalize(eTop);

	m_data[eFront][A] = clip[0][3] - clip[0][2];
	m_data[eFront][B] = clip[1][3] - clip[1][2];
	m_data[eFront][C] = clip[2][3] - clip[2][2];
	m_data[eFront][D] = clip[3][3] - clip[3][2];
	Normalize(eFront);

	m_data[eBack][A] = clip[0][3] + clip[0][2];
	m_data[eBack][B] = clip[1][3] + clip[1][2];
	m_data[eBack][C] = clip[2][3] + clip[2][2];
	m_data[eBack][D] = clip[3][3] + clip[3][2];
	Normalize(eBack);
}

void orb::Frustum::Normalize(Plane plane)
{
	double magnitude = glm::sqrt(
		m_data[plane][A] * m_data[plane][A] +
		m_data[plane][B] * m_data[plane][B] +
		m_data[plane][C] * m_data[plane][C]
	);

	m_data[plane][A] /= magnitude;
	m_data[plane][B] /= magnitude;
	m_data[plane][C] /= magnitude;
	m_data[plane][D] /= magnitude;
}

orb::Frustum::Visibility orb::Frustum::IsInside(const dvec3& point) const
{
	for (unsigned int i = 0; i < 6; i++) {
		if (m_data[i][A] * point.x +
			m_data[i][B] * point.y +
			m_data[i][C] * point.z +
			m_data[i][D] <= 0) {
			return eInvisible;
		}
	}

	return eCompletly;
}

orb::Frustum::Visibility orb::Frustum::IsInside(const Box3D& box) const
{
	auto GetVisibility = [](const dvec4& clip, const Box3D& box)
	{
		double x0 = box.GetMin().x * clip.x;
		double x1 = box.GetMax().x * clip.x;
		double y0 = box.GetMin().y * clip.y;
		double y1 = box.GetMax().y * clip.y;
		double z0 = box.GetMin().z * clip.z + clip.w;
		double z1 = box.GetMax().z * clip.z + clip.w;
		double p1 = x0 + y0 + z0;
		double p2 = x1 + y0 + z0;
		double p3 = x1 + y1 + z0;
		double p4 = x0 + y1 + z0;
		double p5 = x0 + y0 + z1;
		double p6 = x1 + y0 + z1;
		double p7 = x1 + y1 + z1;
		double p8 = x0 + y1 + z1;

		if (p1 <= 0 && p2 <= 0 && p3 <= 0 && p4 <= 0 && p5 <= 0 && p6 <= 0 && p7 <= 0 && p8 <= 0) {
			return eInvisible;
		}
		if (p1 > 0 && p2 > 0 && p3 > 0 && p4 > 0 && p5 > 0 && p6 > 0 && p7 > 0 && p8 > 0) {
			return eCompletly;
		}

		return ePartially;
	};

	Visibility v0 = GetVisibility(GetPlane(eRight), box);
	if (v0 == eInvisible) {
		return eInvisible;
	}

	Visibility v1 = GetVisibility(GetPlane(eLeft), box);
	if (v1 == eInvisible) {
		return eInvisible;
	}

	Visibility v2 = GetVisibility(GetPlane(eBottom), box);
	if (v2 == eInvisible) {
		return eInvisible;
	}

	Visibility v3 = GetVisibility(GetPlane(eTop), box);
	if (v3 == eInvisible) {
		return eInvisible;
	}

	Visibility v4 = GetVisibility(GetPlane(eFront), box);
	if (v4 == eInvisible) {
		return eInvisible;
	}

	if (v0 == eCompletly && v1 == eCompletly &&
		v2 == eCompletly && v3 == eCompletly &&
		v4 == eCompletly)
	{
		return eCompletly;
	}

	return ePartially;
}