#include "Terrain.h"
#include "Command.h"
#include "Renderer.h"
#include "RenderPass.h"
#include "TileProducer.h"
#include "Logger.h"

inline void* alignedAlloc(size_t size, size_t alignment)
{
	void *data = nullptr;
	data = _aligned_malloc(size, alignment);
	return data;
}

inline void alignedFree(void* data)
{
	_aligned_free(data);
}

std::shared_ptr<orb::Terrain> orb::Terrain::Build(
	Renderer* renderer,
	double size,
	const CreateInfo& ci)
{
	auto terrain = std::shared_ptr<Terrain>(new Terrain(renderer, size));
	terrain->Initialize(ci);
	return terrain;
}

std::shared_ptr<orb::TileProducer> orb::Terrain::AddTileProducer(
	TileProducer* tp)
{
	auto tileProducer = std::shared_ptr<TileProducer>(tp);
	tileProducer->Initialize();

	m_tileProducers.push_back(tileProducer);
	return tileProducer;
}

void orb::Terrain::Initialize(const CreateInfo& ci)
{
	m_root = std::unique_ptr<Tile>(new Tile());
	m_root->parent = nullptr;
	m_root->coord = { 0, 0, -m_size, -m_size, m_size * 2, -5000, 5000 };
	m_root->level = 0;
	m_root->visibility = Frustum::ePartially;
	m_root->occluded = false;

	m_mesh = Mesh::BuildPatch(m_renderer, ci.gridSize, ci.gridSize);
	m_maxLevel = ci.maxLevel;
	m_isSpherical = ci.spherical;

	if (ci.spherical) {
		struct SphericalDeformation : Deformation
		{
			SphericalDeformation(double radius)
				: R(radius) {
			}

			dvec3 LocalToDeformed(const dvec3& local) override {
				const dvec3 v = dvec3(local.x, local.y, R);
				return v * (local.z + R) / glm::length(v);
			}

			dmat4 LocalToDeformedDifferential(const dvec3& local, bool clamp) {
				dvec3 p = local;

				if (clamp) {
					p.x = p.x - glm::floor((p.x + R) / (2.0 * R)) * 2.0 * R;
					p.y = p.y - glm::floor((p.y + R) / (2.0 * R)) * 2.0 * R;
				}

				double l = p.x*p.x + p.y*p.y + R*R;
				double c0 = 1.0 / glm::sqrt(l);
				double c1 = c0 * R / l;

				return dmat4(
					(p.y*p.y + R*R) * c1, -p.x*p.y * c1       , p.x * c0, R*p.x * c0,
					-p.x*p.y * c1       , (p.x*p.x + R*R) * c1, p.y * c0, R*p.y * c0,
					-p.x*R * c1         , -p.y*R * c1         , R * c0  , (R*R) * c0,
					0.0                 , 0.0                 , 0.0     , 1.0
				);
			}

			dvec3 DeformedToLocal(const dvec3& deformed) {
				double l = glm::length(deformed);

				if (deformed.z >= glm::abs(deformed.x) && deformed.z >= glm::abs(deformed.y)) {
					return dvec3(deformed.x / deformed.z * R, deformed.y / deformed.z * R, l - R);
				}
				if (deformed.z <= -glm::abs(deformed.x) && deformed.z <= -glm::abs(deformed.y)) {
					return dvec3(std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity());
				}
				if (deformed.y >= glm::abs(deformed.x) && deformed.y >= glm::abs(deformed.z)) {
					return dvec3(deformed.x / deformed.y * R, (2.0 - deformed.z / deformed.y) * R, l - R);
				}
				if (deformed.y <= -glm::abs(deformed.x) && deformed.y <= -glm::abs(deformed.z)) {
					return dvec3(-deformed.x / deformed.y * R, (-2.0 - deformed.z / deformed.y) * R, l - R);
				}
				if (deformed.x >= glm::abs(deformed.y) && deformed.x >= glm::abs(deformed.z)) {
					return dvec3((2.0 - deformed.z / deformed.x) * R, deformed.y / deformed.x * R, l - R);
				}
				if (deformed.x <= -glm::abs(deformed.y) && deformed.x <= -glm::abs(deformed.z)) {
					return dvec3((-2.0 - deformed.z / deformed.x) * R, -deformed.y / deformed.x * R, l - R);
				}

				return dvec3(1);
			}

			Box2D DeformedToLocalBounds(const dvec3& center, double radius) {
				dvec3 p = DeformedToLocal(center);

				double k = (1.0 - R * R / (2.0 * R * R)) * glm::length(dvec3(p.x, p.y, R));
				double A = k * k - p.x * p.x;
				double B = k * k - p.y * p.y;
				double C = -2.0 * p.x * p.y;
				double D = -2.0 * R * R * p.x;
				double E = -2.0 * R * R * p.y;
				double F = R * R * (k * k - R * R);

				double a = C * C - 4.0 * A * B;
				double b = 2.0 * C * E - 4.0 * B * D;
				double c = E * E - 4.0 * B * F;
				double d = glm::sqrt(b * b - 4.0 * a * c);
				double x1 = (-b - d) / (2.0 * a);
				double x2 = (-b + d) / (2.0 * a);

				b = 2.0 * C * D - 4.0 * A * E;
				c = D * D - 4.0 * A * F;
				d = glm::sqrt(b * b - 4.0 * a * c);
				double y1 = (-b - d) / (2.0 * a);
				double y2 = (-b + d) / (2.0 * a);

				return Box2D(dvec2(x1, y1), dvec2(x2, y2));
			}

			dmat4 DeformedToTangentFrame(const dvec3& deformed) {
				dvec3 Uz = glm::normalize(deformed);
				dvec3 Ux = glm::normalize(glm::cross(dvec3(0, 1, 0), Uz));
				dvec3 Uy = glm::cross(Uz, Ux);

				return dmat4(
					Ux.x, Ux.y, Ux.z, 0,
					Uy.x, Uy.y, Uy.z, 0,
					Uz.x, Uz.y, Uz.z, -R,
					0, 0, 0, 1
				);
			}

			Frustum::Visibility GetVisibility(const dvec3& pos, const Frustum& frustum, const Box3D& box) {
				auto GetPlaneVisibility = [](const dvec4& clip, dvec3* b, double f) -> Frustum::Visibility {
					double o = b[0].x * clip.x + b[0].y * clip.y + b[0].z * clip.z;
					bool p = o + clip.w > 0.0;

					if ((o * f + clip.w > 0.0) == p) {
						o = b[1].x * clip.x + b[1].y * clip.y + b[1].z * clip.z;

						if ((o + clip.w > 0.0) == p && (o * f + clip.w > 0.0) == p) {
							o = b[2].x * clip.x + b[2].y * clip.y + b[2].z * clip.z;

							if ((o + clip.w > 0.0) == p && (o * f + clip.w > 0.0) == p) {
								o = b[3].x * clip.x + b[3].y * clip.y + b[3].z * clip.z;

								return 	(o + clip.w > 0.0) == p && (o * f + clip.w > 0.0) == p ?
									(p ? Frustum::eCompletly : Frustum::eInvisible) : Frustum::ePartially;
							}
						}
					}

					return Frustum::ePartially;
				};

				dvec3 deformedBox[4];
				deformedBox[0] = LocalToDeformed(dvec3(box.GetMin().x, box.GetMin().y, box.GetMin().z));
				deformedBox[1] = LocalToDeformed(dvec3(box.GetMax().x, box.GetMin().y, box.GetMin().z));
				deformedBox[2] = LocalToDeformed(dvec3(box.GetMax().x, box.GetMax().y, box.GetMin().z));
				deformedBox[3] = LocalToDeformed(dvec3(box.GetMin().x, box.GetMax().y, box.GetMin().z));

				double a = (box.GetMax().z + R) / (box.GetMin().z + R);
				double dx = (box.GetMax().x - box.GetMin().x) / 2 * a;
				double dy = (box.GetMax().y - box.GetMin().y) / 2 * a;
				double dz = box.GetMax().z + R;
				double f = glm::sqrt(dx * dx + dy * dy + dz * dz) / (box.GetMin().z + R);

				Frustum::Visibility v0 = GetPlaneVisibility(frustum.GetPlane(Frustum::eRight), deformedBox, f);
				if (v0 == Frustum::eInvisible) {
					return Frustum::eInvisible;
				}

				Frustum::Visibility v1 = GetPlaneVisibility(frustum.GetPlane(Frustum::eLeft), deformedBox, f);
				if (v1 == Frustum::eInvisible) {
					return Frustum::eInvisible;
				}

				Frustum::Visibility v2 = GetPlaneVisibility(frustum.GetPlane(Frustum::eBottom), deformedBox, f);
				if (v2 == Frustum::eInvisible) {
					return Frustum::eInvisible;
				}

				Frustum::Visibility v3 = GetPlaneVisibility(frustum.GetPlane(Frustum::eTop), deformedBox, f);
				if (v3 == Frustum::eInvisible) {
					return Frustum::eInvisible;
				}

				Frustum::Visibility v4 = GetPlaneVisibility(frustum.GetPlane(Frustum::eFront), deformedBox, f);
				if (v4 == Frustum::eInvisible) {
					return Frustum::eInvisible;
				}

				double lSq = glm::length2(pos);
				double rm = R + glm::min(0.0, box.GetMin().z);
				double rM = R + box.GetMax().z;
				double rmSq = rm * rm;
				double rMSq = rM * rM;
				dvec4 far = dvec4(pos, glm::sqrt((lSq - rmSq) * (rMSq - rmSq)) - rmSq);

				Frustum::Visibility v5 = GetPlaneVisibility(far, deformedBox, f);
				if (v5 == Frustum::eInvisible) {
					return Frustum::eInvisible;
				}

				if (v0 == Frustum::eCompletly && v1 == Frustum::eCompletly &&
					v2 == Frustum::eCompletly && v3 == Frustum::eCompletly &&
					v4 == Frustum::eCompletly && v5 == Frustum::eCompletly) {
					return Frustum::eCompletly;
				}

				return Frustum::ePartially;
			}

			DynamicUniformData GetUniformData(const dmat4& ltw, const dmat4& lts, int level, const Coord& coord) {
				DynamicUniformData data;

				dvec3 p0 = dvec3(coord.ox, coord.oy, R);
				dvec3 p1 = dvec3(coord.ox + coord.length, coord.oy, R);
				dvec3 p2 = dvec3(coord.ox, coord.oy + coord.length, R);
				dvec3 p3 = dvec3(coord.ox + coord.length, coord.oy + coord.length, R);
				dvec3 pc = (p0 + p3) * 0.5;

				dvec3 v0 = glm::normalize(p0);
				dvec3 v1 = glm::normalize(p1);
				dvec3 v2 = glm::normalize(p2);
				dvec3 v3 = glm::normalize(p3);

				data.localToWorld = glm::rowMajor4(ltw);

				data.screenQuadCorners = glm::rowMajor4(lts * glm::transpose(dmat4(
					v0.x * R, v1.x * R, v2.x * R, v3.x * R,
					v0.y * R, v1.y * R, v2.y * R, v3.y * R,
					v0.z * R, v1.z * R, v2.z * R, v3.z * R,
					1.0     , 1.0     , 1.0     , 1.0
				)));

				data.screenQuadVerticals = glm::rowMajor4(lts * glm::transpose(dmat4(
					v0.x, v1.x, v2.x, v3.x,
					v0.y, v1.y, v2.y, v3.y,
					v0.z, v1.z, v2.z, v3.z,
					0.0 , 0.0 , 0.0 , 0.0
				)));

				data.screenQuadCornerNorms = vec4(
					(float)glm::length(p0),
					(float)glm::length(p1),
					(float)glm::length(p2),
					(float)glm::length(p3)
				);

				data.offset = vec4(coord.ox, coord.oy, coord.length, level);
				data.radius = (float)R;
				return data;
			}

			double R;
		};
		m_deformation = std::unique_ptr<Deformation>(new SphericalDeformation(m_size));
	}
	else {
		struct PlanarDeformation : Deformation
		{
			dvec3 LocalToDeformed(const dvec3& local) override {
				return local;
			}

			dmat4 LocalToDeformedDifferential(const dvec3& local, bool clamp) {
				return glm::translate(dmat4(), dvec3(local.x, local.y, 0));
			}

			dvec3 DeformedToLocal(const dvec3& deformed) {
				return deformed;
			}

			Box2D DeformedToLocalBounds(const dvec3& center, double radius) {
				return Box2D(dvec2(center) - radius, dvec2(center) + radius);
			}

			dmat4 DeformedToTangentFrame(const dvec3& deformed) {
				return glm::translate(dmat4(), dvec3(-deformed.x, -deformed.y, 0));
			}

			Frustum::Visibility GetVisibility(const dvec3& pos, const Frustum& frustum, const Box3D& box) {
				return frustum.IsInside(box);
			}

			DynamicUniformData GetUniformData(const dmat4& ltw, const dmat4& lts, int level, const Coord& coord) {
				DynamicUniformData data;

				data.localToWorld = glm::rowMajor4(ltw);

				data.screenQuadCorners = glm::rowMajor4(lts * glm::transpose(dmat4(
					coord.ox, coord.ox + coord.length, coord.ox               , coord.ox + coord.length,
					coord.oy, coord.oy               , coord.oy + coord.length, coord.oy + coord.length,
					0.0     , 0.0                    , 0.0                    , 0.0                    ,
					1.0     , 1.0                    , 1.0                    , 1.0
				)));

				data.screenQuadVerticals = glm::rowMajor4(lts * glm::transpose(dmat4(
					0.0, 0.0, 0.0, 0.0,
					0.0, 0.0, 0.0, 0.0,
					1.0, 1.0, 1.0, 1.0,
					0.0, 0.0, 0.0, 0.0
				)));

				data.offset = vec4(coord.ox, coord.oy, coord.length, level);
				data.radius = 0.0f;
				return data;
			}
		};
		m_deformation = std::unique_ptr<Deformation>(new PlanarDeformation());
	}

	uint32_t bufferSize = MAX_TILE_INSTANCES * 256;
	m_dynamicData = (DynamicUniformData*)alignedAlloc(bufferSize, 256);
	m_dynamicUBO = m_renderer->CreateBuffer(
		bufferSize,
		m_dynamicData,
		Buffer::CreateInfo() = {
			Buffer::eUniform,
			Buffer::eHostVisible,
			true
		}
	);
}

void orb::Terrain::Draw(Command* cmd)
{
	unsigned int faceCount = (m_isSpherical) ? 6 : 1;
	unsigned int tileCount = 0;
	dmat4 clipMatrix = dmat4();
	clipMatrix[1].y = -1.0f;

	for (unsigned int i = 0; i < faceCount; i++)
	{
		Face face = (m_isSpherical) ? (Face)i : ePosY;
		dmat3 faceMatrix = dmat3(1.0);

		switch (face) {
			case ePosX: faceMatrix = dmat3( 0, 0, 1, 0, 1, 0, 1, 0, 0 ); break;
			case eNegX: faceMatrix = dmat3( 0, 0,-1, 0, 1, 0,-1, 0, 0 ); break;
			case ePosY: faceMatrix = dmat3( 1, 0, 0, 0, 0, 1, 0, 1, 0 ); break;
			case eNegY: faceMatrix = dmat3( 1, 0, 0, 0, 0,-1, 0,-1, 0 ); break;
			case ePosZ: faceMatrix = dmat3(-1, 0, 0, 0, 1, 0, 0, 0, 1 ); break;
			case eNegZ: faceMatrix = dmat3( 1, 0, 0, 0, 1, 0, 0, 0,-1 ); break;
			default: break;
		}

		const auto camera = cmd->GetParent()->GetCamera();
		dmat4 ltc = (clipMatrix * camera->GetViewMatrix()) * dmat4(faceMatrix);
		dmat4 lts = camera->GetProjMatrix() * ltc;
		dmat4 ltcInv = glm::inverse(ltc);

		dvec3 deformedCamPos = dvec3(ltcInv * dvec4(0, 0, 0, 1));
		dvec3 localCamPos = m_deformation->DeformedToLocal(deformedCamPos);
		m_localFrustum.Rebuild(camera->GetProjMatrix(), ltc);

		dmat4 M = m_deformation->LocalToDeformedDifferential(localCamPos, true);
		double x = glm::length(dvec3(M[0].x, M[1].x, M[2].x));
		double y = glm::length(dvec3(M[0].y, M[1].y, M[2].y));
		float distFactor = (float)glm::max(x, y);

		dvec3 lp = dvec3(m_localFrustum.GetPlane(Frustum::eLeft));
		dvec3 rp = dvec3(m_localFrustum.GetPlane(Frustum::eRight));
		float thfov = (float)glm::tan(glm::acos(glm::min(1.0, glm::max(-1.0, -glm::dot(lp, rp)))) * 0.5);
		float w = (float)1280.0; // hc

		float splitDist = distFactor * w / 1024.0f * glm::tan(glm::radians(40.0f)) / thfov;
		if (splitDist < 1.5f || !(std::isinf(splitDist) || std::isnan(splitDist))) {
			splitDist = 1.5f;
		}

		dmat2 localCamDir = dmat2(1);
		if (m_horizonCulling && localCamPos.z <= m_root->coord.zmax)
		{
			dvec3 deformedDir = dvec3(ltcInv * dvec4(0, 0, 1, 1));
			dvec3 localDir = dvec3(glm::normalize(dvec2(m_deformation->DeformedToLocal(deformedDir) - localCamPos)), 0);
			localCamDir = dmat2(localDir.y, -localDir.x, -localDir.x, -localDir.y);

			for (int i = 0; i < m_horizonSize; ++i) {
				m_horizon[i] = -std::numeric_limits<float>::infinity();
			}
		}

		auto AddOccluder = [&](const Box3D& box) -> bool {
			if (!m_horizonCulling || localCamPos.z > m_root->coord.zmax) {
				return false;
			}

			dvec2 corners[4];
			dvec2 o = dvec2(localCamPos);
			corners[0] = localCamDir * (dvec2(box.GetMin().x, box.GetMin().y) - o);
			corners[1] = localCamDir * (dvec2(box.GetMin().x, box.GetMax().y) - o);
			corners[2] = localCamDir * (dvec2(box.GetMax().x, box.GetMin().y) - o);
			corners[3] = localCamDir * (dvec2(box.GetMax().x, box.GetMax().y) - o);

			if (corners[0].y <= 0.0 ||
				corners[1].y <= 0.0 ||
				corners[2].y <= 0.0 ||
				corners[3].y <= 0.0) {
				return false;
			}

			double dzmin = box.GetMin().z - localCamPos.z;
			double dzmax = box.GetMax().z - localCamPos.z;

			dvec3 bounds[4];
			bounds[0] = dvec3(corners[0].x, dzmin, dzmax) / corners[0].y;
			bounds[1] = dvec3(corners[1].x, dzmin, dzmax) / corners[1].y;
			bounds[2] = dvec3(corners[2].x, dzmin, dzmax) / corners[2].y;
			bounds[3] = dvec3(corners[3].x, dzmin, dzmax) / corners[3].y;

			double xmin = glm::min(glm::min(bounds[0].x, bounds[1].x), glm::min(bounds[2].x, bounds[3].x)) * 0.33 + 0.5;
			double xmax = glm::max(glm::max(bounds[0].x, bounds[1].x), glm::max(bounds[2].x, bounds[3].x)) * 0.33 + 0.5;
			double zmin = glm::min(glm::min(bounds[0].y, bounds[1].y), glm::min(bounds[2].y, bounds[3].y));
			double zmax = glm::max(glm::max(bounds[0].z, bounds[1].z), glm::max(bounds[2].z, bounds[3].z));

			int imin = glm::max((int)glm::floor(xmin * m_horizonSize), 0);
			int imax = glm::min((int)glm::ceil(xmax * m_horizonSize), m_horizonSize - 1);

			bool occluded = (imax >= imin);
			for (int i = imin; i <= imax; ++i) {
				if (zmax > m_horizon[i]) {
					occluded = false;
					break;
				}
			}

			if (!occluded) {
				imin = glm::max((int)glm::ceil(xmin * m_horizonSize), 0);
				imax = glm::min((int)glm::floor(xmax * m_horizonSize), m_horizonSize - 1);
				for (int i = imin; i <= imax; ++i) {
					m_horizon[i] = (float)glm::max(m_horizon[i], (float)zmin);
				}
			}

			return occluded;
		};

		auto IsOccluded = [&](const Box3D& box) -> bool {
			if (!m_horizonCulling || localCamPos.z > m_root->coord.zmax) {
				return false;
			}

			dvec2 corners[4];
			dvec2 o = dvec2(localCamPos);
			corners[0] = localCamDir * (dvec2(box.GetMin().x, box.GetMin().y) - o);
			corners[1] = localCamDir * (dvec2(box.GetMin().x, box.GetMax().y) - o);
			corners[2] = localCamDir * (dvec2(box.GetMax().x, box.GetMin().y) - o);
			corners[3] = localCamDir * (dvec2(box.GetMax().x, box.GetMax().y) - o);

			if (corners[0].y <= 0.0 ||
				corners[1].y <= 0.0 ||
				corners[2].y <= 0.0 ||
				corners[3].y <= 0.0) {
				return false;
			}

			double dz = box.GetMax().z - localCamPos.z;
			corners[0] = dvec2(corners[0].x, dz) / corners[0].y;
			corners[1] = dvec2(corners[1].x, dz) / corners[1].y;
			corners[2] = dvec2(corners[2].x, dz) / corners[2].y;
			corners[3] = dvec2(corners[3].x, dz) / corners[3].y;

			double xmin = glm::min(glm::min(corners[0].x, corners[1].x), glm::min(corners[2].x, corners[3].x)) * 0.33 + 0.5;
			double xmax = glm::max(glm::max(corners[0].x, corners[1].x), glm::max(corners[2].x, corners[3].x)) * 0.33 + 0.5;
			double zmax = glm::max(glm::max(corners[0].y, corners[1].y), glm::max(corners[2].y, corners[3].y));

			int imin = glm::max((int)glm::floor(xmin * m_horizonSize), 0);
			int imax = glm::min((int)glm::ceil(xmax * m_horizonSize), m_horizonSize - 1);

			for (int i = imin; i <= imax; ++i) {
				if (zmax > m_horizon[i]) {
					return false;
				}
			}

			return (imax >= imin);
		};
	
		auto GetCameraDist = [&](const Box3D& box, const dvec3& point) -> double {
			return(
				glm::max(glm::abs(point.z - box.GetMax().z) / distFactor,
				glm::max(glm::min(glm::abs(point.x - box.GetMin().x),
				glm::abs(point.x - box.GetMax().x)),
				glm::min(glm::abs(point.y - box.GetMin().y),
				glm::abs(point.y - box.GetMax().y))))
			);
		};

		auto UpdateUniformData = [&](int level, const Coord& coord) {
			if (tileCount >= MAX_TILE_INSTANCES) {
				return;
			}

			DynamicUniformData* data = (DynamicUniformData*)(((uint64_t)m_dynamicData + (tileCount * 256)));
			memcpy(data, &m_deformation->GetUniformData(dmat4(faceMatrix), lts, level, coord), sizeof(DynamicUniformData));
			for (auto& tp : m_tileProducers) {
				tp->UpdateUniformData(dmat4(faceMatrix), level, coord, tileCount);
			}

			data->camera.x = static_cast<float>((localCamPos.x - coord.ox) / coord.length);
			data->camera.y = static_cast<float>((localCamPos.y - coord.oy) / coord.length);
			data->camera.z = static_cast<float>(localCamPos.z / (coord.length * distFactor));
			data->camera.w = static_cast<float>(localCamPos.z);
			data->splitDist = splitDist;

			tileCount++;
		};
		
		for (auto& tp : m_tileProducers) {
			tp->Synchronize(dmat4(faceMatrix), m_root.get());
		}

		std::function<void(Tile*)> RecursiveCull;
		RecursiveCull = [&](Tile* tile)
		{
			auto Subdivide = [&]()
			{
				float hl = (float)tile->coord.length / 2.0f;

				// bottom left quad
				tile->children[0] = std::unique_ptr<Tile>(new Tile());
				tile->children[0]->parent = tile;
				tile->children[0]->level = tile->level + 1;
				tile->children[0]->visibility = Frustum::ePartially;
				tile->children[0]->occluded = false;
				tile->children[0]->coord = {
					2 * tile->coord.tx,
					2 * tile->coord.ty,
					tile->coord.ox,
					tile->coord.oy,
					hl,
					tile->coord.zmin,
					tile->coord.zmax
				};

				// bottom right quad
				tile->children[1] = std::unique_ptr<Tile>(new Tile());
				tile->children[1]->parent = tile;
				tile->children[1]->level = tile->level + 1;
				tile->children[1]->visibility = Frustum::ePartially;
				tile->children[1]->occluded = false;
				tile->children[1]->coord = {
					2 * tile->coord.tx + 1,
					2 * tile->coord.ty,
					tile->coord.ox + hl,
					tile->coord.oy,
					hl, 
					tile->coord.zmin,
					tile->coord.zmax
				};

				// top left quad
				tile->children[2] = std::unique_ptr<Tile>(new Tile());
				tile->children[2]->parent = tile;
				tile->children[2]->level = tile->level + 1;
				tile->children[2]->visibility = Frustum::ePartially;
				tile->children[2]->occluded = false;
				tile->children[2]->coord = {
					2 * tile->coord.tx,
					2 * tile->coord.ty + 1,
					tile->coord.ox,
					tile->coord.oy + hl,
					hl,
					tile->coord.zmin,
					tile->coord.zmax
				};

				// top right quad
				tile->children[3] = std::unique_ptr<Tile>(new Tile());
				tile->children[3]->parent = tile;
				tile->children[3]->level = tile->level + 1;
				tile->children[3]->visibility = Frustum::ePartially;
				tile->children[3]->occluded = false;
				tile->children[3]->coord = {
					2 * tile->coord.tx + 1,
					2 * tile->coord.ty + 1,
					tile->coord.ox + hl,
					tile->coord.oy + hl,
					hl,
					tile->coord.zmin,
					tile->coord.zmax
				};
			};

			auto Release = [&]() {
				for (unsigned int i = 0; i < 4; ++i) {
					tile->children[i] = nullptr;
				}
			};

			const Box3D box = Box3D(
				dvec3(tile->coord.ox, tile->coord.oy, tile->coord.zmin), 
				dvec3(tile->coord.ox + tile->coord.length, tile->coord.oy + tile->coord.length, tile->coord.zmax)
			);

			const Box3D base = Box3D(
				dvec3(tile->coord.ox, tile->coord.oy, 0),
				dvec3(tile->coord.ox + tile->coord.length, tile->coord.oy + tile->coord.length, 0)
			);

			Frustum::Visibility v = (tile->parent == nullptr) ? Frustum::ePartially : tile->parent->visibility;
			if (v == Frustum::ePartially) {
				tile->visibility = m_deformation->GetVisibility(deformedCamPos, m_localFrustum, box);
			}
			else {
				tile->visibility = v;
			}

			if (tile->visibility != Frustum::eInvisible) {
				tile->occluded = IsOccluded(box);
				if (tile->occluded) {
					tile->visibility = Frustum::eInvisible;
				}
			}

			if (GetCameraDist(base, localCamPos) < (tile->coord.length * splitDist) &&
				tile->visibility != Frustum::eInvisible &&
				static_cast<unsigned int>(tile->level) < m_maxLevel) {

				if (tile->children[0] == nullptr) {
					Subdivide();
				}

				int order[4];
				double ox = localCamPos.x;
				double oy = localCamPos.y;
				double cx = tile->coord.ox + tile->coord.length * 0.5;
				double cy = tile->coord.oy + tile->coord.length * 0.5;

				if (oy < cy) {
					if (ox < cx) {
						order[0] = 0;
						order[1] = 1;
						order[2] = 2;
						order[3] = 3;
					}
					else {
						order[0] = 1;
						order[1] = 0;
						order[2] = 3;
						order[3] = 2;
					}
				}
				else {
					if (ox < cx) {
						order[0] = 2;
						order[1] = 0;
						order[2] = 3;
						order[3] = 1;
					}
					else {
						order[0] = 3;
						order[1] = 1;
						order[2] = 2;
						order[3] = 0;
					}
				}

				RecursiveCull(tile->children[order[0]].get());
				RecursiveCull(tile->children[order[1]].get());
				RecursiveCull(tile->children[order[2]].get());
				RecursiveCull(tile->children[order[3]].get());

				tile->occluded = (
					tile->children[0]->occluded &&
					tile->children[1]->occluded &&
					tile->children[2]->occluded &&
					tile->children[3]->occluded
				);
			}
			else {
				if (tile->visibility != Frustum::eInvisible) {
					tile->occluded = AddOccluder(box);
					if (tile->occluded) {
						tile->visibility = Frustum::eInvisible;
					}
				}

				if (tile->children[0] != nullptr) {
					Release();
				}
			}
		};

		std::function<void(Tile*)> RecursiveDraw;
		RecursiveDraw = [&](Tile* tile)
		{
			if (tile->visibility == Frustum::eInvisible) {
				return;
			}

			if (tile->children[0] == nullptr) {
				UpdateUniformData(tile->level, tile->coord);
			}
			else
			{
				int order[4];
				double ox = localCamPos.x;
				double oy = localCamPos.y;
				double cx = tile->coord.ox + tile->coord.length / 2.0;
				double cy = tile->coord.oy + tile->coord.length / 2.0;

				if (oy < cy) {
					if (ox < cx) {
						order[0] = 0;
						order[1] = 1;
						order[2] = 2;
						order[3] = 3;
					}
					else {
						order[0] = 1;
						order[1] = 0;
						order[2] = 3;
						order[3] = 2;
					}
				}
				else {
					if (ox < cx) {
						order[0] = 2;
						order[1] = 0;
						order[2] = 3;
						order[3] = 1;
					}
					else {
						order[0] = 3;
						order[1] = 1;
						order[2] = 2;
						order[3] = 0;
					}
				}

				int done = 0;
				for (int i = 0; i < 4; ++i)
				{
					if (tile->children[order[i]]->visibility == Frustum::eInvisible) {
						done |= (1 << order[i]);
					}
					else {
						RecursiveDraw(tile->children[order[i]].get());
						done |= (1 << order[i]);
					}
				}
			}
		};
	
		RecursiveCull(m_root.get());
		RecursiveDraw(m_root.get());
	}

	memcpy(m_dynamicUBO->GetMappedMemory(), m_dynamicData, tileCount * 256);
	m_dynamicUBO->Flush();

	for (auto& tp : m_tileProducers) {
		tp->Flush(tileCount);
	}

	for (unsigned int i = 0; i < tileCount; ++i) {
		cmd->Test(i * 256);
		m_mesh->Draw(cmd);
	}
}

void orb::Terrain::Cleanup()
{
}