#include "Script.h"
#include "Logger.h"

#pragma message ("Building Lua Context...")
#include "Binding.h"

REGISTER_FILE_IMPL(orb::Script, "lua", "luac")

bool orb::Script::CreateData()
{
	bool success = true;
	if (m_state == nullptr)
	{
		if (FileHandle::CreateData())
		{
			FileHandle::ScanFileForInclude(m_data);
			m_state = BuildLuaContext(m_engine_instance);

			const char* buffer = static_cast<const char*>(&m_data[0]);
			success = (luaL_loadbuffer(m_state, buffer, m_data.size(), m_path.c_str()) == LUA_OK);
			FileHandle::DeleteData();

			if (success) {
				ScriptObject::PushOwner(this);
				success = (lua_pcall(m_state, 0, 0, -2) == LUA_OK);
				ScriptObject::PopStack();
				Script::CollectGarbage();
			}
		}

		if (!success) {
			LOG(Logger::Error) << m_path << ": " << lua_tostring(m_state, -1);
			lua_pop(m_state, 2);
		}
	}

	return success;
}

void orb::Script::CollectGarbage()
{
	lua_gc(m_state, LUA_GCCOLLECT, 0);
}

void orb::Script::DeleteData()
{
	if (m_state != nullptr)
	{
		lua_close(m_state);
		m_state = nullptr;
	}
}