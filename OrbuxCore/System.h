/**
* @file System.h
* @brief
*/

#pragma once

#include <memory>
#include <vector>
#include <string>

namespace orb
{
	// Forward declarations:
	class Engine;

	class System
	{
		public:
			//! TYPEDEF/ENUMS:
			typedef std::vector<std::shared_ptr<System>> List;

			struct Version
			{
				// OPERATORS:
				friend bool operator==(const Version& a, const Version& b);
				friend bool operator!=(const Version& a, const Version& b);

				//! SERVICES:
				std::string Print() const;

				// MEMBERS:
				unsigned int major;
				unsigned int minor;
				unsigned int patch;
			};

			//! CTOR/DTOR:
			System(Engine* engine_instance);
			virtual ~System();

			//! VIRTUALS:
			virtual bool Init(int argc, char** argv) = 0;
			virtual void Cleanup() = 0;

		protected:
			//! MEMBERS:
			Engine* m_engine_instance;
			Version m_version;
	};

	////////////////////////////////////////////////////////////////////////////////
	// System::Version inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline bool operator==(const System::Version& a, const System::Version& b) {
		return (
			a.major == b.major &&
			a.minor == b.minor &&
			a.patch == b.patch
		);
	}
	/*----------------------------------------------------------------------------*/
	inline bool operator!=(const System::Version& a, const System::Version& b) {
		return (
			a.major != b.major ||
			a.minor != b.minor ||
			a.patch != b.patch
		);
	}
	/*----------------------------------------------------------------------------*/
	inline std::string System::Version::Print() const {
		return (
			std::to_string(major) + "." +
			std::to_string(minor) + "." +
			std::to_string(patch)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// System inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline System:: System(Engine* engine_instance)
		: m_engine_instance(engine_instance)
		, m_version({ 0, 0, 0 }) {
	}
	/*----------------------------------------------------------------------------*/
	inline System::~System() {
	}

} // orb namespace