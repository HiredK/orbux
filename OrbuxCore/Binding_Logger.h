/**
* @file Binding_Logger.h
* @brief
*/

#pragma once

#include "Binding_Core.h"
#include "Logger.h"

namespace orb
{
	////////////////////////////////////////////////////////////////////////////////
	// Binding_Logger:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Logger()
	{
		return (
			luabind::namespace_("orb")
			[
				luabind::class_<Logger>("Logger")
				.enum_("Level")
				[
					luabind::value("Info", Logger::Info),
					luabind::value("Warning", Logger::Warning),
					luabind::value("Error", Logger::Error),
					luabind::value("Timer", Logger::Timer)
				]
			]
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_Logger_Print:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	static int Binding_Logger_Print(lua_State* L)
	{
		int n = lua_gettop(L);
		int i;

		lua_getglobal(L, "tostring");
		std::stringstream ss;

		for (i = 1; i <= n; i++)
		{
			const char* s;
			lua_pushvalue(L, -1);
			lua_pushvalue(L, i);
			lua_call(L, 1, 1);

			s = lua_tostring(L, -1);
			if (s == NULL) {
				LOG(Logger::Error) << "lua_tostring failed!";
			}
			else {
				ss << ((i == 1) ? "" : " ") << s;
			}

			lua_pop(L, 1);
		}

		LOG() << ss.str();

		return 0;
	}

	/*----------------------------------------------------------------------------*/
	inline void Binding_Logger_OverridePrintLib(lua_State *L)
	{
		static const struct luaL_Reg lib[] = {
			{ "print", Binding_Logger_Print },
			{ NULL, NULL }
		};

		lua_getglobal(L, "_G");
		luaL_register(L, NULL, lib);
		lua_pop(L, 1);
	}

} // orb namespace