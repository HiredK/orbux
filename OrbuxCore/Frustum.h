/**
* @file Frustum.h
* @brief
*/

#pragma once

#include "Box2D.h"
#include "Box3D.h"

namespace orb
{
	class Frustum
	{
		public:
			//! TYPEDEF/ENUMS:
			enum Plane 
			{ 
				eRight, 
				eLeft, 
				eBottom, 
				eTop, 
				eFront, 
				eBack 
			};

			enum Visibility
			{ 
				eCompletly, 
				ePartially,
				eInvisible
			};

			//! CTOR/DTOR:
			Frustum();
			virtual ~Frustum();

			//! SERVICES:
			void Rebuild(const dmat4& proj, const dmat4& view);
			void Normalize(Plane plane);

			//! CULLING:
			Visibility IsInside(const dvec3& point) const;
			Visibility IsInside(const Box3D& box) const;

			//! ACCESSORS:
			dvec4 GetPlane(Plane plane) const;

		private:
			//! MEMBERS:
			enum { A, B, C, D };
			double m_data[6][4];
	};

	////////////////////////////////////////////////////////////////////////////////
	// Frustum inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Frustum:: Frustum() {
	}
	/*----------------------------------------------------------------------------*/
	inline Frustum::~Frustum() {
	}

	/*----------------------------------------------------------------------------*/
	inline dvec4 Frustum::GetPlane(Plane plane) const {
		return dvec4(
			m_data[plane][A],
			m_data[plane][B],
			m_data[plane][C], 
			m_data[plane][D]
		);
	}
}