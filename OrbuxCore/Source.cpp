#include "Engine.h"
#include "Logger.h"

#include "SDL/SDL.h"

orb::Logger& orb::LOG = orb::Logger("orbux.log");

int main(int argc, char *argv[])
{
	std::unique_ptr<orb::Engine> engine(new orb::Engine());

	std::vector<char*> embeddedCmd = { argv[0], "-mount", "sandbox", "-entry", "client.lua" };
	return engine->Execute(
		static_cast<int>(embeddedCmd.size()), 
		embeddedCmd.data()
	);

	//return engine->Execute(argc, argv);
}