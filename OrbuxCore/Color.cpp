#include "Color.h"
#include <algorithm>

const orb::Color orb::Color::Black(0, 0, 0);
const orb::Color orb::Color::White(255, 255, 255);
const orb::Color orb::Color::Red(255, 0, 0);
const orb::Color orb::Color::Green(0, 255, 0);
const orb::Color orb::Color::Blue(0, 0, 255);
const orb::Color orb::Color::Yellow(255, 255, 0);
const orb::Color orb::Color::Magenta(255, 0, 255);
const orb::Color orb::Color::Cyan(0, 255, 255);
const orb::Color orb::Color::Transparent(0, 0, 0, 0);

orb::Color orb::Color::operator+(const Color& other)
{
	unsigned char r = std::min(int(this->r) + other.r, 255);
	unsigned char g = std::min(int(this->g) + other.g, 255);
	unsigned char b = std::min(int(this->b) + other.b, 255);
	unsigned char a = std::min(int(this->a) + other.a, 255);
	return Color(r, g, b, a);
}

orb::Color orb::Color::operator-(const Color& other)
{
	unsigned char r = std::max(int(this->r) - other.r, 0);
	unsigned char g = std::max(int(this->g) - other.g, 0);
	unsigned char b = std::max(int(this->b) - other.b, 0);
	unsigned char a = std::max(int(this->a) - other.a, 0);
	return Color(r, g, b, a);
}

orb::Color orb::Color::operator*(const Color& other)
{
	unsigned char r = int(this->r) * other.r / 255;
	unsigned char g = int(this->g) * other.g / 255;
	unsigned char b = int(this->b) * other.b / 255;
	unsigned char a = int(this->a) * other.a / 255;
	return Color(r, g, b, a);
}