#include "Window.h"
#include "Dx3D12Renderer.h"
#include "VulkanRenderer.h"
#include "Engine.h"
#include "Logger.h"

#include "SDL/SDL.h"
#include "SDL/SDL_syswm.h"

void orb::Window::Open(
	int w, 
	int h, 
	const std::string& title,
	Renderer::Type type, 
	uint32_t usage)
{
	auto ConvertUsageFlags = [](uint32_t flags) -> uint32_t
	{
		uint32_t result = 0;
		if (flags & eFullscreen)        result |= SDL_WINDOW_FULLSCREEN;
		if (flags & eShown)             result |= SDL_WINDOW_SHOWN;
		if (flags & eHidden)            result |= SDL_WINDOW_HIDDEN;
		if (flags & eBorderless)        result |= SDL_WINDOW_BORDERLESS;
		if (flags & eResizable)         result |= SDL_WINDOW_RESIZABLE;
		if (flags & eMinimized)         result |= SDL_WINDOW_MINIMIZED;
		if (flags & eMaximized)         result |= SDL_WINDOW_MAXIMIZED;
		if (flags & eGrabbed)           result |= SDL_WINDOW_INPUT_GRABBED;
		if (flags & eInputFocus)        result |= SDL_WINDOW_INPUT_FOCUS;
		if (flags & eMouseFocus)        result |= SDL_WINDOW_MOUSE_FOCUS;
		if (flags & eFullscreenDesktop) result |= SDL_WINDOW_FULLSCREEN_DESKTOP;
		if (flags & eForeign)           result |= SDL_WINDOW_FOREIGN;
		if (flags & eHeighDPI)          result |= SDL_WINDOW_ALLOW_HIGHDPI;
		if (flags & eMouseCapture)      result |= SDL_WINDOW_MOUSE_CAPTURE;
		if (flags & eAlwaysOnTop)       result |= SDL_WINDOW_ALWAYS_ON_TOP;
		if (flags & eSkipTaskbar)       result |= SDL_WINDOW_SKIP_TASKBAR;
		if (flags & eUtility)           result |= SDL_WINDOW_UTILITY;
		if (flags & eTooltip)           result |= SDL_WINDOW_TOOLTIP;
		if (flags & ePopupMenu)         result |= SDL_WINDOW_POPUP_MENU;
		return result;
	};

	m_SDLWindow = SDL_CreateWindow(
		title.c_str(),
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED, 
		w,
		h, 
		ConvertUsageFlags(usage)
	);

	m_sysWMinfo = new SDL_SysWMinfo();
	SDL_VERSION(&m_sysWMinfo->version);
	SDL_GetWindowWMInfo(m_SDLWindow, m_sysWMinfo);
	m_usage = usage;

	std::string rendererName;
	switch (type)
	{
		default:
		case Renderer::eUndefined:
		case Renderer::eDx3D12: {
			m_renderer = std::unique_ptr<Renderer>(new Dx3D12Renderer(*this));
			rendererName = "Dx3D12";
			break;
		}

		case Renderer::eVulkan: {
			m_renderer = std::unique_ptr<Renderer>(new VulkanRenderer(*this));
			rendererName = "Vulkan";
			break;
		}
	}

	{
		Logger::ScopedTimer timer(LOG, "Renderer::Initialize");
		m_renderer->Initialize();
	}

	LOG() << rendererName << ": " << m_renderer->GetGPUInfo().Print();
	m_canvas = std::shared_ptr<Canvas>(new Canvas(*this));
	m_canvas->Initialize();
}

bool orb::Window::IsFocused() const
{
	return SDL_GetWindowFlags(m_SDLWindow) & SDL_WINDOW_MOUSE_FOCUS;
}

int orb::Window::GetWidth() const
{
	return SDL_GetWindowSurface(m_SDLWindow)->w;
}

int orb::Window::GetHeight() const
{
	return SDL_GetWindowSurface(m_SDLWindow)->h;
}

void orb::Window::PollEvent()
{
	auto e = std::make_unique<SDL_Event>();
	while (SDL_PollEvent(e.get()))
	{
		switch (e->type)
		{
			case SDL_WINDOWEVENT:
				switch (e->window.event)
				{
					case SDL_WINDOWEVENT_RESIZED:
					{
						Logger::ScopedTimer timer(LOG, "Renderer::Resize");
						m_renderer->Resize(
							static_cast<int>(e->window.data1),
							static_cast<int>(e->window.data2)
						);
						break;
					}
				}
				break;

			case SDL_KEYDOWN:
				m_keyPressed[e->key.keysym.sym] = true;
				break;

			case SDL_KEYUP:
				m_keyPressed[e->key.keysym.sym] = false;
				break;

			case SDL_MOUSEBUTTONDOWN:
				if (!m_canvas->IsMouseHover()) {
					m_mouseButtonPressed[e->button.button] = true;
				}

				break;

			case SDL_MOUSEBUTTONUP:
				m_mouseButtonPressed[e->button.button] = false;
				break;

			case SDL_MOUSEMOTION:
				onMouseMotion.Emit(e.get(), vec2(
					static_cast<float>(e->motion.xrel),
					static_cast<float>(e->motion.yrel)
				));
				break;

			default:
				break;
		}

		onEvent.Emit(e.get());
	}
}

void orb::Window::Display()
{
	m_canvas->NewFrame();
	m_renderer->Display();
}

void orb::Window::Close()
{
	m_renderer->Cleanup();
}