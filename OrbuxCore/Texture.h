/**
* @file Texture.h
* @brief
*/

#pragma once

#include "Buffer.h"
#include "Color.h"
#include "Format.h"
#include <memory>

namespace orb
{
	// Forward declarations:
	class Renderer;

	class RENDERER_API Texture
	{
		public:
			enum Type
			{
				eTexture1D,
				eTexture2D,
				eTexture3D,
				eTextureCube,
				eTexture1DArray,
				eTexture2DArray,
				eTextureCubeArray
			};

			enum UsageFlags
			{
				eTransferSrc            = 1 << 1,
				eTransferDst            = 1 << 2,
				eSampled                = 1 << 3,
				eStorage                = 1 << 4,
				eColorAttachment        = 1 << 5,
				eDepthStencilAttachment = 1 << 6,
				eTransientAttachment    = 1 << 7,
				eInputAttachment        = 1 << 8
			};

			struct CreateInfo
			{
				//! SERVICES:
				static CreateInfo GetDefault();

				//! MEMBERS:
				uint32_t usage;
				uint32_t memProps;
				Type type;
				Format format;
				uint32_t layerCount;
				Color clearColor;
			};

			//! CTOR/DTOR:
			Texture(
				Renderer* renderer,
				uint32_t w, 
				uint32_t h, 
				uint32_t depth
			);

			virtual ~Texture();

			//! VIRTUALS:
			virtual void Build(const void* data, const CreateInfo& ci) = 0;
			virtual void Cleanup() = 0;

			//! ACCESSORS:
			Renderer* GetRenderer() const;
			uint32_t GetWidth() const;
			uint32_t GetHeight() const;
			uint32_t GetDepth() const;
			Format GetFormat() const;
			uint32_t GetLayerCount() const;
			const Color& GetClearColor() const;

		protected:
			//! MEMBERS:
			Renderer* m_renderer;
			uint32_t m_w;
			uint32_t m_h;
			uint32_t m_depth;
			Format m_format;
			uint32_t m_layerCount;
			Color m_clearColor;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Texture::CreateInfo inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Texture::CreateInfo Texture::CreateInfo::GetDefault() {
		return CreateInfo() = {
			Texture::eSampled | Texture::eTransferDst,
			Buffer::eDeviceLocal,
			Texture::eTexture2D,
			Format::eRGBA8,
			1,
			Color(0, 0, 0)
		};
	}

	////////////////////////////////////////////////////////////////////////////////
	// Texture inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Texture:: Texture(
		Renderer* renderer, uint32_t w, uint32_t h, uint32_t depth)
		: m_renderer(renderer)
		, m_w(w)
		, m_h(h)
		, m_depth(depth)
		, m_format(Format::eUndefined)
		, m_layerCount(1)
		, m_clearColor(Color(0, 0, 0)) {
	}
	/*----------------------------------------------------------------------------*/
	inline Texture::~Texture() {
	}

	/*----------------------------------------------------------------------------*/
	inline Renderer* Texture::GetRenderer() const {
		return m_renderer;
	}
	/*----------------------------------------------------------------------------*/
	inline uint32_t Texture::GetWidth() const {
		return m_w;
	}
	/*----------------------------------------------------------------------------*/
	inline uint32_t Texture::GetHeight() const {
		return m_h;
	}
	/*----------------------------------------------------------------------------*/
	inline uint32_t Texture::GetDepth() const {
		return m_depth;
	}
	/*----------------------------------------------------------------------------*/
	inline Format Texture::GetFormat() const {
		return m_format;
	}
	/*----------------------------------------------------------------------------*/
	inline uint32_t Texture::GetLayerCount() const {
		return m_layerCount;
	}
	/*----------------------------------------------------------------------------*/
	inline const Color& Texture::GetClearColor() const {
		return m_clearColor;
	}
}