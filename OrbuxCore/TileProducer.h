/**
* @file TileProducer.h
* @brief
*/

#pragma once

#include "Terrain.h"
#include "Signal.h"

namespace orb
{
	class TileProducer
	{
		public:
			struct TileData
			{
				int level;
				Terrain::Coord coord;
				uint32_t layer;
				uint32_t parentLayer;
				dmat4 localToWorld;
			};

			//! CTOR/DTOR:
			TileProducer(Renderer* renderer, uint32_t tileSize, uint32_t border = 2);
			virtual ~TileProducer();

			//! SERVICES:
			void Initialize();
			void Synchronize(const dmat4& ltw, const Terrain::Tile* root);
			void UpdateUniformData(const dmat4& ltw, int level, const Terrain::Coord& coord, uint32_t index);
			TileData* GetTile(int level, int tx, int ty);
			void Flush(uint32_t tileCount = MAX_TILE_INSTANCES);

			//! ACCESSORS:
			Buffer* GetDynamicUBO() const;
			Renderer* GetRenderer() const;

			//! SIGNALS:
			Signal<TileData*> onProduce;

		protected:
			struct QuadTree 
			{
				QuadTree* children[4];
				QuadTree* parent;
				TileData* data;
				bool needTile;
			};

			struct DynamicUniformData
			{
				vec4 tileSize;
				vec4 tileCoords;
				float level;
				float layer;
			};

			//! MEMBERS:
			Renderer* m_renderer;
			std::map<std::string, TileData*> m_tileCache;
			uint32_t m_tileCount;
			std::shared_ptr<Buffer> m_dynamicUBO;
			DynamicUniformData* m_dynamicData;
			QuadTree m_root;
			uint32_t m_tileSize;
			uint32_t m_border;
	};

	////////////////////////////////////////////////////////////////////////////////
	// TileProducer inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline TileProducer:: TileProducer(
		Renderer* renderer, uint32_t tileSize, uint32_t border)
		: m_renderer(renderer)
		, m_tileCount(0)
		, m_tileSize(tileSize)
		, m_border(border) {
	}
	/*----------------------------------------------------------------------------*/
	inline TileProducer::~TileProducer() {
	}

	/*----------------------------------------------------------------------------*/
	inline Buffer* TileProducer::GetDynamicUBO() const {
		return m_dynamicUBO.get();
	}
	/*----------------------------------------------------------------------------*/
	inline Renderer* TileProducer::GetRenderer() const {
		return m_renderer;
	}
}