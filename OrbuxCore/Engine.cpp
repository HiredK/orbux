#include "Engine.h"
#include "InputParser.h"
#include "Logger.h"

#include "FileSystem.h"
#include "Script.h"
#include "ScriptObject.h"

#include "SDL/SDL.h"

bool orb::Engine::Init(int argc, char** argv)
{
	LOG() << "Init Orbux Engine v" << m_version.Print() << " (Using SDL v" << Engine::PrintSDL() << ")";

	m_systems.push_back(std::unique_ptr<FileSystem>(new FileSystem(this)));
	bool success = true;

	for (auto& system : m_systems) {
		success &= system->Init(argc, argv);
	}

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		LOG(Logger::Error) << "SDL_Init Error: " << SDL_GetError();
		success = false;
	}

	return success;
}

int orb::Engine::Execute(int argc, char** argv)
{
	if (Engine::Init(argc, argv))
	{
		auto input = std::unique_ptr<InputParser>(new InputParser(argc, argv));
		std::vector<std::shared_ptr<std::thread>> threads;

		for (auto& path : input->GetOption("-entry")) {
			if (orb::Script::CTOR.IsSupported(path))
			{
				Script* entry = dynamic_cast<Script*>(GetSystem<FileSystem>()->Search(path));
				auto thread = std::shared_ptr<std::thread>(new std::thread([this, &entry]()
				{
					entry->SetAllocated(true);
					entry->Process();

					Uint64 last = 0;
					Uint64 now = SDL_GetPerformanceCounter();
					float dt = 0.0f;

					while (entry->IsAllocated())
					{
						last = now;
						now = SDL_GetPerformanceCounter();
						dt = ((now - last) * 1000 / (float)SDL_GetPerformanceFrequency());
						ScriptObject::UpdateAll(dt);
					}

					entry->Process();
				}));

				threads.push_back(thread);
			}
		}

		for (auto& thread : threads) {
			thread->join();
		}

		Engine::Cleanup();

		LOG() << "EXIT_SUCCESS";
		return EXIT_SUCCESS;
	}

	LOG() << "EXIT_FAILURE";
	return EXIT_FAILURE;
}

std::string orb::Engine::PrintSDL() const
{
	return (
		std::to_string(SDL_MAJOR_VERSION) + "." +
		std::to_string(SDL_MINOR_VERSION) + "." +
		std::to_string(SDL_PATCHLEVEL)
	);
}

void orb::Engine::Cleanup()
{
	Logger::ScopedTimer timer(LOG, "Engine::Cleanup");
	for (auto& system : m_systems) {
		system->Cleanup();
	}
}