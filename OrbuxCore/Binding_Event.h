/**
* @file Binding_Event.h
* @brief
*/

#pragma once

#include "Binding_Core.h"
#include "SDL/SDL.h"

namespace orb
{
	////////////////////////////////////////////////////////////////////////////////
	// Binding_Event_Mouse:
	////////////////////////////////////////////////////////////////////////////////
	struct Mouse
	{
	};

	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Event_Mouse()
	{
		return (
			luabind::class_<Mouse>("Mouse")
			.enum_("Button")
			[
				luabind::value("Left", SDL_BUTTON_LEFT),
				luabind::value("Middle", SDL_BUTTON_MIDDLE),
				luabind::value("Right", SDL_BUTTON_RIGHT),
				luabind::value("X1", SDL_BUTTON_X1),
				luabind::value("X2", SDL_BUTTON_X2)
			]
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_Event_Keyboard:
	////////////////////////////////////////////////////////////////////////////////
	struct Keyboard
	{
	};

	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Event_Keyboard()
	{
		return (
			luabind::class_<Keyboard>("Keyboard")
			.enum_("Key")
			[
				luabind::value("A", SDLK_a),
				luabind::value("B", SDLK_b),
				luabind::value("C", SDLK_c),
				luabind::value("D", SDLK_d),
				luabind::value("E", SDLK_e),
				luabind::value("F", SDLK_f),
				luabind::value("G", SDLK_g),
				luabind::value("H", SDLK_h),
				luabind::value("I", SDLK_i),
				luabind::value("J", SDLK_j),
				luabind::value("K", SDLK_k),
				luabind::value("L", SDLK_l),
				luabind::value("M", SDLK_m),
				luabind::value("N", SDLK_n),
				luabind::value("O", SDLK_o),
				luabind::value("P", SDLK_p),
				luabind::value("Q", SDLK_q),
				luabind::value("R", SDLK_r),
				luabind::value("S", SDLK_s),
				luabind::value("T", SDLK_t),
				luabind::value("U", SDLK_u),
				luabind::value("V", SDLK_v),
				luabind::value("W", SDLK_w),
				luabind::value("X", SDLK_x),
				luabind::value("Y", SDLK_y),
				luabind::value("Z", SDLK_z),

				luabind::value("LShift", SDLK_LSHIFT)
			]
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Binding_Event:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope Binding_Event()
	{
		return (
			luabind::namespace_("orb")
			[
				Binding_Event_Keyboard(),
				Binding_Event_Mouse(),

				luabind::class_<SDL_Event>("Event")
			]
		);
	}

} // orb namespace