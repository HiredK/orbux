#include "TileProducer.h"
#include "Renderer.h"
#include "Binding_Core.h"
#include "Logger.h"

inline void* alignedAlloc(size_t size, size_t alignment)
{
	void *data = nullptr;
	data = _aligned_malloc(size, alignment);
	return data;
}

inline void alignedFree(void* data)
{
	_aligned_free(data);
}

void orb::TileProducer::Initialize()
{
	m_root.children[0] = nullptr;
	m_root.parent = nullptr;
	m_root.data = nullptr;
	m_root.needTile = true;

	uint32_t bufferSize = MAX_TILE_INSTANCES * 256;
	m_dynamicData = (DynamicUniformData*)alignedAlloc(bufferSize, 256);
	m_dynamicUBO = m_renderer->CreateBuffer(
		bufferSize,
		m_dynamicData,
		Buffer::CreateInfo() = {
			Buffer::eUniform,
			Buffer::eHostVisible,
			true
		}
	);
}

void orb::TileProducer::Synchronize(const dmat4& ltw, const Terrain::Tile* root)
{
	std::function<void(const Terrain::Tile*, QuadTree*, QuadTree*)> RecursiveSync;
	RecursiveSync = [&](
		const Terrain::Tile* tile,
		QuadTree* tree,
		QuadTree* parent)
	{
		if (tree->needTile) {
			auto data = GetTile(tile->level, tile->coord.tx, tile->coord.ty);
			data->coord = tile->coord;
			data->localToWorld = ltw;

			onProduce.Emit(data);
			tree->data = data;
			tree->needTile = false;
		}

		if (tile->children[0] != nullptr) {
			for (unsigned int i = 0; i < 4; ++i)  {
				if (tree->children[i] == nullptr) 
				{
					tree->children[i] = new QuadTree();
					tree->children[i]->children[0] = nullptr;
					tree->children[i]->parent = tree;
					tree->children[i]->data = nullptr;
					tree->children[i]->needTile = true;
				}

				RecursiveSync(tile->children[i].get(), tree->children[i], tree);
			}
		}
	};

	RecursiveSync(root, &m_root, nullptr);
}

void orb::TileProducer::UpdateUniformData(const dmat4& ltw, int level, const Terrain::Coord& coord, uint32_t index) {

	TileData* tileData = nullptr;
	QuadTree* tt = &m_root;
	QuadTree* tc = nullptr;

	int tx = coord.tx;
	int ty = coord.ty;
	int tl = 0;

	float dx = 0;
	float dy = 0;
	float dd = 1;
	float ds0 = (m_tileSize / 2) * 2.0f - 2.0f * m_border;
	float ds = ds0;

	while (tl != level && 
		(tc = tt->children[((tx >> (level - tl - 1)) & 1) |
		((ty >> (level - tl - 1)) & 1) << 1]) != nullptr) {
		tl += 1;
		tt = tc;
	}

	while (level > tl) {
		dx += (tx % 2) * dd;
		dy += (ty % 2) * dd;
		dd *= 2;
		ds /= 2;
		level -= 1;
		tx /= 2;
		ty /= 2;
	}

	tileData = tt->data;
	while (!tileData) {
		dx += (tx % 2) * dd;
		dy += (ty % 2) * dd;
		dd *= 2;
		ds /= 2;
		level -= 1;
		tx /= 2;
		ty /= 2;

		tt = tt->parent;
		if (tt) {
			tileData = tt->data;
		}
		else {
			break;
		}
	}

	if (tileData) {
		DynamicUniformData* data = (DynamicUniformData*)(((uint64_t)m_dynamicData + (index * 256)));
		dx = dx * ((m_tileSize / 2) * 2 - 2 * m_border) / dd;
		dy = dy * ((m_tileSize / 2) * 2 - 2 * m_border) / dd;

		vec4 coords = vec4((dx + m_border) / m_tileSize, (dy + m_border) / m_tileSize, 0.0f, ds / m_tileSize);
		if (m_tileSize % 2 != 0) {
			coords.x = (dx + m_border + 0.5f) / m_tileSize;
			coords.y = (dy + m_border + 0.5f) / m_tileSize;
		}

		data->tileSize.x = coords.w;
		data->tileSize.y = coords.w;
		data->tileSize.z = (m_tileSize / 2) * 2.0f - 2.0f * m_border;
		data->tileSize.w = 0.0f;

		data->tileCoords.x = coords.x;
		data->tileCoords.y = coords.y;
		data->tileCoords.z = coords.z;
		data->tileCoords.w = 0.0f;

		data->level = (float)level;
		data->layer = (float)tileData->layer;
	}
}

orb::TileProducer::TileData* orb::TileProducer::GetTile(int level, int tx, int ty)
{
	std::stringstream ss;
	size_t h0 = std::hash<int>()(level);
	size_t h1 = std::hash<int>()(tx);
	size_t h2 = std::hash<int>()(ty);
	ss << h0 << h1 << h2;

	auto cachedTile = m_tileCache.find(ss.str());
	if (cachedTile == m_tileCache.end())
	{
		TileData* data = new TileData();
		data->level = level;
		data->layer = m_tileCount;
		data->parentLayer = 0;

		if (level > 0) {
			TileData* parentTileData = GetTile(level - 1, tx / 2, ty / 2);
			data->parentLayer = parentTileData->layer;
		}

		m_tileCache[ss.str()] = data;
		m_tileCount++;
	}

	return m_tileCache[ss.str()];
}

void orb::TileProducer::Flush(uint32_t tileCount)
{
	memcpy(m_dynamicUBO->GetMappedMemory(), m_dynamicData, tileCount * 256);
	m_dynamicUBO->Flush();
}