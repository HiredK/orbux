/**
* @file Shader.h
* @brief
*/

#pragma once

#include "FileFactory.h"
#include "FileHandle.h"
#include "Format.h"

// Forward declarations:
namespace picojson
{
	class value;
}

namespace orb
{
	class Shader : public FileHandle
	{
		public:
			REGISTER_FILE(Shader);

			enum Format
			{
				eHLSL,
				eGLSL,
				eSPIRV,
				eFormatNum
			};

			enum Stage
			{
				eUndefined,
				eShared,
				eVertex,
				eTessControl,
				eTessEvaluation,
				eGeometry,
				eFragment,
				eCompute,
				eStageNum
			};

			//! CTOR/DTOR:
			Shader(Engine* engine_instance, const std::string& path);
			virtual ~Shader();

			bool Exists(Stage stage, Format format) const;
			const std::string& GetStageData(Stage stage, Format format) const;

		protected:
			//! VIRTUALS:
			bool CreateData() override;
			void DeleteData() override;

		private:
			//! MEMBERS:
			std::string m_stagesData[eStageNum][eFormatNum];
			std::shared_ptr<picojson::value> m_root;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Shader inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Shader:: Shader(Engine* engine_instance, const std::string& path)
		: FileHandle(engine_instance, path) {
	}
	/*----------------------------------------------------------------------------*/
	inline Shader::~Shader() {
	}

	/*----------------------------------------------------------------------------*/
	inline bool Shader::Exists(Stage stage, Format format) const {
		return !m_stagesData[stage][format].empty();
	}
	/*----------------------------------------------------------------------------*/
	inline const std::string& Shader::GetStageData(Stage stage, Format format) const {
		return m_stagesData[stage][format];
	}

} // orb namespace