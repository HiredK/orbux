/**
* @file FileFactory.h
* @brief Implements a system that automatically derives a class from a FileHandle
* by registering the expected file extension(s).
*/

#pragma once

#include <algorithm>
#include <map>
#include <vector>
#include <tuple>
#include <stdarg.h>
#include <ctype.h>

/**
* @def REGISTER_FILE
* @brief Define this macro in the derived header file:
* REGISTER_FILE(Class)
*/
#define REGISTER_FILE(Class) \
	static const orb::FileFactory::FileCreatorImpl<Class> CTOR;

/**
* @def REGISTER_FILE_IMPL
* @brief Define this macro in the derived source file:
* REGISTER_FILE_IMPL(Class, "ext1", "ext2")
*/
#define REGISTER_FILE_IMPL(Class, ...) \
	const orb::FileFactory::FileCreatorImpl<Class> Class::CTOR(#Class, std::tuple_size<decltype(std::make_tuple(__VA_ARGS__))>::value, __VA_ARGS__);

namespace orb
{
	// Forward declarations:
	class Engine;
	class FileHandle;

	class FileFactory
	{
		public:
			struct IFileCreator
			{
				//! VIRTUALS:
				virtual FileHandle* Create(Engine* engine_instance, const std::string& path) = 0;
				virtual bool IsSupported(const std::string& path) const = 0;
			};

			template <typename Class>
			struct FileCreatorImpl : public virtual IFileCreator
			{
				//! CTOR/DTOR:
				FileCreatorImpl(const std::string& name, int argc, ...);

				//! VIRTUALS:
				FileHandle* Create(Engine* engine_instance, const std::string& path) override;
				bool IsSupported(const std::string& path) const override;

				//! MEMBERS:
				std::vector<std::string> extv;
			};

			//! SERVICES:
			static FileHandle* Detect(Engine* engine_instance, const std::string& path);
			static std::string ExtractExtensionFromPath(const std::string& path);
			static std::string RemoveExtensionFromPath(const std::string& path);

		protected:
			static std::map<std::string, IFileCreator*>& GetFileCreators();
	};

	////////////////////////////////////////////////////////////////////////////////
	// FileFactory::FileCreatorImpl inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	template <typename Class>
	inline FileFactory::FileCreatorImpl<Class>::FileCreatorImpl(const std::string& name, int argc, ...)
	{
		va_list ap;
		va_start(ap, argc);

		while (argc--) {
			std::string ext = va_arg(ap, char*);
			std::transform(ext.begin(), ext.end(), ext.begin(), ::toupper);
			extv.push_back(ext);
		}

		GetFileCreators()[name] = this;
		va_end(ap);
	}

	/*----------------------------------------------------------------------------*/
	template <typename Class>
	inline FileHandle* FileFactory::FileCreatorImpl<Class>::Create(Engine* engine_instance, const std::string& path) {
		return new Class(engine_instance, path);
	}
	/*----------------------------------------------------------------------------*/
	template <typename Class>
	inline bool FileFactory::FileCreatorImpl<Class>::IsSupported(const std::string& path) const {
		auto it = std::find_if(extv.begin(), extv.end(), 
			[&path](const std::string& ext) -> bool {
			return (ext == FileFactory::ExtractExtensionFromPath(path));
		});

		return (it != extv.end());
	}

	////////////////////////////////////////////////////////////////////////////////
	// FileFactory inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline FileHandle* FileFactory::Detect(Engine* engine_instance, const std::string& path)
	{
		FileHandle* file = nullptr;
		auto it = std::find_if(GetFileCreators().begin(), GetFileCreators().end(), 
			[&path](const std::pair<std::string, IFileCreator*>& ctor) -> bool {
			return ctor.second->IsSupported(path);
		});

		if (it != GetFileCreators().end()) {
			file = (*it).second->Create(engine_instance, path);
		}

		return file;
	}

	/*----------------------------------------------------------------------------*/
	inline std::string FileFactory::ExtractExtensionFromPath(const std::string& path) {
		std::string ext = path.substr(path.find_last_of(".") + 1);
		std::transform(ext.begin(), ext.end(), ext.begin(), toupper);
		return ext;
	}
	/*----------------------------------------------------------------------------*/
	inline std::string FileFactory::RemoveExtensionFromPath(const std::string& path) {
		return path.substr(0, path.find_last_of("."));
	}
	/*----------------------------------------------------------------------------*/
	inline std::map<std::string, FileFactory::IFileCreator*>& FileFactory::GetFileCreators() {
		static std::map<std::string, IFileCreator*> creators;
		return creators;
	}

} // orb namespace