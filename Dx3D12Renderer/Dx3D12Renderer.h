/**
* @file Dx3D12Renderer.h
* @brief
*/

#pragma once

#include "Renderer.h"

// Forward declarations:
struct IDXGIFactory4;
struct IDXGIAdapter1;
struct ID3D12Device;
struct ID3D12CommandQueue;
struct ID3D12GraphicsCommandList;
struct ID3D12DescriptorHeap;
struct IDXGISwapChain3;
struct ID3D12Resource;
struct ID3D12Fence;
typedef void* HANDLE;
enum DXGI_FORMAT;

namespace orb
{
	class RENDERER_API Dx3D12Renderer : public Renderer
	{
		public:
			friend class Dx3D12Command;
			friend class Dx3D12RenderPass;

			//! CTOR/DTOR:
			Dx3D12Renderer(const Window& window, BufferingType buffering = eTriple);
			virtual ~Dx3D12Renderer();

			////////////////////////////////////////////////////////////////////////
			// Dx3D12Renderer::CreateBuffer:
			////////////////////////////////////////////////////////////////////////
			/*--------------------------------------------------------------------*/
			std::shared_ptr<Buffer> CreateBuffer(
				uint32_t size,
				const void* data,
				const Buffer::CreateInfo& ci
			) override;

			////////////////////////////////////////////////////////////////////////
			// Dx3D12Renderer::CreateTexture:
			////////////////////////////////////////////////////////////////////////
			/*--------------------------------------------------------------------*/
			std::shared_ptr<Texture> CreateTexture(
				uint32_t w,
				uint32_t h,
				uint32_t depth,
				const void* data,
				const Texture::CreateInfo& ci
			) override;

			////////////////////////////////////////////////////////////////////////
			// Dx3D12Renderer::CreateRenderPass:
			////////////////////////////////////////////////////////////////////////
			/*--------------------------------------------------------------------*/
			RenderPass* CreateRenderPass(
				RenderPass* parent,
				const RenderPass::CreateInfo& ci
			) override;

			void Initialize() override;
			void Resize(int w, int h) override;
			void Display() override;
			void Cleanup() override;
			Type GetType() const override;

			//! SERVICES:
			static DXGI_FORMAT ConvertToDxFormat(Format format);

		protected:
			struct SwapchainImage
			{
				ID3D12Resource* view;
				ID3D12Fence* fence;
				uint64_t fenceValue;
			};

			//! SERVICES:
			void CreateDevice();
			void CreateCommandQueue();
			void CreateSwapchain();

			//! MEMBERS:
			IDXGIFactory4* m_factory;
			IDXGIAdapter1* m_gpu;
			ID3D12Device* m_device;
			ID3D12CommandQueue* m_commandQueue;
			ID3D12DescriptorHeap* m_swapchainHeap;
			IDXGISwapChain3* m_swapchain;
			std::vector<SwapchainImage> m_swapchainImages;
			HANDLE m_fenceEvent;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Dx3D12Renderer inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Dx3D12Renderer:: Dx3D12Renderer(const Window& window, BufferingType buffering)
		: Renderer(window, buffering)
		, m_factory(nullptr)
		, m_gpu(nullptr)
		, m_device(nullptr)
		, m_commandQueue(nullptr)
		, m_swapchainHeap(nullptr)
		, m_swapchain(nullptr)
		, m_fenceEvent(nullptr) {
	}
	/*----------------------------------------------------------------------------*/
	inline Dx3D12Renderer::~Dx3D12Renderer() {
	}

	/*----------------------------------------------------------------------------*/
	inline Renderer::Type Dx3D12Renderer::GetType() const {
		return eDx3D12;
	}
}