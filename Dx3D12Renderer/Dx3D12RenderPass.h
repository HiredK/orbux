/**
* @file Dx3D12RenderPass.h
* @brief
*/

#pragma once

#include "RenderPass.h"

// Forward declarations:
struct ID3D12GraphicsCommandList;
struct ID3D12RootSignature;
struct ID3D12PipelineState;
struct ID3D12DescriptorHeap;

namespace orb
{
	class RENDERER_API Dx3D12RenderPass : public RenderPass
	{
		public:
			friend class Dx3D12Command;
			friend class Dx3D12Renderer;

			//! CTOR/DTOR:
			Dx3D12RenderPass(Renderer* renderer, RenderPass* parent);
			virtual ~Dx3D12RenderPass();

			//! VIRTUALS:
			void Build(const CreateInfo& ci) override;
			void Resize(int w, int h) override;
			void Record() override;
			void Cleanup() override;

		protected:
			//! SERVICES:
			void CreateCommandList();
			void CreateRootSignature();
			void CreatePipelineStateObject();

			//! MEMBERS:
			ID3D12GraphicsCommandList* m_commandList;
			ID3D12RootSignature* m_rootSignature;
			ID3D12PipelineState* m_pipelineStateObject;
			ID3D12DescriptorHeap* m_cbvSrvHeap;
			ID3D12DescriptorHeap* m_rtvHeap;
			std::vector<std::shared_ptr<Command>> m_commands;
			ivec2 m_resolution;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Dx3D12RenderPass inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Dx3D12RenderPass:: Dx3D12RenderPass(Renderer* renderer, RenderPass* parent)
		: RenderPass(renderer, parent)
		, m_commandList(nullptr)
		, m_rootSignature(nullptr)
		, m_pipelineStateObject(nullptr)
		, m_cbvSrvHeap(nullptr)
		, m_rtvHeap(nullptr)
		, m_resolution(ivec2(1, 1)){
	}
	/*----------------------------------------------------------------------------*/
	inline Dx3D12RenderPass::~Dx3D12RenderPass() {
	}

} // orb namespace