#include "Dx3D12Command.h"
#include "Dx3D12RenderPass.h"
#include "Dx3D12Renderer.h"

#include <d3d12.h>
#include "d3dx12.h" // Update 10.0.15063.0

void orb::Dx3D12Command::Build()
{
	auto device = static_cast<Dx3D12Renderer*>(m_parent->GetRenderer())->m_device;
	if (FAILED(device->CreateCommandAllocator(
		D3D12_COMMAND_LIST_TYPE_DIRECT,
		__uuidof(ID3D12CommandAllocator), (void**)&m_allocator)))
	{
		return;
	}
}

void orb::Dx3D12Command::Viewport(float x, float y, float w, float h, float minDepth, float maxDepth)
{
	auto cmdList = static_cast<Dx3D12RenderPass*>(m_parent)->m_commandList;
	cmdList->RSSetViewports(
		1,
		{ &CD3DX12_VIEWPORT(x, y, w, h, minDepth, maxDepth) }
	);
}

void orb::Dx3D12Command::Scissor(int x, int y, int w, int h)
{
	auto cmdList = static_cast<Dx3D12RenderPass*>(m_parent)->m_commandList;
	cmdList->RSSetScissorRects(
		1, 
		{ &(D3D12_RECT() = { x, y, w, h }) }
	);
}

void orb::Dx3D12Command::Draw(
	uint32_t vertexCount,
	uint32_t instanceCount,
	uint32_t firstVertex,
	uint32_t firstInstance)
{
}

void orb::Dx3D12Command::DrawIndexed(
	uint32_t indexCount,
	uint32_t instanceCount,
	uint32_t firstIndex, 
	int32_t vertexOffset, 
	uint32_t firstInstance)
{
}

void orb::Dx3D12Command::Cleanup()
{
}