/**
* @file Dx3D12Texture.h
* @brief
*/

#pragma once

#include "Texture.h"

namespace orb
{
	class RENDERER_API Dx3D12Texture : public Texture
	{
		public:
			//! CTOR/DTOR:
			Dx3D12Texture(
				Renderer* renderer,
				uint32_t w,
				uint32_t h,
				uint32_t depth = 0
			);

			virtual ~Dx3D12Texture();

			//! VIRTUALS:
			void Build(const void* data, const CreateInfo& ci) override;
			void Cleanup() override;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Dx3D12Texture inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Dx3D12Texture:: Dx3D12Texture(
		Renderer* renderer, uint32_t w, uint32_t h, uint32_t depth)
		: Texture(renderer, w, h, depth) {
	}
	/*----------------------------------------------------------------------------*/
	inline Dx3D12Texture::~Dx3D12Texture() {
	}

} // orb namespace