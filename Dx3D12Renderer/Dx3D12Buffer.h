/**
* @file Dx3D12Buffer.h
* @brief
*/

#pragma once

#include "Buffer.h"

namespace orb
{
	class RENDERER_API Dx3D12Buffer : public Buffer
	{
		public:
			//! CTOR/DTOR:
			Dx3D12Buffer(Renderer* renderer);
			virtual ~Dx3D12Buffer();

			//! VIRTUALS:
			void Build(uint32_t size, const void* data, const CreateInfo& ci) override;
			void* Map(uint32_t size, uint32_t offset) override;
			void Unmap() override;
			void Flush() override;
			void Bind(Command* cmd) override;
			void Cleanup() override;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Dx3D12Buffer inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Dx3D12Buffer::Dx3D12Buffer(Renderer* renderer)
		: Buffer(renderer) {
	}
	/*----------------------------------------------------------------------------*/
	inline Dx3D12Buffer::~Dx3D12Buffer() {
	}
}