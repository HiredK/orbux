/**
* @file Dx3D12Command.h
* @brief
*/

#pragma once

#include "Command.h"

// Forward declarations:
struct ID3D12CommandAllocator;

namespace orb
{
	class RENDERER_API Dx3D12Command : public Command
	{
		public:
			friend class Dx3D12RenderPass;

			//! CTOR/DTOR:
			Dx3D12Command(RenderPass* parent);
			virtual ~Dx3D12Command();

			//! VIRTUALS:
			void Build() override;
			void Viewport(float x, float y, float w, float h, float minDepth, float maxDepth) override;
			void Scissor(int x, int y, int w, int h) override;

			void Draw(
				uint32_t vertexCount,
				uint32_t instanceCount,
				uint32_t firstVertex,
				uint32_t firstInstance
			) override;

			void DrawIndexed(
				uint32_t indexCount, 
				uint32_t instanceCount,
				uint32_t firstIndex,
				int32_t vertexOffset, 
				uint32_t firstInstance
			) override;

			void Cleanup() override;

		protected:
			//! MEMBERS:
			ID3D12CommandAllocator* m_allocator;
	};

	////////////////////////////////////////////////////////////////////////////////
	// VulkanCommand inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Dx3D12Command:: Dx3D12Command(RenderPass* parent)
		: Command(parent)
		, m_allocator(nullptr) {
	}
	/*----------------------------------------------------------------------------*/
	inline Dx3D12Command::~Dx3D12Command() {
	}
}