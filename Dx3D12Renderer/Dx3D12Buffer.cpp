#include "Dx3D12Buffer.h"
#include "Dx3D12Renderer.h"
#include "Dx3D12RenderPass.h"

#include <d3d12.h>
#include "d3dx12.h" // Update 10.0.15063.0
#include <wrl.h>

void orb::Dx3D12Buffer::Build(uint32_t size, const void* data, const CreateInfo& ci)
{
}

void* orb::Dx3D12Buffer::Map(uint32_t size, uint32_t offset)
{
	return nullptr;
}

void orb::Dx3D12Buffer::Unmap()
{
}

void orb::Dx3D12Buffer::Flush()
{
}

void orb::Dx3D12Buffer::Bind(Command* cmd)
{
}

void orb::Dx3D12Buffer::Cleanup()
{
}