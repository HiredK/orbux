#include "Dx3D12RenderPass.h"
#include "Dx3D12Buffer.h"
#include "Dx3D12Command.h"
#include "Dx3D12Renderer.h"
#include "Dx3D12Texture.h"
#include "Shader.h"

#include <D3Dcompiler.h>
#include <d3d12.h>
#include "d3dx12.h" // Update 10.0.15063.0

void orb::Dx3D12RenderPass::Build(const CreateInfo& ci)
{
	m_createInfo = ci;

	Dx3D12RenderPass::CreateCommandList();
	Dx3D12RenderPass::CreateRootSignature();
	Dx3D12RenderPass::CreatePipelineStateObject();

	m_cameraUBO = m_renderer->CreateBuffer(
		sizeof(CameraData),
		&m_cameraData,
		Buffer::CreateInfo() = {
			Buffer::eUniform, 
			Buffer::eHostVisible,
			false
		}
	);

	Dx3D12RenderPass::Resize(
		static_cast<int>(ci.resolution.x),
		static_cast<int>(ci.resolution.y)
	);
}

void orb::Dx3D12RenderPass::CreateCommandList()
{
	auto device = static_cast<Dx3D12Renderer*>(m_renderer)->m_device;

	for (uint32_t i = 0; i < m_renderer->GetBufferCount(); ++i) {
		auto cmd = std::shared_ptr<Command>(new Dx3D12Command(this));
		cmd->Build();

		m_commands.push_back(cmd);
	}

	if (FAILED(device->CreateCommandList(
		0,
		D3D12_COMMAND_LIST_TYPE_DIRECT,
		static_cast<Dx3D12Command*>(m_commands[0].get())->m_allocator,
		nullptr,
		__uuidof(ID3D12GraphicsCommandList),
		(void**)&m_commandList)))
	{
		return;
	}

	if (FAILED(m_commandList->Close()))
	{
		return;
	}
}

void orb::Dx3D12RenderPass::CreateRootSignature()
{
	auto device = static_cast<Dx3D12Renderer*>(m_renderer)->m_device;
	std::vector<CD3DX12_DESCRIPTOR_RANGE> descRanges;
	std::vector<CD3DX12_ROOT_PARAMETER> rootParams;
	CD3DX12_ROOT_SIGNATURE_DESC rootSignatureDesc = {};
	ID3DBlob* rootSignatureBlob;

	CD3DX12_DESCRIPTOR_RANGE cameraCBV = {};
	cameraCBV.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
	descRanges.push_back(cameraCBV);

	CD3DX12_ROOT_PARAMETER cameraRootParam = {};
	cameraRootParam.InitAsDescriptorTable(1, &descRanges[0], D3D12_SHADER_VISIBILITY_ALL);
	rootParams.push_back(cameraRootParam);

	CD3DX12_STATIC_SAMPLER_DESC staticSamplers[1];
	staticSamplers[0].Init(0, D3D12_FILTER_MIN_MAG_MIP_LINEAR);

	rootSignatureDesc.Init(
		static_cast<UINT>(rootParams.size()),
		rootParams.data(),
		1,
		staticSamplers,
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS
	);

	if (FAILED(D3D12SerializeRootSignature(
		&rootSignatureDesc,
		D3D_ROOT_SIGNATURE_VERSION_1,
		&rootSignatureBlob,
		nullptr)))
	{
		return;
	}

	if (FAILED(device->CreateRootSignature(
		0,
		rootSignatureBlob->GetBufferPointer(),
		rootSignatureBlob->GetBufferSize(),
		IID_PPV_ARGS(&m_rootSignature)
	)))
	{
		return;
	}
}

void orb::Dx3D12RenderPass::CreatePipelineStateObject()
{
	auto device = static_cast<Dx3D12Renderer*>(m_renderer)->m_device;
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	ID3DBlob* shaderBlob;
	ID3DBlob* shaderBlobError;

	auto inputElementDescs = std::vector<D3D12_INPUT_ELEMENT_DESC>();
	for (auto& vi : m_createInfo.vertexInput) {
		inputElementDescs.push_back(
			D3D12_INPUT_ELEMENT_DESC() = {
				vi.second.name.c_str(),                             
				0,                                                  
				Dx3D12Renderer::ConvertToDxFormat(vi.second.format),
				0,                                                  
				vi.second.offset,                                   
				D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,         
				0                                                   
			}
		);
	}

	D3D12_INPUT_LAYOUT_DESC inputLayoutDesc = {};
	inputLayoutDesc.NumElements = static_cast<UINT>(inputElementDescs.size());
	inputLayoutDesc.pInputElementDescs = inputElementDescs.data();
	psoDesc.InputLayout = inputLayoutDesc;

	auto StageToDxTarget = std::map<Shader::Stage, LPCSTR>() =
	{
		{ Shader::eVertex         , "vs_5_0" },
		{ Shader::eTessControl    , "hs_5_0" },
		{ Shader::eTessEvaluation , "hs_5_0" },
		{ Shader::eGeometry       , "gs_5_0" },
		{ Shader::eFragment       , "ps_5_0" },
		{ Shader::eCompute        , "cs_5_0" }
	};

	for (unsigned int i = 0; i < Shader::eStageNum; ++i) {
		if (i == Shader::eShared) {
			continue;
		}

		auto stage = static_cast<Shader::Stage>(i);
		if (m_createInfo.shader->Exists(stage, Shader::eHLSL))
		{
			auto stageData = m_createInfo.shader->GetStageData(stage, Shader::eHLSL);
			if (FAILED(D3DCompile(
				static_cast<LPCVOID>(stageData.data()),
				stageData.size(),
				m_createInfo.shader->GetPath().c_str(),
				nullptr,
				nullptr,
				"main",
				StageToDxTarget[stage],
				D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION,
				0,
				&shaderBlob,
				&shaderBlobError
			)))
			{
				auto e = (char*)shaderBlobError->GetBufferPointer();
				printf("%s\n", e);
				continue;
			}

			const auto bc = D3D12_SHADER_BYTECODE() = {
				shaderBlob->GetBufferPointer(),
				shaderBlob->GetBufferSize()
			};

			switch (stage) {
				case Shader::eVertex:   psoDesc.VS = bc; break;
				case Shader::eFragment: psoDesc.PS = bc; break;
				default: continue;
			}
		}
	}

	auto ConvertToDxBlending = [](const Blending& input) -> D3D12_RENDER_TARGET_BLEND_DESC
	{
		auto BlendFactorToD3D12 = std::map<BlendFactor, D3D12_BLEND>() =
		{
			{ eZero                  , D3D12_BLEND_ZERO             },
			{ eOne                   , D3D12_BLEND_ONE              },
			{ eSrcColor              , D3D12_BLEND_SRC_COLOR        },
			{ eOneMinusSrcColor      , D3D12_BLEND_INV_SRC_COLOR    },
			{ eDstColor              , D3D12_BLEND_DEST_COLOR       },
			{ eOneMinusDstColor      , D3D12_BLEND_INV_DEST_COLOR   },
			{ eSrcAlpha              , D3D12_BLEND_SRC_ALPHA        },
			{ eOneMinusSrcAlpha      , D3D12_BLEND_INV_SRC_ALPHA    },
			{ eDstAlpha              , D3D12_BLEND_DEST_ALPHA       },
			{ eOneMinusDstAlpha      , D3D12_BLEND_INV_DEST_ALPHA   },
			{ eConstantColor         , D3D12_BLEND_BLEND_FACTOR     },
			{ eOneMinusConstantColor , D3D12_BLEND_INV_BLEND_FACTOR },
			{ eConstantAlpha         , D3D12_BLEND_BLEND_FACTOR     },
			{ eOneMinusConstantAlpha , D3D12_BLEND_INV_BLEND_FACTOR },
			{ eSrcAlphaSaturate      , D3D12_BLEND_SRC_ALPHA_SAT    },
			{ eSrc1Color             , D3D12_BLEND_SRC1_COLOR       },
			{ eOneMinusSrc1Color     , D3D12_BLEND_INV_SRC1_COLOR   },
			{ eSrc1Alpha             , D3D12_BLEND_SRC1_ALPHA       },
			{ eOneMinusSrc1Alpha     , D3D12_BLEND_INV_SRC1_ALPHA   }
		};

		auto BlendOpToD3D12 = std::map<BlendOp, D3D12_BLEND_OP>() =
		{
			{ eAdd                   , D3D12_BLEND_OP_ADD           },
			{ eSubtract              , D3D12_BLEND_OP_SUBTRACT      },
			{ eReverseSubtract       , D3D12_BLEND_OP_REV_SUBTRACT  },
			{ eMin                   , D3D12_BLEND_OP_MIN           },
			{ eMax                   , D3D12_BLEND_OP_MAX           }
		};

		D3D12_RENDER_TARGET_BLEND_DESC rtBlendDesc = {};
		rtBlendDesc.BlendEnable = input.blendEnable;
		rtBlendDesc.SrcBlend = BlendFactorToD3D12[input.srcColorBlendFactor];
		rtBlendDesc.DestBlend = BlendFactorToD3D12[input.dstColorBlendFactor];
		rtBlendDesc.BlendOp = BlendOpToD3D12[input.colorBlendOp];
		rtBlendDesc.SrcBlendAlpha = BlendFactorToD3D12[input.srcAlphaBlendFactor];
		rtBlendDesc.DestBlendAlpha = BlendFactorToD3D12[input.dstAlphaBlendFactor];
		rtBlendDesc.BlendOpAlpha = BlendOpToD3D12[input.alphaBlendOp];
		rtBlendDesc.RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
		return rtBlendDesc;
	};

	DXGI_SAMPLE_DESC sampleDesc = {};
	sampleDesc.Count = 1;

	D3D12_RASTERIZER_DESC& rastDesc = psoDesc.RasterizerState;
	rastDesc.FillMode = D3D12_FILL_MODE_SOLID;
	rastDesc.CullMode = D3D12_CULL_MODE_NONE;
	rastDesc.FrontCounterClockwise = FALSE;
	rastDesc.DepthBias = D3D12_DEFAULT_DEPTH_BIAS;
	rastDesc.DepthBiasClamp = D3D12_DEFAULT_DEPTH_BIAS_CLAMP;
	rastDesc.SlopeScaledDepthBias = D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS;
	rastDesc.DepthClipEnable = true;
	rastDesc.MultisampleEnable = FALSE;
	rastDesc.AntialiasedLineEnable = FALSE;
	rastDesc.ForcedSampleCount = 0;
	rastDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

	psoDesc.pRootSignature = m_rootSignature;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.SampleDesc = sampleDesc;

	psoDesc.BlendState.AlphaToCoverageEnable = false;
	psoDesc.BlendState.RenderTarget[0] = ConvertToDxBlending(Blending::GetDefault());
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.NumRenderTargets = 1;

	if (FAILED(device->CreateGraphicsPipelineState(
		&psoDesc,
		IID_PPV_ARGS(&m_pipelineStateObject))))
	{
		return;
	}
}

void orb::Dx3D12RenderPass::Resize(int w, int h)
{
	m_resolution = ivec2(w, h);
}

void orb::Dx3D12RenderPass::Record()
{
	auto device = static_cast<Dx3D12Renderer*>(m_renderer)->m_device;
	auto bufferIndex = m_renderer->GetBufferIndex();
	auto dxSwapchainImageView = static_cast<Dx3D12Renderer*>(m_renderer)->m_swapchainImages[bufferIndex].view;
	auto dxSwapchainHeap = static_cast<Dx3D12Renderer*>(m_renderer)->m_swapchainHeap;
	auto dxCmdQueue = static_cast<Dx3D12Renderer*>(m_renderer)->m_commandQueue;
	auto dxCmdAlloc = static_cast<Dx3D12Command*>(m_commands[bufferIndex].get())->m_allocator;

	auto rtvStride = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	auto rtvHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		dxSwapchainHeap->GetCPUDescriptorHandleForHeapStart(),
		bufferIndex,
		rtvStride
	);

	float clearColor[4] = {
		m_createInfo.clearColor.r * (1.0f / 255.0f),
		m_createInfo.clearColor.g * (1.0f / 255.0f),
		m_createInfo.clearColor.b * (1.0f / 255.0f),
		m_createInfo.clearColor.a * (1.0f / 255.0f)
	};

	dxCmdAlloc->Reset();
	m_commandList->Reset(dxCmdAlloc, m_pipelineStateObject);
	m_commandList->ResourceBarrier(
		1,
		&CD3DX12_RESOURCE_BARRIER::Transition(
			dxSwapchainImageView,
			D3D12_RESOURCE_STATE_PRESENT,
			D3D12_RESOURCE_STATE_RENDER_TARGET
		)
	);

	m_commandList->ClearRenderTargetView(rtvHandle, clearColor, 0, nullptr);
	m_commandList->OMSetRenderTargets(1, &rtvHandle, false, nullptr);
	m_commandList->SetGraphicsRootSignature(m_rootSignature);

	m_commands[bufferIndex]->Viewport(0.0f, 0.0f, (float)m_resolution.x, (float)m_resolution.y);
	m_commands[bufferIndex]->Scissor(0, 0, m_resolution.x, m_resolution.y);
	onRecord.Emit(m_commands[bufferIndex].get());
	m_commandList->ResourceBarrier(
		1, 
		&CD3DX12_RESOURCE_BARRIER::Transition(
			dxSwapchainImageView,
			D3D12_RESOURCE_STATE_RENDER_TARGET,
			D3D12_RESOURCE_STATE_PRESENT
		)
	);

	m_commandList->Close();
	ID3D12CommandList* ppCommandLists[] = { m_commandList };
	dxCmdQueue->ExecuteCommandLists(
		_countof(ppCommandLists), ppCommandLists
	);
}

void orb::Dx3D12RenderPass::Cleanup()
{
}