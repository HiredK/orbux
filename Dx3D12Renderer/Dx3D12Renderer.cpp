#include "Dx3D12Renderer.h"
#include "Dx3D12Buffer.h"
#include "Dx3D12Command.h"
#include "Dx3D12RenderPass.h"
#include "Dx3D12Texture.h"
#include "Window.h"

#include "SDL/SDL.h"
#include "SDL/SDL_syswm.h"

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <wrl.h>
#include <dxgi1_4.h>
#include <d3d12.h>
#include "d3dx12.h" // Update 10.0.15063.0

DXGI_FORMAT orb::Dx3D12Renderer::ConvertToDxFormat(Format format)
{
	auto FormatToDxFormat = std::map<Format, DXGI_FORMAT>() =
	{
		{ Format::eUndefined , DXGI_FORMAT_UNKNOWN            },
		{ Format::eR32F      , DXGI_FORMAT_R32_FLOAT          },
		{ Format::eRG32F     , DXGI_FORMAT_R32G32_FLOAT       },
		{ Format::eRGB32F    , DXGI_FORMAT_R32G32B32_FLOAT    },
		{ Format::eRGBA32F   , DXGI_FORMAT_R32G32B32A32_FLOAT },
		{ Format::eR8        , DXGI_FORMAT_R8_UNORM           },
		{ Format::eRG8       , DXGI_FORMAT_R8G8_UNORM         },
		{ Format::eRGBA8     , DXGI_FORMAT_R8G8B8A8_UNORM     }
	};

	return FormatToDxFormat[format];
}

std::shared_ptr<orb::Buffer> orb::Dx3D12Renderer::CreateBuffer(
	uint32_t size,
	const void* data,
	const Buffer::CreateInfo& ci)
{
	return nullptr;
}

std::shared_ptr<orb::Texture> orb::Dx3D12Renderer::CreateTexture(
	uint32_t w,
	uint32_t h,
	uint32_t depth,
	const void* data,
	const Texture::CreateInfo& ci)
{
	return nullptr;
}

orb::RenderPass* orb::Dx3D12Renderer::CreateRenderPass(
	RenderPass* parent,
	const RenderPass::CreateInfo& ci)
{
	RenderPass* actualParent = nullptr;
	if (!m_renderPasses.empty()) {
		actualParent = m_renderPasses.back().get();
	}

	auto renderPass = std::shared_ptr<RenderPass>(new Dx3D12RenderPass(this, actualParent));
	renderPass->Build(ci);

	if (ci.type == RenderPass::eDrawOnce ||
		ci.type == RenderPass::eDrawOnceBackground)
	{
		auto it = m_renderPasses.begin();
		while (it != m_renderPasses.end()) {
			if ((*it)->GetType() == RenderPass::eDrawOnce ||
				(*it)->GetType() == RenderPass::eDrawOnceBackground) {
				it++;
			}
			else {
				break;
			}
		}

		m_renderPasses.insert(it, renderPass);
	}
	else {
		m_renderPasses.push_back(renderPass);
	}

	for (unsigned int i = 0; i < m_renderPasses.size(); ++i) {
		auto renderPass = static_cast<Dx3D12RenderPass*>(m_renderPasses[i].get());
		if (i > 0) {
			renderPass->m_parent = m_renderPasses[i - 1].get();
		}
		else {
			renderPass->m_parent = nullptr;
		}
	}

	return renderPass.get();
}

void orb::Dx3D12Renderer::Initialize()
{
	Dx3D12Renderer::CreateDevice();
	Dx3D12Renderer::CreateCommandQueue();
	Dx3D12Renderer::CreateSwapchain();

	m_fenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
	if (m_fenceEvent == NULL)
	{
		return;
	}
}

void orb::Dx3D12Renderer::CreateDevice()
{
	D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_12_1;
	UINT factoryFlags = 0;
	UINT adapterIndex = 0;

	Microsoft::WRL::ComPtr<ID3D12Debug> debugController;
	if (FAILED(D3D12GetDebugInterface(
		IID_PPV_ARGS(&debugController))))
	{
		return;
	}

	factoryFlags |= DXGI_CREATE_FACTORY_DEBUG;
	debugController->EnableDebugLayer();

	if (FAILED(CreateDXGIFactory2(
		factoryFlags,
		__uuidof(IDXGIFactory4), (void**)&m_factory)))
	{
		return;
	}

	while (m_factory->EnumAdapters1(
		adapterIndex, &m_gpu) != DXGI_ERROR_NOT_FOUND)
	{
		auto WideToString = [](const WCHAR* input, size_t size)
		{
			char* result = new char[size];
			wcstombs_s(0, result, size, input, size);
			return std::string(result);
		};

		DXGI_ADAPTER_DESC1 props;
		m_gpu->GetDesc1(&props);

		if (props.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) {
			adapterIndex++;
			continue;
		}

		if (FAILED(D3D12CreateDevice(
			m_gpu, 
			featureLevel,
			_uuidof(ID3D12Device), nullptr))) {
			adapterIndex++;
			continue;
		}

		m_gpuInfo.vendorID = props.VendorId;
		m_gpuInfo.deviceID = props.DeviceId;
		m_gpuInfo.info = WideToString(props.Description, 128);
		break;
	}

	if (FAILED(D3D12CreateDevice(
		m_gpu, 
		featureLevel,
		__uuidof(ID3D12Device), (void**)&m_device)))
	{
		return;
	}
}

void orb::Dx3D12Renderer::CreateCommandQueue()
{
	D3D12_COMMAND_QUEUE_DESC commandQueueDesc = {};
	commandQueueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	commandQueueDesc.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
	commandQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	commandQueueDesc.NodeMask = 0;

	if (FAILED(m_device->CreateCommandQueue(
		&commandQueueDesc, 
		__uuidof(ID3D12CommandQueue), (void**)&m_commandQueue)))
	{
		return;
	}
}

void orb::Dx3D12Renderer::CreateSwapchain()
{
	D3D12_DESCRIPTOR_HEAP_DESC swapchainHeapDesc = {};
	swapchainHeapDesc.NumDescriptors = static_cast<UINT>(Renderer::GetBufferCount());
	swapchainHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	swapchainHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;

	if (FAILED(m_device->CreateDescriptorHeap(
		&swapchainHeapDesc,
		__uuidof(ID3D12DescriptorHeap), (void**)&m_swapchainHeap)))
	{
		return;
	}

	RECT windowClientRect;
	if (!GetClientRect(
		m_window.GetSysWMinfo()->info.win.window,
		&windowClientRect))
	{
		return;
	}

	DXGI_SWAP_CHAIN_DESC swapchainDesc = {};
	swapchainDesc.BufferDesc.Width = windowClientRect.right - windowClientRect.left;
	swapchainDesc.BufferDesc.Height = windowClientRect.bottom - windowClientRect.top;
	swapchainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapchainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapchainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapchainDesc.SampleDesc.Count = 1;
	swapchainDesc.SampleDesc.Quality = 0;
	swapchainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapchainDesc.BufferCount = static_cast<UINT>(Renderer::GetBufferCount());
	swapchainDesc.OutputWindow = m_window.GetSysWMinfo()->info.win.window;
	swapchainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swapchainDesc.Windowed = !m_window.IsFullscreen();
	swapchainDesc.Flags = 0;

	Microsoft::WRL::ComPtr<IDXGISwapChain> swapchain;
	if (FAILED(m_factory->CreateSwapChain(
		m_commandQueue, &swapchainDesc, &swapchain)))
	{
		return;
	}

	if (FAILED(swapchain->QueryInterface(
		__uuidof(IDXGISwapChain3), (void**)&m_swapchain)))
	{
		return;
	}

	Dx3D12Renderer::Resize(
		static_cast<int>(swapchainDesc.BufferDesc.Width),
		static_cast<int>(swapchainDesc.BufferDesc.Height)
	);
}

void orb::Dx3D12Renderer::Resize(int w, int h)
{
	if (!m_swapchainImages.empty())
	{
		for (auto& swapchainImage : m_swapchainImages) {
			swapchainImage.view->Release();
			swapchainImage.fence->Release();
		}

		DXGI_SWAP_CHAIN_DESC desc = {};
		m_swapchain->GetDesc(&desc);
		m_swapchain->ResizeBuffers(
			Renderer::GetBufferCount(),
			w,
			h, 
			desc.BufferDesc.Format,
			desc.Flags
		);

		m_swapchainImages.clear();
	}

	CD3DX12_CPU_DESCRIPTOR_HANDLE descriptorHandle(m_swapchainHeap->GetCPUDescriptorHandleForHeapStart());
	UINT descriptorSize = m_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	m_swapchainImages.resize(Renderer::GetBufferCount());

	for (uint32_t i = 0; i < Renderer::GetBufferCount(); ++i)
	{
		if (FAILED(m_swapchain->GetBuffer(i,
			__uuidof(ID3D12Resource), (void**)&m_swapchainImages[i].view)))
		{
			return;
		}

		if (FAILED(m_device->CreateFence(
			0, 
			D3D12_FENCE_FLAG_NONE,
			__uuidof(ID3D12Fence), (void**)&m_swapchainImages[i].fence)))
		{
			return;
		}

		m_device->CreateRenderTargetView(
			m_swapchainImages[i].view, 
			NULL,
			descriptorHandle
		);

		descriptorHandle.Offset(1, descriptorSize);
		m_swapchainImages[i].fenceValue = 0;
	}

	for (auto& renderPass : m_renderPasses) {
		renderPass->Resize(w, h);
	}
}

void orb::Dx3D12Renderer::Display()
{
	m_bufferIndex = m_swapchain->GetCurrentBackBufferIndex();
	auto& swapchainImage = m_swapchainImages[m_bufferIndex];

	if (swapchainImage.fence->GetCompletedValue() < swapchainImage.fenceValue)
	{
		swapchainImage.fence->SetEventOnCompletion(
			swapchainImage.fenceValue,
			m_fenceEvent
		);

		WaitForSingleObject(m_fenceEvent, INFINITE);
	}

	for (auto& renderPass : m_renderPasses) {
		renderPass->Record();
	}

	swapchainImage.fenceValue++;
	m_commandQueue->Signal(swapchainImage.fence, swapchainImage.fenceValue);
	m_swapchain->Present(0, 0);
}

void orb::Dx3D12Renderer::Cleanup()
{
}